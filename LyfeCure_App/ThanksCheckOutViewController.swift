//
//  ThanksCheckOutViewController.swift
//  LyfeCure_App
//
//  Created by EL Group on 19/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ThanksCheckOutViewController: UIViewController {

    @IBOutlet weak var lblOrderId : UILabel!
    
    var orderNo : String?
    var orderMessage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    
    func onViewDidLoad() {
        lblOrderId?.text = "Order Id : \(/orderNo)\n\n Order Message : \(/orderMessage)"
    }

    @IBAction func btnBack(_ sender : UIButton){
        guard let viewControllers = self.navigationController?.viewControllers else {
            return
        }
        
        for (_ , vc) in viewControllers.enumerated() {
            if vc is DashboardTableViewController {
                self.navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
}
