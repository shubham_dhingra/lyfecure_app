//
//  URLImageDownloader.swift
//  Glam
//
//  Created by ShailshNailwal on 2/9/18.
//  Copyright © 2018 Shailsh. All rights reserved.
//

import UIKit

class URLImageCache: NSObject {
    
    static public var imageBaseURL = base_url
    
    static public var sharedCache: URLImageCache = URLImageCache()
    static var arrCache: Array<Dictionary<String, Any>> = Array<Dictionary<String, Any>>()
    
    static var keyURLStr: String = "urlStr"
    static var keyimage:  String = "img"

    func downloadImage(fromURLString urlStr: String, completionHandler completionhandler: @escaping(_ downloadStatus: Bool, _ imageFound: Bool , _ alreadyPresent: Bool, _ urlImageInfo: Dictionary<String, Any>?)-> Void) -> Void {
        
        var useURLString = urlStr
        
        if !useURLString.starts(with: URLImageCache.imageBaseURL) {
            
            useURLString = URLImageCache.imageBaseURL + urlStr
        }
        
        let filtered : Array = URLImageCache.arrCache.filter{($0[URLImageCache.keyURLStr] as! String) == useURLString}
        
        if filtered.count > 0 {
            
            completionhandler(true, true, true, filtered[0])
            return
        }
        
        let imageURL: URL? = URL.init(string: useURLString)
        
        let session: URLSession = URLSession.shared
        let task: URLSessionTask = session.dataTask(with: imageURL!) { (data, response, error) in
            
            if data != nil {
                
                let img: UIImage? = UIImage.init(data: data!)
                
                if img != nil {
                    
                    var urlImageInfo: Dictionary<String, Any> = Dictionary<String, UIImage>()
                    
                    urlImageInfo[URLImageCache.keyURLStr] = useURLString
                    urlImageInfo[URLImageCache.keyimage]  = img
                    
                    URLImageCache.arrCache.append(urlImageInfo)
                    
                    completionhandler(true, true, false, urlImageInfo)
//                    print("Image found")
                }
                else {
                    
                    completionhandler(true, false, false, nil)
//                    print("No image in data")
                }
            }
            else {
                
                completionhandler(false, false, false, nil)
//                print("No data")
            }
        }
        task.resume()
    }
    
    func downloadImage(forImageView imageView: UIImageView ,fromURLString urlStr: String, completionHandler completionhandler: @escaping(_ downloadStatus: Bool, _ imageFound: Bool , _ alreadyPresent: Bool, _ urlImageInfo: Dictionary<String, Any>?)-> Void) -> Void {
        
        var useURLString = urlStr
        
        if !useURLString.starts(with: URLImageCache.imageBaseURL) {
            
            useURLString = URLImageCache.imageBaseURL + urlStr
        }
        
        let filtered : Array = URLImageCache.arrCache.filter{($0[URLImageCache.keyURLStr] as! String) == useURLString}
        
        if filtered.count > 0 {
            
            DispatchQueue.main.async {
                
                imageView.image = filtered[0][URLImageCache.keyimage] as? UIImage
            }
            completionhandler(true, true, true, filtered[0])
            return
        }
        
        let imageURL: URL? = URL.init(string: useURLString)
        
        let session: URLSession = URLSession.shared
        let task: URLSessionTask = session.dataTask(with: imageURL!) { (data, response, error) in
            
            if data != nil {
                
                let img: UIImage? = UIImage.init(data: data!)
                
                if img != nil {
                    
                    DispatchQueue.main.async {
                        
                        imageView.image = img
                    }
                    
                    var urlImageInfo: Dictionary<String, Any> = Dictionary<String, UIImage>()
                    
                    urlImageInfo[URLImageCache.keyURLStr] = useURLString
                    urlImageInfo[URLImageCache.keyimage]  = img
                    
                    URLImageCache.arrCache.append(urlImageInfo)
                    
                    completionhandler(true, true, false, urlImageInfo)
//                    print("Image found")
                }
                else {
                    
                    completionhandler(true, false, false, nil)
//                    print("No image in data")
                }
            }
            else {
                
                completionhandler(false, false, false, nil)
//                print("No data")
            }
        }
        task.resume()
    }
    
    func downloadImage(toDisplayOnTable tableView: UITableView, atIndexPath indexPath: IndexPath, inImageView imageView: UIImageView?, fromURLString urlStr: String, completionHandler completionhandler: @escaping(_ downloadStatus: Bool, _ imageFound: Bool , _ alreadyPresent: Bool, _ urlImageInfo: Dictionary<String, Any>?)-> Void) -> Void {
        
        var useURLString = urlStr
        
        if !useURLString.starts(with: URLImageCache.imageBaseURL) {
            
            useURLString = URLImageCache.imageBaseURL + urlStr
        }
        
        //// check if image is already downloaded ////
        let filtered : Array = URLImageCache.arrCache.filter{($0[URLImageCache.keyURLStr] as! String) == useURLString}
        if filtered.count > 0 {
            
//            completionhandler(true, true, true, filtered[0])
            
            let visibleCellsIndexPaths: [IndexPath] = tableView.indexPathsForVisibleRows!
            
            let predicate: NSPredicate = NSPredicate.init(format: "row = %d and section = %d", indexPath.row, indexPath.section)
            let test: Array = (visibleCellsIndexPaths as NSArray).filtered(using: predicate)
//            let test: Array = visibleCellsIndexPaths.filter{$0.row == indexPath.row && $0.section == indexPath.section}
            
            if test.count > 0 {
                
                DispatchQueue.main.async {
                    
                    imageView?.image = filtered[0][URLImageCache.keyimage] as? UIImage
                }
            }
            else if (imageView != nil) {
                
                let visibleCells: Array = tableView.visibleCells
                
                var cell: UITableViewCell?
                var view: UIView? = imageView
                
                while view != nil {
                    
                    if view is UITableViewCell {
                        
                        cell = view as? UITableViewCell
                        break
                    }
                    view = view?.superview
                }

                if visibleCells.contains(cell!) {
                    
                    DispatchQueue.main.async {
                        
                        imageView?.image = filtered[0][URLImageCache.keyimage] as? UIImage
                    }
                }
                else {
                    
                    DispatchQueue.main.async {
                        
                        imageView?.image = filtered[0][URLImageCache.keyimage] as? UIImage
                    }
                }
            }
            
            completionhandler(true, true, false, filtered[0])
            return
        }
        
        let imageURL: URL?       = URL.init(string: useURLString)
        let session: URLSession  = URLSession.shared
        let task: URLSessionTask = session.dataTask(with: imageURL!) { (data, response, error) in
            
            if data != nil {
                
                let img: UIImage? = UIImage.init(data: data!)
                
                if img != nil {
                    
                    var urlImageInfo: Dictionary<String, Any> = Dictionary<String, UIImage>()
                    
                    urlImageInfo[URLImageCache.keyURLStr] = useURLString
                    urlImageInfo[URLImageCache.keyimage]  = img
                    
                    URLImageCache.arrCache.append(urlImageInfo)
                    
                    DispatchQueue.main.async {
                        
                        let visibleCellsIndexPaths: [IndexPath] = tableView.indexPathsForVisibleRows!
                        
                        let predicate: NSPredicate = NSPredicate.init(format: "row = %d and section = %d", indexPath.row, indexPath.section)
                        let test: Array = (visibleCellsIndexPaths as NSArray).filtered(using: predicate)
                        //                        let test: Array = visibleCellsIndexPaths.filter{$0.row == indexPath.row && $0.section == indexPath.section}
                        
                        if test.count > 0 {
                            
                            imageView?.image = img
                        }
                        else if imageView != nil {
                            
                            let visibleCells: Array = tableView.visibleCells
                            
                            var cell: UITableViewCell?
                            var view: UIView? = imageView
                            
                            while view != nil {
                                
                                if view is UITableViewCell {
                                    
                                    cell = view as? UITableViewCell
                                    break
                                }
                                view = view?.superview
                            }
                            
                            if visibleCells.contains(cell!) {
                                
                                imageView?.image = img
                                //print("cell found")
                            }
                            else {
                                
                                imageView?.image = img
                            }
                        }
                    }
                    
                    completionhandler(true, true, false, urlImageInfo)
//                    print("Image found")
                }
                else {
                    
                    completionhandler(true, false, false, nil)
//                    print("No image in data")
                }
            }
            else {
                
                completionhandler(false, false, false, nil)
//                print("No data")
            }
        }
        task.resume()
    }
    
    
    func downloadImage(toDisplayOnCollectionView collectionView: UICollectionView, atIndexPath indexPath: IndexPath, inImageView imageView: UIImageView?, fromURLString urlStr: String, completionHandler completionhandler: @escaping(_ downloadStatus: Bool, _ imageFound: Bool , _ alreadyPresent: Bool, _ urlImageInfo: Dictionary<String, Any>?)-> Void) -> Void {
        
        var useURLString = urlStr
        
        if !useURLString.starts(with: URLImageCache.imageBaseURL) {
            
            useURLString = URLImageCache.imageBaseURL + urlStr
        }
        
        //// check if image is already downloaded ////
        let filtered : Array = URLImageCache.arrCache.filter{($0[URLImageCache.keyURLStr] as! String) == useURLString}
        if filtered.count > 0 {
            
//            let visibleCellsIndexPaths: [IndexPath] = collectionView.indexPathsForVisibleItems
            
//            let predicate: NSPredicate = NSPredicate.init(format: "row = %d and section = %d", indexPath.row, indexPath.section)
//            let test: Array = (visibleCellsIndexPaths as NSArray).filtered(using: predicate)
//            let test: Array = visibleCellsIndexPaths.filter{$0.row == indexPath.row && $0.section == indexPath.section}
            
//            if test.count > 0 {
            
                DispatchQueue.main.async {
                    
                    imageView?.image = filtered[0][URLImageCache.keyimage] as? UIImage
                }
//            }
//            else if (imageView != nil) {
//
////                let visibleCells: Array = collectionView.visibleCells
////
////                var cell: UICollectionViewCell?
////                var view: UIView? = imageView
////
////                while view != nil {
////
////                    if view is UICollectionViewCell {
////
////                        cell = view as? UICollectionViewCell
////                        break
////                    }
////                    view = view?.superview
////                }
//
//                DispatchQueue.main.async {
//
//                    imageView?.image = filtered[0][URLImageCache.keyimage] as? UIImage
//                }
//            }
            
            completionhandler(true, true, true, filtered[0])
            
            return
        }
        
        let imageURL: URL?       = URL.init(string: useURLString)
        let session: URLSession  = URLSession.shared
        let task: URLSessionTask = session.dataTask(with: imageURL!) { (data, response, error) in
            
            if data != nil {
                
                let img: UIImage? = UIImage.init(data: data!)
                
                if img != nil {
                    
                    var urlImageInfo: Dictionary<String, Any> = Dictionary<String, UIImage>()
                    
                    urlImageInfo[URLImageCache.keyURLStr] = useURLString
                    urlImageInfo[URLImageCache.keyimage]  = img
                    
                    URLImageCache.arrCache.append(urlImageInfo)
                    
                    DispatchQueue.main.async {
                        
                        let visibleCellsIndexPaths: [IndexPath] = collectionView.indexPathsForVisibleItems
                        
//                        let predicate: NSPredicate = NSPredicate.init(format: "row = %d and section = %d", indexPath.row, indexPath.section)
//                        let visibleIPs: Array = (visibleCellsIndexPaths as NSArray).filtered(using: predicate)
//                        let visibleIPs: Array = visibleCellsIndexPaths.filter{$0.row == indexPath.row && $0.section == indexPath.section}
                        
//                        if visibleIPs.count > 0 {
//                            
//                            imageView?.image = img
//                        }
//                        else
                        if (imageView != nil) {
                            
                            //                            let visibleCells: Array = collectionView.visibleCells
                            
                            var imageViewCell: UICollectionViewCell?
                            var view: UIView? = imageView
                            
                            while view != nil {
                                
                                if view is UICollectionViewCell {
                                    
                                    imageViewCell = view as? UICollectionViewCell
                                    break
                                }
                                
                                view = view?.superview
                            }
                            
                            if imageViewCell == collectionView.cellForItem(at: indexPath) {
                                
                                DispatchQueue.main.async {
                                    
                                    imageView?.image = img
                                }
                            }
                            else if collectionView.cellForItem(at: indexPath) == nil {
                                
                                DispatchQueue.main.async {
                                    
                                    imageView?.image = img
                                }
                            }
                            
                            //                            if visibleCells.contains(imageViewCell!) {
                            //
                            //                                DispatchQueue.main.async {
                            //
                            //                                    imageView?.image = img
                            //                                }
                            //
                            //                                print("cell found")
                            //                            }
                            //                            else {
                            //
                            //                                DispatchQueue.main.async {
                            //
                            //                                    imageView?.image = img
                            //                                }
                            //                            }
                        }
                        
//                        let visibleCells: Array = collectionView.visibleCells
//
//                        var cell: UICollectionViewCell?
//                        var view: UIView? = imageView
//
//                        while view != nil {
//
//                            if view is UICollectionViewCell {
//
//                                cell = view as? UICollectionViewCell
//                                break
//                            }
//                            view = view?.superview
//                        }
//
//                        for checkCell in visibleCells {
//
////                            let collectionCell = collectionView.cellForItem(at: collectionCellIP)
//
//                            if checkCell == cell {
//
//                                imageView?.image = img
//                            }
//                        }
                        
                        
//                        if visibleIPs.count > 0 {
//
//                            imageView?.image = img
//                        }
//                        else if imageView != nil {
//
//                            var cell: UICollectionViewCell?
//                            var view: UIView? = imageView
//
//                            while view != nil {
//
//                                if view is UICollectionViewCell {
//
//                                    cell = view as? UICollectionViewCell
//                                    break
//                                }
//                                view = view?.superview
//                            }
//
//                            var ip: IndexPath?
//
//                            if cell != nil {
//
//                                ip = collectionView.indexPath(for: cell!)
//                            }
//
//                            if ip != nil {
//
//                                let visibleIPsForIV: Array = visibleCellsIndexPaths.filter{$0.row == ip?.row && $0.section == ip?.section}
//
//                                if visibleIPsForIV.count > 0 {
//
//                                    imageView?.image = img
//                                    print("Corrected")
//                                }
//                            }
//
////                            imageView?.image = img
//                            print("Check 2")
//                        }
                    }
                    
                    completionhandler(true, true, false, urlImageInfo)
//                    print("Image found")
                }
                else {
                    
                    completionhandler(true, false, false, nil)
                    //                    print("No image in data")
                }
            }
            else {
                
                completionhandler(false, false, false, nil)
                //                print("No data")
            }
        }
        task.resume()
    }
}
