//
//  DownloadTask.swift
//  Glam
//
//  Created by ShailshNailwal on 3/14/18.
//  Copyright © 2018 Shailsh. All rights reserved.
//

import UIKit

protocol DownloadTaskDelegate {
    
    func downloadTask(_ task: DownloadTask, startedWithTotalDataToSend byteToSend: Int64)
    func downloadTask(_ task: DownloadTask, dataSent byteSent: Int64)
    func downloadTask(_ task: DownloadTask, finishedWithSuccess isFinished: Bool)
    func downloadTask(_ task: DownloadTask, finishedWithSuccess isFinished: Bool, withData data: Data?)
}

class DownloadTask: NSObject, URLSessionDelegate, URLSessionTaskDelegate {

    var urlString: String?
    var params: Dictionary<String, Any>?
    
    var session: URLSession?
    var sessionTask: URLSessionTask?
    
    var delegate: DownloadTaskDelegate?
    
    var totalDataToSend: Int64 = 0
    var totalDataSent: Int64   = 0
    
    var taskStarted: Bool = false
    
    func startTask() -> Void {
        
        session = URLSession.init(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
        
        var request: URLRequest = URLRequest.init(url: URL.init(string: urlString! as String)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var requestData: Data? = nil
        do {
            
            if params != nil {
                
                let data:Data = try JSONSerialization.data(withJSONObject: params ?? [:], options: JSONSerialization.WritingOptions.prettyPrinted)
                
                requestData = data
            }
        }
        catch {
            
            //print("data is not found")
        }

        if requestData != nil {
            
            request.httpBody = requestData
        }
        
        let user: User = User.getUserDataFromUserDefaults()
        request.setValue(user.accessToken, forHTTPHeaderField: key_auth_token_header)
        
        sessionTask = session?.dataTask(with: request, completionHandler: { (data, response, error) in
            
            var testData: Data? = data
            
            if data == nil {
                
                testData = Data()
            }
            
            let strTest:String = String.init(data: testData!, encoding: .utf8)!
            //print(strTest)
            
            if strTest.count == 0 {
                
                //print("Data is blank or nill from \(String(describing: self.urlString))")
            }
            
//            let result:Any = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//
//            var processCompleted: Bool = true;
//            if result is Dictionary<String, Any> {
//
//                var successResultFound: Bool = false
//
//                if ((result as! Dictionary<String, Any>)[apiResultSuccessKey] != nil) {
//
//                    let number:NSNumber = (((result as! Dictionary<String, Any>)[apiResultSuccessKey]) as! NSNumber)
//                    successResultFound = number.boolValue
//                }
//            }
            
            //print("\(error)")
            if self.delegate != nil {
                
                self.delegate?.downloadTask(self, finishedWithSuccess: (data != nil), withData: data)
            }
        })
        
        sessionTask?.resume()
        self.totalDataToSend = Int64((requestData! as NSData).length)
        if (delegate != nil) {
            
            delegate?.downloadTask(self, startedWithTotalDataToSend: self.totalDataToSend)
        }
    }
    
    //MARK: - Task
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        self.totalDataSent = totalBytesSent
        if delegate != nil {
            
            delegate?.downloadTask(self, dataSent: totalBytesSent)
        }
    }
}
