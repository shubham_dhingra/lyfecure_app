//
//  APICallExecutor.swift
//  Glam
//
//  Created by Macbook Pro on 05/01/18.
//  Copyright © 2018 Shailsh. All rights reserved.
//

import UIKit

class APICallExecutor: NSObject {
    
    public static var baseURL: String    = base_url
    //public static var secureBaseURL: String    = base_url_secure
    public static var deviceType: String = "ios"
    
    static var apiResultSuccessKey: String = "succ"
    static var apiResultStatusKey: String  = "status"
    
    static func addHeaders() -> Dictionary<String, String> {
            
            //let user: User = User.getUserDataFromUserDefaults()
            
            var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
            
    //        let codeArr = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
    //        let code: String = codeArr[0]
            
           // headerInfo["deviceType"] = "ios"
           // headerInfo[key_locale] = "en"
          let token = UserDefaults.standard.value(forKey: key_auth_token) as? String
            if token != nil {
               // headerInfo["api_auth_token"] = token
            headerInfo["api_auth_token"] =  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjllNDkwMjc3LWQ3NTktNTc3MC1iNWUzLWQ1MGFiMDRlZjE2NCIsImlhdCI6MTU3NzI1NDYzMiwiZXhwIjoxNjA4ODEyMjMyfQ.UwiIEkL_nHIcs54z7gigERdI9FwMurfk9pXQh6PUP4E"
               // headerInfo["api_auth_token"] = user.accessToken
            }
            
            return headerInfo
        }
    
    
    //// for post request ////
    static func postRequestForURLString(_ useBaseString: Bool, _ apiString:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APICallExecutor.requestForURLString(useBaseString, apiString, "POST", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for post request ////
    static func putRequestForURLString(_ useBaseString: Bool, _ apiString:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APICallExecutor.requestForURLString(useBaseString, apiString, "PUT", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for delete request ////
    static func deleteRequestForURLString(_ useBaseString: Bool, _ apiString:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APICallExecutor.requestForURLString(useBaseString, apiString, "DELETE", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for get request ////
    static func getRequestForURLString(_ useBaseString: Bool, _ apiString:String,_ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>?, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var params = [String:Any]()
        if parameters != nil {
            
            params = Dictionary()
            parameters?.forEach { (arg) in
                
                let (k, v) = arg
                params[k] = v
            }
        }
        
        APICallExecutor.requestForURLString(useBaseString, apiString, "GET", headerInfo, params, complitionHandler: complitionHandler);
    }
    
    //// for all request ////
    static func requestForURLString(_ useBaseString: Bool, _ apiString:String, _ httpMehtod:String, _ headerInfo: Dictionary<String, String>, _ parameters:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) {
        
        var requestData: Data? = nil
        var paramListString: String = ""
        
        if httpMehtod.uppercased().compare("GET") == ComparisonResult.orderedSame {
            
            for key in parameters.keys {
                
                var value:Any! = ""
                value = parameters[key]
                paramListString = paramListString + "\(key)=" + (value as AnyObject).description + "&"
            }
            paramListString = paramListString.trimmingCharacters(in: CharacterSet.init(charactersIn: "&"))
        }
        else {
            
            do {
                
                let data:Data = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
                
                requestData = data
            }
            catch {
                
                //print("data is not found")
            }
        }
        
        var urlString: String = ""
        
        if httpMehtod.uppercased().compare("POST") == ComparisonResult.orderedSame {
            
            if useBaseString {
                
                urlString = baseURL + apiString
            }
            else {
                
                urlString = apiString
            }
        }
        else {
            
            if useBaseString {
                
                if paramListString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
                    
                    urlString = baseURL + apiString + "?" + paramListString
                }
                else {
                    
                    urlString = baseURL + apiString
                }
            }
            else {
                
                if paramListString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
                    
                    urlString = apiString + "?" + paramListString
                }
                else {
                    
                    urlString = apiString
                }
            }
        }
        
        let url:URL = URL.init(string: urlString)!
        let request:NSMutableURLRequest = NSMutableURLRequest.init(url: url, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 30)
       
        request.httpMethod = httpMehtod
        request.setValue("application/json",forHTTPHeaderField: "Content-Type")
       // request.setValue(deviceType,forHTTPHeaderField: "Device-Type")
        request.setValue(UserDefaults.standard.value(forKey: key_device_token) as? String, forHTTPHeaderField: key_device_token)
     
        
        request.setValue(UserDefaults.standard.value(forKey: key_auth_token) as? String, forHTTPHeaderField: key_auth_token_header)
        
        for key:String in headerInfo.keys {
            
            request.setValue(headerInfo[key], forHTTPHeaderField: key)
        }
        
        if requestData != nil {
            
            request.httpBody = requestData
        }
        

        
        print("Method : \(request.httpMethod)")
        print("URL : \(url)")
        print("Header : \(headerInfo.description)")
        print("Parameters : \(parameters.description)")
        
        let session:URLSession = URLSession.shared
        let task:URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if data == nil {
                
                print("Data is nil from api : ", urlString)
                complitionHandler(false, error, false, nil, "", true)
            }
            else if response == nil {
                print("Response is nil from api : ", urlString)
                complitionHandler(false, error, false, nil, "", true)
            }
            else {
                
                do {
                    
//                    print("Local : \(user.languageCode)")
//                    print("Method : \(request.httpMethod)")
//                    print("URL : \(url)")
//                    print("Header : \(headerInfo.description)")
//                    print("Parameters : \(parameters.description)")
//                    let strTest:String = String.init(data: data!, encoding: .utf8)!
//                    print("response : " + strTest)
//                    if strTest.count == 0 {
//
//                        print("Data is blank or nill from \(url)")
//                    }
                    
                    let result:Any = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                   
                    if result is Dictionary<String, Any> {
                        print("result : ", result)
//                        {"errors":[]}
                        
                        if ((result as! Dictionary<String, Any>)["errors"] != nil)  {
                            complitionHandler(true, error, false, result, "", false)
                            return
                        }
                        var successResultFound: Bool = false
                        
                        //// for success result ////
                        if ((result as! Dictionary<String, Any>)[apiResultSuccessKey] != nil) {
                            
                            let number:NSNumber = (((result as! Dictionary<String, Any>)[apiResultSuccessKey]) as! NSNumber)
                            successResultFound = number.boolValue
                        }
                        
                        if ((result as! Dictionary<String, Any>)[apiResultStatusKey] != nil) {
                            
                            let number:NSNumber = (((result as! Dictionary<String, Any>)[apiResultStatusKey]) as! NSNumber)
                            successResultFound = (successResultFound || number.boolValue)
                        }
                        //// ***************** ////
                        
                        complitionHandler(true, error, successResultFound, result, "", true)
                    }
                    else {
                        
                        let msg: String = String.init(data: data!, encoding: String.Encoding.utf8)!
                        complitionHandler(true, error, false, nil, msg, true)
                    }
                }
                catch {
                    
                    complitionHandler(false, error, false, nil, (UIApplication.shared.delegate as! AppDelegate).localLanguage.strMsgServerDataError, true)
                }
            }
        }
        task.resume()
    }
}
