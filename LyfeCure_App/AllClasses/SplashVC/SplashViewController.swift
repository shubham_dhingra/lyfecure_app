//
//  SplashViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 06/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
   var dataArr: [Any] = []
    @IBOutlet var scrollCollectionView: UICollectionView!
     let pageSpacing: CGFloat = 4
     @IBOutlet weak var pageControl : UIPageControl!
    let isNotLargeDevice: Bool = UIScreen.main.bounds.size.height < 670
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (UDKeys.UserID.fetch() as? String) != nil {
               let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "DashboardTableViewController") as? DashboardTableViewController
                self.navigationController?.pushViewController(dashboardVC!, animated: false)
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        dataArr = [UIImage (named: "splash-screen_1") as Any,UIImage (named: "splash-screen_2") as Any, UIImage (named: "splash-screen_3") as Any]
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: AppColors.appGreen)
        pageControl!.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        pageControl!.currentPageIndicatorTintColor = AppColors.appGreen
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
               layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
               layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width , height: scrollCollectionView.frame.height)
               layout.scrollDirection = .horizontal
               layout.minimumInteritemSpacing = 0
               layout.minimumLineSpacing = 0
               scrollCollectionView.setCollectionViewLayout(layout, animated: false)
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell: pagecontrolCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "pagecontrolCollectionViewCell", for: indexPath) as! pagecontrolCollectionViewCell
        
        if indexPath.row == 0{
            cell.imgView.image = UIImage (named: "splash-screen_1")
        }
      else  if indexPath.row == 1{
            cell.imgView.image = UIImage (named: "splash-screen_2")
        }
       else  if indexPath.row == 2{
            cell.imgView.image = UIImage (named: "splash-screen_3")
        }
           return cell
       }
    //MARK: UIScrollViewDelegate
      func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
          let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
          pageControl.currentPage = Int(pageNumber)
          print(pageNumber)
        if pageNumber == 2{
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
               let loginVC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                      self.navigationController?.pushViewController(loginVC!, animated: true)
                
                }
        }
    }
}

    /// An extension to `UIImage` for creating images with shapes.
    extension UIImage {
        
        /// Creates a circular outline image.
        class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {
            
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
            guard let context = UIGraphicsGetCurrentContext() else {
                return nil
            }
            context.setStrokeColor(color.cgColor)
            context.setLineWidth(lineWidth)
            // Inset the rect to account for the fact that strokes are
            // centred on the bounds of the shape.
            let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
            context.addEllipse(in: rect)
            context.strokePath()
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
        
    }
    
