//
//  DashboardTableViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 03/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class DashboardTableViewController:  SOContainerViewController ,UICollectionViewDelegate,UICollectionViewDataSource{
    
    @IBOutlet weak var bannerCollectionView : UICollectionView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var searchBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var sideMenuBtn : UIButton!
    @IBOutlet weak var seeAll1Btn : UIButton!
    @IBOutlet weak var seeAll2Btn : UIButton!
    @IBOutlet weak var seeAll3Btn : UIButton!
    @IBOutlet weak var seeAll4Btn : UIButton!
    @IBOutlet weak var categoryCollectionView : UICollectionView!
    @IBOutlet weak var dealOfTheDayCollectionView : UICollectionView!
    @IBOutlet weak var exclusiveCollectionView : UICollectionView!
    @IBOutlet weak var bestSellingCollectionView : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    var categoryArr = [DashboardM]()
    var dealArr = [DashboardM]()
    var exclusiveArr = [DashboardM]()
    var bestArr = [DashboardM]()
    var bannerArr = [DashboardM]()
    var imgpath :String?
    let slideVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MenuVC") as! MenuViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // customNavigationBar()
        
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: AppColors.appGreen)
        pageControl!.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        pageControl!.currentPageIndicatorTintColor = AppColors.appGreen
        
        if bannerCollectionView != nil{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width , height:160)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            bannerCollectionView.setCollectionViewLayout(layout, animated: false)
        }
        
        if categoryCollectionView != nil{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width/3 , height:205)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 5
            layout.minimumInteritemSpacing = 5
            categoryCollectionView.setCollectionViewLayout(layout, animated: false)
        }
        
        if dealOfTheDayCollectionView != nil{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width/2, height:255)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            dealOfTheDayCollectionView.setCollectionViewLayout(layout, animated: false)
        }
        
        if bestSellingCollectionView != nil{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width/2 , height:255)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            bestSellingCollectionView.setCollectionViewLayout(layout, animated: false)
        }
        
        if exclusiveCollectionView != nil{
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width/2, height:255)
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            exclusiveCollectionView.setCollectionViewLayout(layout, animated: false)
        }
        
        CategoryAPI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.sideViewController = slideVc
        self.menuSide = .left
        
    }
    
    @IBAction func btnSidePanelAct(_ sender : UIButton){
        self.sideViewController = slideVc
        self.menuSide = .left
        if let container = self.so_containerViewController{
            container.isSideViewControllerPresented = true
        }
    }
    
    /*********** COLLECTION VIEW DELEGATE********/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bannerCollectionView{
            return bannerArr.count
        }
        if collectionView == categoryCollectionView{
            return categoryArr.count
        }
        if collectionView == dealOfTheDayCollectionView{
            return dealArr.count
        }
        if collectionView == exclusiveCollectionView{
            return exclusiveArr.count
        }
        if collectionView == bestSellingCollectionView{
            return bestArr.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == bannerCollectionView{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
            var banner: DashboardM = DashboardM()
            banner = bannerArr[indexPath.row]
            let imgstr = banner.image
            let str = imgpath! + imgstr!
            // cell1.categoryImgView.image = str
            if str == ""{
                
            }
            let url = URL(string: str)
            if url != nil{
                cell1.bannerImgView?.kf.setImage(with: url, placeholder: UIImage(named:""), options:  [.scaleFactor(UIScreen.main.scale)],
                                                 progressBlock: { receivedSize, totalSize in
                                                    
                },completionHandler: { image, error, cacheType, imageURL in
                    
                })
            }
            else{
                let imgUrl = URL(string: (str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                if  imgUrl != nil {
                    cell1.bannerImgView?.kf.indicatorType = .activity
                    cell1.bannerImgView?.kf.setImage(with: imgUrl, placeholder:nil)
                }
            }
            
            return cell1
        }
        if collectionView == categoryCollectionView{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
            cell1.layer.borderColor = UIColor.clear.cgColor
            cell1.layer.cornerRadius = 8
            cell1.layer.shadowColor = UIColor.lightGray.cgColor
            cell1.layer.shadowOpacity = 0.3
            cell1.layer.shadowOffset = CGSize(width: 2.0, height: 10)
            cell1.layer.shadowRadius = 10
            
            var category : DashboardM = DashboardM()
            category = categoryArr[indexPath.row]
            cell1.titleLbl.text = category.name
            
            let imgstr = category.image
            let str = imgpath! + imgstr!
            // cell1.categoryImgView.image = str
            if str == ""{
                
            }
            let url = URL(string: str)
            if url != nil{
                cell1.categoryImgView?.kf.setImage(with: url, placeholder: UIImage(named:"flower.jpeg"), options:  [.scaleFactor(UIScreen.main.scale)],
                                                   progressBlock: { receivedSize, totalSize in
                                                    
                },completionHandler: { image, error, cacheType, imageURL in
                    
                })
            }
            else{
                let imgUrl = URL(string: (str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                if  imgUrl != nil {
                    cell1.categoryImgView?.kf.indicatorType = .activity
                    cell1.categoryImgView?.kf.setImage(with: imgUrl, placeholder:nil)
                }
            }
            return cell1
        }
        if collectionView == dealOfTheDayCollectionView{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"DealsForTheDayCollectionViewCell", for: indexPath) as! DealsForTheDayCollectionViewCell
            cell1.bgView.layer.borderColor = AppColors.appGradientlightColor.cgColor
            cell1.bgView.layer.borderWidth = 1
            var deal : DashboardM = DashboardM()
            deal = dealArr[indexPath.row]
            cell1.titleLbl.text = deal.product_name
            cell1.subTitleLbl.text = deal.product_url
            cell1.mrpPriceLbl.text = String("M.R.P ₹ ") + deal.product_mrpprice!
            cell1.priceLbl.text = String("M.R.P ₹ ") + deal.product_price!
            let imgstr = deal.product_image
            let str = imgpath! + imgstr!
            if str == ""{
                
            }
            let url = URL(string: str)
            if url != nil{
                cell1.categoryImgView?.kf.setImage(with: url, placeholder: UIImage(named:"flower.jpeg"), options:  [.scaleFactor(UIScreen.main.scale)],
                                                   progressBlock: { receivedSize, totalSize in
                                                    
                },completionHandler: { image, error, cacheType, imageURL in
                    
                })
            }
            else{
                let imgUrl = URL(string: (str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                if  imgUrl != nil {
                    cell1.categoryImgView?.kf.indicatorType = .activity
                    cell1.categoryImgView?.kf.setImage(with: imgUrl, placeholder:nil)
                }
            }
            return cell1
        }
        if collectionView == exclusiveCollectionView{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"LCExclusiveCollectionViewCell", for: indexPath) as! LCExclusiveCollectionViewCell
            cell1.bgView.layer.borderColor = AppColors.appGradientlightColor.cgColor
            cell1.bgView.layer.borderWidth = 1
            var exclusive : DashboardM = DashboardM()
            exclusive = exclusiveArr[indexPath.row]
            cell1.titleLbl.text = exclusive.product_name
            cell1.subTitleLbl.text = exclusive.product_url
            cell1.mrpPriceLbl.text = String("M.R.P ₹ ") + exclusive.product_mrpprice!
            cell1.priceLbl.text = String("M.R.P ₹ ") + exclusive.product_price!
            let imgstr = exclusive.product_image
            let str = imgpath! + imgstr!
            if str == ""{
                
            }
            let url = URL(string: str)
            if url != nil{
                cell1.categoryImgView?.kf.setImage(with: url, placeholder: UIImage(named:"flower.jpeg"), options:  [.scaleFactor(UIScreen.main.scale)],
                                                   progressBlock: { receivedSize, totalSize in
                                                    
                },completionHandler: { image, error, cacheType, imageURL in
                    
                })
            }
            else{
                let imgUrl = URL(string: (str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                if  imgUrl != nil {
                    cell1.categoryImgView?.kf.indicatorType = .activity
                    cell1.categoryImgView?.kf.setImage(with: imgUrl, placeholder:nil)
                }
            }
            return cell1
        }
        if collectionView == bestSellingCollectionView{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"BsetSellingCollectionViewCell", for: indexPath) as! BsetSellingCollectionViewCell
            cell1.bgView.layer.borderColor = AppColors.appGradientlightColor.cgColor
            cell1.bgView.layer.borderWidth = 1
            var best : DashboardM = DashboardM()
            best = bestArr[indexPath.row]
            cell1.titleLbl.text = best.product_name
            cell1.subTitleLbl.text = best.product_url
            cell1.mrpPriceLbl.text = String("M.R.P ₹ ") + best.product_mrpprice!
            cell1.priceLbl.text = String("M.R.P ₹ ") + best.product_price!
            let imgstr = best.product_image
            let str = imgpath! + imgstr!
            if str == ""{
                
            }
            let url = URL(string: str)
            if url != nil{
                cell1.categoryImgView?.kf.setImage(with: url, placeholder: UIImage(named:"flower.jpeg"), options:  [.scaleFactor(UIScreen.main.scale)],
                                                   progressBlock: { receivedSize, totalSize in
                                                    
                },completionHandler: { image, error, cacheType, imageURL in
                    
                })
            }
            else{
                let imgUrl = URL(string: (str.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                if  imgUrl != nil {
                    cell1.categoryImgView?.kf.indicatorType = .activity
                    cell1.categoryImgView?.kf.setImage(with: imgUrl, placeholder:nil)
                }
            }
            return cell1
        }
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"BsetSellingCollectionViewCell", for: indexPath) as! BsetSellingCollectionViewCell
        
        return cell1
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView {
            let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
            // var cat : DashboardM = DashboardM()
            let cat = categoryArr[indexPath.row]
            listingVC?.ListDict = cat
            self.navigationController?.pushViewController(listingVC!, animated: true)
        }
        if collectionView == dealOfTheDayCollectionView{
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailTableViewController") as? DetailTableViewController
            var deal : DashboardM = DashboardM()
            deal = dealArr[indexPath.row]
            detailVC?.detailsDict = deal
            self.navigationController?.pushViewController(detailVC!, animated: true)
            
        }
        if collectionView == exclusiveCollectionView{
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailTableViewController") as? DetailTableViewController
            var exclusive : DashboardM = DashboardM()
            exclusive = exclusiveArr[indexPath.row]
            detailVC?.detailsDict = exclusive
            self.navigationController?.pushViewController(detailVC!, animated: true)
        }
        if collectionView == bestSellingCollectionView{
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailTableViewController") as? DetailTableViewController
            var best : DashboardM = DashboardM()
            best = bestArr[indexPath.row]
            detailVC?.detailsDict = best
            self.navigationController?.pushViewController(detailVC!, animated: true)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.headerView.bounds.size.height
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        headerView.frame  = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 215)
        //           guestBtn.addTarget(self, action: #selector(DetailViewController.guestBtnAction(_:)), for: .touchUpInside)
        self.view.sendSubviewToBack(headerView)
        return headerView
        
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func CategoryAPI() {
        //            DispatchQueue.main.async {
        //                MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
        //                self.view.isUserInteractionEnabled = false
        //            }
        let params:Dictionary<String, Any> = [:]
        
        
        APICallExecutor.postRequestForURLString(true, categoryURL, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            //            DispatchQueue.main.async {
            //
            //                if self.navigationController != nil {
            //                    MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
            //                    self.view.isUserInteractionEnabled = true
            //                }
            //            }
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    self.imgpath =  dict?["img_path"] as? String
                    // let message = dict?["message"] as? String
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            let list : DashboardM = DashboardM()
                            self.categoryArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: self.imgpath!)
                        }
                        DispatchQueue.main.async  {
                            if self.categoryArr.count > 0{
                                self.categoryCollectionView .reloadData()
                                self.dealOfTheDayAPI()
                            }
                            else{
                            }
                        }
                    }
                    
                }
                    
                else {
                    let error = (result as? [String:Any])!
                    
                    print(error)
                    let msg = error["message"] as? String
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                    }
                }
            }
                
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    
    func dealOfTheDayAPI() {
        
        let params:Dictionary<String, Any> = [:]
        
        APICallExecutor.postRequestForURLString(true, dealproductURL, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    self.imgpath =  dict?["img_path"] as? String
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            // self.categoryArr.append(contentsOf: tempArr)
                            let list : DashboardM = DashboardM()
                            self.dealArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: self.imgpath!)
                        }
                        DispatchQueue.main.async  {
                            if self.dealArr.count > 0{
                                self.dealOfTheDayCollectionView .reloadData()
                                self.exclusiveAPI()
                                
                            }
                            else{
                                
                            }
                        }
                    }
                    
                }
                    
                else {
                    let error = (result as? [String:Any])!
                    
                    print(error)
                    let msg = error["message"] as? String
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                    }
                }
            }
                
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    
    func bannerAPI() {
        
        let params:Dictionary<String, Any> = [:]
        
        //                  params[key_fld_user_id] = "11"
        //                  params[key_fld_group_id] = "1"
        
        APICallExecutor.postRequestForURLString(true, bannerURL, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    self.imgpath =  dict?["img_path"] as? String
                    let message = dict?["message"] as? String
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            // self.categoryArr.append(contentsOf: tempArr)
                            let list : DashboardM = DashboardM()
                            self.bannerArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: self.imgpath!)
                            
                            
                        }
                        DispatchQueue.main.async  {
                            if self.bannerArr.count > 0{
                                self.pageControl.numberOfPages = self.bannerArr.count
                                self.bannerCollectionView .reloadData()
                                
                            }
                            else{
                                
                                
                            }
                        }
                    }
                    
                }
                    
                else {
                    let error = (result as? [String:Any])!
                    
                    print(error)
                    let msg = error["message"] as? String
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                    }
                }
            }
                
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    func exclusiveAPI() {
        
        let params:Dictionary<String, Any> = [:]
        
        //                  params[key_fld_user_id] = "11"
        //                  params[key_fld_group_id] = "1"
        
        APICallExecutor.postRequestForURLString(true, exclusiveURL, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    self.imgpath =  dict?["img_path"] as? String
                    let message = dict?["message"] as? String
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            // self.categoryArr.append(contentsOf: tempArr)
                            let list : DashboardM = DashboardM()
                            self.exclusiveArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: self.imgpath!)
                            
                            
                        }
                        DispatchQueue.main.async  {
                            if self.exclusiveArr.count > 0{
                                self.exclusiveCollectionView .reloadData()
                                self.bestAPI()
                            }
                            else{
                                
                                
                            }
                        }
                    }
                    
                }
                    
                else {
                    let error = (result as? [String:Any])!
                    
                    print(error)
                    let msg = error["message"] as? String
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                    }
                }
            }
                
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    
    func bestAPI() {
        
        let params:Dictionary<String, Any> = [:]
        
        //                  params[key_fld_user_id] = "11"
        //                  params[key_fld_group_id] = "1"
        
        APICallExecutor.postRequestForURLString(true, bestURL, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    self.imgpath =  dict?["img_path"] as? String
                    let message = dict?["message"] as? String
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            // self.categoryArr.append(contentsOf: tempArr)
                            let list : DashboardM = DashboardM()
                            self.bestArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: self.imgpath!)
                            
                        }
                        DispatchQueue.main.async  {
                            if self.bestArr.count > 0{
                                self.bestSellingCollectionView .reloadData()
                                self.bannerAPI()
                            }
                            else{
                                
                                
                            }
                        }
                    }
                    
                }
                    
                else {
                    let error = (result as? [String:Any])!
                    
                    print(error)
                    let msg = error["message"] as? String
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                    }
                }
            }
                
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    @IBAction func seeAll1Action(_ sender: Any) {
        //        let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
        //        // var cat : DashboardM = DashboardM()
        //        let cat = categoryArr[indexPath.row]
        //        listingVC?.ListDict = cat
        //        self.navigationController?.pushViewController(listingVC!, animated: true)
    }
    
    @IBAction func seeAll2Action(_ sender: Any) {
        let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
        // var cat : DashboardM = DashboardM()
        //        let deal = dealArr[indexPath.row]
        //        listingVC?.ListDict = cat
        self.navigationController?.pushViewController(listingVC!, animated: true)
    }
    @IBAction func seeAll3Action(_ sender: Any) {
        let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
        // var cat : DashboardM = DashboardM()
        //        let deal = dealArr[indexPath.row]
        //        listingVC?.ListDict = cat
        self.navigationController?.pushViewController(listingVC!, animated: true)
        
    }
    @IBAction func seeAll4Action(_ sender: Any) {
        let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
        // var cat : DashboardM = DashboardM()
        //        let deal = dealArr[indexPath.row]
        //        listingVC?.ListDict = cat
        self.navigationController?.pushViewController(listingVC!, animated: true)
    }
    
    
    @IBAction func cartBtnAction (_ sender: UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    
    @IBAction func searchBtnAction (_ sender: UIButton){
        let ListVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(ListVC, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK: UIScrollViewDelegate
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        print(pageNumber)
        //           if pageNumber == 2{
        //               Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
        //                  let loginVC =  self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        //                         self.navigationController?.pushViewController(loginVC!, animated: true)
        //
        //                   }
        //           }
    }
    
    @objc func sideMenuAction(_ sender:AnyObject){
        
    }
    
    @objc func searchButtonClicked(_ sender:AnyObject){
        self.navigationController?.popViewController(animated: true)
    }
    //    @objc func addButtonClicked(_ sender:AnyObject){
    //    self.navigationController?.popViewController(animated: true)
    //    }
}




//func customNavigationBar(){
//       self.navigationController?.navigationBar.isHidden = false
//       let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
//       fixedSpace.width = 20.0
//       
//       let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//       menuButton.setImage(UIImage.init(named: "menu"), for: .normal)
//       menuButton.addTarget(self, action: #selector(DashboardTableViewController.sideMenuAction(_ :)), for: .touchUpInside)
//       let leftItem1 = UIBarButtonItem()
//       leftItem1.customView = menuButton
//       
//       //        let labelTitle = UILabel()
//       //        labelTitle.text = ""
//       //        labelTitle.font = UIFont.init(name: "Poppins-Regular", size: 21.0)
//       //        let leftItem2 = UIBarButtonItem()
//       //        leftItem2.customView = labelTitle
//       //        self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
//       
//       let buttonleft = UIButton()
//       buttonleft.setImage(UIImage.init(named: "LYFECURE-1"), for: .normal)
//       let leftItem2 = UIBarButtonItem()
//       leftItem2.customView = buttonleft
//       self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
//       
//       
//       //        let buttonAdd = UIButton()
//       //        buttonAdd.setImage(UIImage.init(named: "search"), for: .normal)
//       //        buttonAdd.addTarget(self, action: #selector(DashboardTableViewController.addButtonClicked(_:)), for: .touchUpInside)
//       //        let rightItem1 = UIBarButtonItem()
//       //        rightItem1.customView = buttonAdd
//       
//       let buttonSearch = UIButton()
//       buttonSearch.setImage(UIImage.init(named: "cart"), for: .normal)
//       buttonSearch.addTarget(self, action: #selector(DashboardTableViewController.searchButtonClicked(_:)), for: .touchUpInside)
//       let rightItem2 = UIBarButtonItem()
//       rightItem2.customView = buttonSearch
//       
//       self.navigationItem.rightBarButtonItems = [fixedSpace,rightItem2]
//       
//       self.navigationController?.navigationBar.shouldRemoveShadow(true)
//       self.navigationController?.navigationBar.barTintColor = .white//UIColor.init(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1.0)
//       
//   }
