//
//  LCExclusiveCollectionViewCell.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 03/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class LCExclusiveCollectionViewCell: UICollectionViewCell {
    @IBOutlet var categoryImgView : UIImageView!
       @IBOutlet var titleLbl : UILabel!
       @IBOutlet var subTitleLbl : UILabel!
       @IBOutlet var mrpPriceLbl : UILabel!
       @IBOutlet var priceLbl : UILabel!
      @IBOutlet var bgView : UIView!
}
