//
//  CategoryCollectionViewCell.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 03/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
     @IBOutlet var categoryImgView : UIImageView!
     @IBOutlet var titleLbl : UILabel!
}
