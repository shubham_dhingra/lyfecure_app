//
//  DashboardViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 06/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
@IBOutlet weak var navigationView : UIView!
    
var layoutDone: Bool = false
//var controllervc = DashboardTableViewController()
@IBOutlet weak var mainView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationView.frame = CGRect(x:0,y:UIApplication.shared.statusBarFrame.height, width: view.frame.size.width, height:self.navigationController!.navigationBar.frame.height+20)
             self.navigationController?.view.addSubview(navigationView)
             self.navigationController?.view.bringSubviewToFront(navigationView)
        self.navigationView.backgroundColor = AppColors.appGreen
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
             super.viewDidLayoutSubviews()
             
//             if !layoutDone {
//               controllervc = storyboard!.instantiateViewController(withIdentifier: "DashboardTableViewController") as! DashboardTableViewController
//               displayContentController(content: controllervc)
//              
//                 layoutDone = true
//             }
         }
     func displayContentController(content: UIViewController) {
            addChild(content)
            content.view.frame = self.view.bounds
            self.view.addSubview(content.view)
    //        content.didMove(toParent: self)
        }
        func hideContentController(content: UIViewController) {
            content.willMove(toParent: nil)
            content.view.removeFromSuperview()
            content.removeFromParent()
        }

}
