//
//  CartVC.swift
//  LyfeCure_App
//
//  Created by Ruchi EL on 04/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class CartVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var CartTableView : UITableView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var footerView : UIView!
    @IBOutlet weak var searchBtn : UIButton!
    @IBOutlet weak var DeleteCartBtn : UIButton!
    @IBOutlet weak var totalPriceLbl : UILabel!
    @IBOutlet weak var qtyLbl : UILabel!
    @IBOutlet weak var DeliveryChargeLbl : UILabel!
    @IBOutlet weak var TotalPayableLbl : UILabel!
    @IBOutlet weak var PlaceOrderBtn : UIButton!
    @IBOutlet weak var sideMenuBtn : UIButton!
    var cartListArr  = [CartM]()
    var SelectedIdx :Int = 0
    var allCellsText : String!
    var dict : [String:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        CartListingAPI()
        PlaceOrderBtn?.isHidden = true
        PlaceOrderBtn.layer.cornerRadius = 25
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    /* MARK : TableView Delegate */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartListArr.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath as IndexPath) as! CartTableViewCell
        cell.mainView!.layer.borderColor = UIColor.clear.cgColor
        cell.mainView!.layer.cornerRadius = 8
        cell.mainView!.layer.shadowColor = UIColor(red: 238.0/255, green: 239.0/255, blue: 238.0/255, alpha: 1.0).cgColor
        cell.mainView!.layer.shadowOpacity = 0.3
        cell.mainView!.layer.shadowOffset = CGSize(width: 2.0, height: 10)
        cell.mainView!.layer.shadowRadius = 10
        cell.imagebackgroundView!.layer.borderColor = UIColor(red: 238/255.0, green: 239/255.0, blue: 238/255.0, alpha: 1.0).cgColor
        cell.imagebackgroundView!.layer.borderWidth = 1
        cell.editCartTxtField.delegate = self
        cell.cartDeleteBtn.tag = indexPath.row
        cell.cartUpdateBtn.tag = indexPath.row
        var cart : CartM = CartM()
        cart = cartListArr[indexPath.row]
        cell.lblTitle?.text = cart.product_name
        cell.lblSubtitle?.text = cart.product_type
        cell.lblOldPrice?.text = String("M.R.P ₹ ") + cart.product_mrpprice!
        cell.lblNewPrice?.text = String("M.R.P ₹ ") + cart.product_price!
        cell.editCartTxtField.text = cart.product_quantity
        // cell.editCartTxtField.text = "\(cart.product_quantity ?? 0)"
        
        allCellsText = cell.editCartTxtField.text
        cell.cartUpdateBtn.addTarget(self, action: #selector(editCartAction(_:)), for: .touchUpInside)
        cell.cartDeleteBtn.addTarget(self, action: #selector(deleteCartAction(_:)), for: .touchUpInside)
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        allCellsText.append(textField.text!)
        
    }
    @objc func editCartAction(_ sender: UIButton){
        
        let index = IndexPath(row: 0, section: 0)
        let cell: CartTableViewCell = self.CartTableView.cellForRow(at: index) as! CartTableViewCell
        allCellsText = cell.editCartTxtField.text!
        SelectedIdx =  sender.tag
        let indexP = NSIndexPath(item: SelectedIdx, section: 0)
        print("indexP",indexP as Any)
        SelectedIdx = sender.tag
        let cardDictionary = cartListArr[SelectedIdx]
        let userid = cardDictionary.user_id
        let productid = cardDictionary.product_id
        CartUpdateAPI(productQty: allCellsText!, userid: userid!, productId: productid!)
        cell.editCartTxtField.resignFirstResponder()
    }
    @objc func deleteCartAction(_ sender: UIButton){
        SelectedIdx =  sender.tag
        let indexP = NSIndexPath(item: SelectedIdx, section: 0)
        print("indexP",indexP as Any)
        SelectedIdx = sender.tag
        let cardDictionary = cartListArr[SelectedIdx]
        let userid = cardDictionary.user_id
        let productid = cardDictionary.product_id
        CartDeleteAPI(idx: SelectedIdx, userid: userid!, productId: productid!)
        
    }
    @IBAction func searchBtnAction (_ sender :UIButton){
        
    }
    
    @IBAction func cartAction (_ sender : UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    @IBAction func searchAction (_ sender : UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func placeOrderAction(_ sender: Any) {
        let checkOutVC =  self.storyboard?.instantiateViewController(withIdentifier: "CheckoutTVC") as? CheckoutTVC
        checkOutVC?.cartDict = dict
        self.navigationController?.pushViewController(checkOutVC!, animated: true)
    }
    func CartListingAPI() {
        
        var params:Dictionary<String, Any> = [:]
        
        let data = UserDefaults.standard.object(forKey: "logindict") as! NSData
        let object = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! [String: String]
        print(object)
        let userid = object["user_id"]
        let groupid = object["user_type"]
        params[key_fld_user_id] = "11"
        params[key_fld_group_id] = groupid
        
        APICallExecutor.postRequestForURLString(true, catlistingUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            
            if done {
                
                if result  != nil {
                    self.dict = (result as? [String:Any])!
                    let grandtotal = self.dict["grand_total"] as? String
                    let shippingcharges = self.dict["shipping_charges"] as? String
                    
                    if let statusCode = self.dict["statusCode"] as? Int {
                        if statusCode == 404 {
                             DispatchQueue.main.async  {
                                
                                self.totalPriceLbl.text = String("₹ 0")
                                self.DeliveryChargeLbl.text = String("₹ 0")
                                self.TotalPayableLbl.text = String("₹ 0")
                            //                                    let qty = cart.product_quantity
                                self.qtyLbl.text = "Price (0) items)"
                                self.PlaceOrderBtn?.isHidden = true
                                self.cartListArr  = []
                                self.CartTableView.reloadData()
                            }
                        }
                        
                    }
                    
                    if let tempArr = self.dict["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            let list : CartM = CartM()
                            self.cartListArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                            
                            DispatchQueue.main.async  {
                                if self.cartListArr.count > 0{
                                    self.totalPriceLbl.text = String("₹ ") + grandtotal!
                                    self.DeliveryChargeLbl.text = String("₹ ") + shippingcharges!
                                    self.TotalPayableLbl.text = String("₹ ") + grandtotal!
                                    self.PlaceOrderBtn.isHidden = false
                                    //                                    let qty = cart.product_quantity
                                    self.qtyLbl.text = "Price (\(self.cartListArr.count) items)"
                                    // self.qtyLbl.text = String("Price(") + qty! + String("items)")
                                    self.CartTableView .reloadData()
                                
                                    
                                }
                                else{
                                    self.cartListArr  = []
                                    self.CartTableView.reloadData()
                                    self.totalPriceLbl.text = String("₹ 0")
                                    self.DeliveryChargeLbl.text = String("₹ 0")
                                    self.TotalPayableLbl.text = String("₹ 0")
                                    //                                    let qty = cart.product_quantity
                                    self.qtyLbl.text = "Price (0) items)"
                                    // self.qtyLbl.text = String("Price(") + qty! + String("items)")
                                }
                            }
                        }
                    }
                        
                    else {
                        let error = (result as? [String:Any])!
                        
                        print(error)
                        let msg = error["message"] as? String
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
        
    }
    
    
    func CartDeleteAPI(idx : NSInteger, userid : String, productId : String)  -> Void {
        var params:Dictionary<String, Any> = [:]
        
        params[key_fld_user_id] = userid
        params[key_fld_product_id] = productId
        
        APICallExecutor.postRequestForURLString(true, cartremoveUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            
            if done {
                
                if result  != nil {
                    if let dict = result as? [String:Any]{
                        // if let dict =  result?["data"] as? Dictionary<String, Any> {
                        let msg =  dict["message"] as? String
                        let statuscode =  dict["statusCode"] as? Int
                        if statuscode  == 201 {
                            self.CartListingAPI()
                            //                        self.cartListArr.remove(at: self.SelectedIdx)
                            //                             DispatchQueue.main.async  {
                            //                                self.CartTableView.reloadData()
                            //                            }
                            //
                            //                        DispatchQueue.main.async  {
                            //                            if self.cartListArr.count > 0{
                            //                                self.CartTableView .reloadData()
                            //                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            //
                            //                            }
                            //                            else{
                            //
                            //
                            //                            }
                            //                        }
                        }
                            
                        else {
                            let error = (result as? [String:Any])!
                            
                            print(error)
                            let msg = error["message"] as? String
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
                
            }
        }
    }
    
    func CartUpdateAPI(productQty : String, userid : String, productId : String)  -> Void {
        
        var params:Dictionary<String, Any> = [:]
        
        params[key_fld_user_id] = userid
        params[key_fld_product_id] = productId
        params[key_fld_product_qty] = productQty
        
        APICallExecutor.postRequestForURLString(true, catupdateUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            if done {
                
                if result  != nil {
                    if let dict = result as? [String:Any]{
                        let msg =  dict["message"] as? String
                        let statuscode =  dict["statusCode"] as? Int
                        if statuscode  == 201 {
                            self.CartListingAPI()
                            DispatchQueue.main.async  {
                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                        else {
                            let error = (result as? [String:Any])!
                            
                            print(error)
                            let msg = error["message"] as? String
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                    }
                }
            }
        }
        
    }
    
}
