//
//  CheckoutTVC.swift
//  LyfeCure_App
//
//  Created by Ruchi EL on 04/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
import EZSwiftExtensions


class CheckoutTVC: UITableViewController,UIGestureRecognizerDelegate,ShippingDelegate {
    func passAddress(dict: ShippingM) {
        print(dict)
       // self.deliverToNameLbl.text = dict.shipping_name + (dict.shipping_address?.description ?? "")
        self.deliverToNameLbl.text = dict.shipping_name + "\n" + dict.shipping_address + "\n" + dict.shipping_pincode + "\n" + dict.shipping_city + "\n" + dict.shipping_state
        self.shippingId = dict.shipping_id
    }
    

    @IBOutlet weak var coupanTextField: UITextField!
    @IBOutlet var headerView : UIView!
    @IBOutlet weak var checkOutBtn: UIButton!
    @IBOutlet weak var changeAddressBtn: UIButton!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var deliveryChargeLbl: UILabel!
    @IBOutlet weak var deliverToAddressLbl: UILabel!
    @IBOutlet weak var deliverToNameLbl: UILabel!
    @IBOutlet weak var orderPriceLbl: UILabel!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var DeliveryView: UIView!
     @IBOutlet weak var coupanView: UIView!
    
     var cartDict : [String:Any] = [:]
    var shippingId : String? = ""
    let appdelegate :AppDelegate! = UIApplication.shared.delegate as? AppDelegate
     
    override func viewDidLoad() {
        super.viewDidLoad()
      self.coupanView!.layer.borderColor = UIColor.clear.cgColor
      self.coupanView!.layer.cornerRadius = 8
      self.coupanView!.layer.shadowColor = UIColor.lightGray.cgColor
      self.coupanView!.layer.shadowOpacity = 0.3
      self.coupanView!.layer.shadowOffset = CGSize(width: 2.0, height: 10)
      self.coupanView!.layer.shadowRadius = 10
        
        self.DeliveryView!.layer.borderColor = UIColor.clear.cgColor
        self.DeliveryView!.layer.cornerRadius = 8
        self.DeliveryView!.layer.shadowColor = UIColor.lightGray.cgColor
        self.DeliveryView!.layer.shadowOpacity = 0.3
        self.DeliveryView!.layer.shadowOffset = CGSize(width: 2.0, height: 10)
        self.DeliveryView!.layer.shadowRadius = 10
        self.coupanTextField.layer.borderColor = UIColor.lightGray.cgColor
        self.coupanTextField.layer.borderWidth = 1
        checkOutBtn.layer.cornerRadius = 25
        setData()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_ :)))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
                         
        }
        @objc func handleTap (_ sender:UITapGestureRecognizer){
            coupanTextField.resignFirstResponder()
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
//        customNavigationBar()
    }
    
    
    func setData (){
       let orderPrice  = self.cartDict["grand_total"] as? String
        self.orderPriceLbl.text = String("₹") + orderPrice!
        let deliveryPrice = self.cartDict["shipping_charges"] as? String
        self.deliveryChargeLbl.text = String("₹") + deliveryPrice!
        self.totalPriceLbl.text = String("₹") + orderPrice!
    }

    @IBAction func SelectAddressAction(_ sender : UIButton){
           let shippingVC =  self.storyboard?.instantiateViewController(withIdentifier: "ShippingViewController") as? ShippingViewController
           shippingVC?.delegate = self
           self.navigationController?.pushViewController(shippingVC!, animated: true)
       }
   @IBAction func AddAddressAction(_ sender : UIButton){
             appdelegate.fromvc = "addaddress"
             let shippingVC =  self.storyboard?.instantiateViewController(withIdentifier: "AddShippingAddressTableViewController") as? AddShippingAddressTableViewController
             self.navigationController?.pushViewController(shippingVC!, animated: true)
         }
    func customNavigationBar(){
            self.navigationController?.navigationBar.isHidden = false
            let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
                   
                   let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                   menuButton.setImage(UIImage.init(named: "back"), for: .normal)
                   menuButton.addTarget(self, action: #selector(CheckoutTVC.backBtnAction(_ :)), for: .touchUpInside)
                   let leftItem1 = UIBarButtonItem()
                   leftItem1.customView = menuButton
                   
//                   let labelTitle = UILabel()
//                   labelTitle.text = ""
//                   labelTitle.font = UIFont.init(name: "Poppins-Regular", size: 21.0)
//                   let leftItem2 = UIBarButtonItem()
//                   leftItem2.customView = labelTitle
//                   self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
        
                   let buttonleft = UIButton()
                   buttonleft.setImage(UIImage.init(named: "LYFECURE-1"), for: .normal)
                   let leftItem2 = UIBarButtonItem()
                   leftItem2.customView = buttonleft
                   self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
                   
//                   let buttonAdd = UIButton()
//                   buttonAdd.setImage(UIImage.init(named: "search"), for: .normal)
//                   buttonAdd.addTarget(self, action: #selector(DetailTableViewController.addButtonClicked(_:)), for: .touchUpInside)
//                   let rightItem1 = UIBarButtonItem()
//                   rightItem1.customView = buttonAdd
                   
                   let buttonSearch = UIButton()
                   buttonSearch.setImage(UIImage.init(named: "cart"), for: .normal)
                   buttonSearch.addTarget(self, action: #selector(DetailTableViewController.cartBtnAction(_:)), for: .touchUpInside)
                   let rightItem2 = UIBarButtonItem()
                   rightItem2.customView = buttonSearch
                   
                   self.navigationItem.rightBarButtonItems = [fixedSpace, rightItem2]
                   
                   self.navigationController?.navigationBar.shouldRemoveShadow(true)
                   self.navigationController?.navigationBar.barTintColor = .white//UIColor.init(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1.0)
                   
        }
        
    
    @objc func backBtnAction(_ sender:AnyObject){
           self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func CheckoutAction(_ sender: UIButton){
        if shippingId?.count == 0 {
            TaskExecutor.showTost(withMessage: "Select Shipping Address", onViewController: self)
            return
        }
//        navigateToThanksVc(orderNo: "120", orderMessage: "Order Placed Successfully")
       CheckOutAPI()
    }
    
    func navigateToCheckOutVc() {
        
    }
}


extension CheckoutTVC {
    func CheckOutAPI() {
           
           var params:Dictionary<String, Any> = [:]
            let dict = self.cartDict
            guard let productDict = dict["data"] as? [[String : Any]] else {
                return
            }
        let firstProduct = productDict[0]
        params[key_fld_user_id] = firstProduct[key_fld_user_id] as? String
        params[key_shipping_id] = self.shippingId?.toInt()
//        params[key_fld_order_price] = (dict["grand_total"] as? String)?.toInt()
//        params[key_fld_order_quantity] = (firstProduct["fld_product_qty"] as? String)?.toInt()
//        params[key_fld_product_id] = (firstProduct[key_fld_product_id] as? String)?.toInt()
        params["fld_coupon_code"] = ""
        params["fld_coupon_percent"] = ""
           
           
           APICallExecutor.postRequestForURLString(true, saveorderUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
               
               
               if done {
                   
                   if result  != nil {
                       let Dictionary = (result as! [String:Any])
                       let statuscode = Dictionary["statusCode"] as? Int
                       
                       if statuscode == 201 {
                           let dict = result as? [String:Any]
                           let message = dict!["message"] as? String
                           let orderNo = dict!["OrderNo"] as? Int
                        ez.runThisInMainThread {
                            self.navigateToThanksVc(orderNo: orderNo?.toString, orderMessage: message)
                        }
                       }
                       else {
                           let error = (result as? [String:Any])!
                           
                           print(error)
                           let msg = error["message"] as? String
                           DispatchQueue.main.async {
                               TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                           }
                       }
                   }
               }
               else {
                   DispatchQueue.main.async {
                       TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                   }
               }
           }
       }
    
    func navigateToThanksVc(orderNo : String? , orderMessage : String?) {
        let thanksVC = self.storyboard?.instantiateViewController(identifier: "ThanksCheckOutViewController") as? ThanksCheckOutViewController
        thanksVC?.orderNo = orderNo
        thanksVC?.orderMessage = orderMessage
        self.navigationController?.pushViewController(thanksVC!, animated: true)
    }
}
