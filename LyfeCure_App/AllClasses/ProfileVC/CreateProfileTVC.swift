//
//  CreateProfileTVC.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 05/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class CreateProfileTVC : UITableViewController,UITextFieldDelegate{
   
    
    @IBOutlet var WholesaleBtn : UIButton!
    @IBOutlet var AgentBtn : UIButton!
    @IBOutlet var RetailerBtn : UIButton!
    @IBOutlet var submitBtn : UIButton!
    @IBOutlet var companyNameTxtfield : UITextField!
    @IBOutlet var druglicenseTxtfield : UITextField!
    @IBOutlet var contactnumberTxtfield : UITextField!
    @IBOutlet var gstnoTxtfield : UITextField!
    @IBOutlet var useremailTxtfield : UITextField!
    @IBOutlet var userTypeTextField : UITextField!
    
    var dict = UpdateProfoleM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.layer.cornerRadius = 10
         WholesaleBtn.isSelected = true
        setProfiledata()
    }

    func setProfiledata(){
           self.companyNameTxtfield.text = dict.company_name
           self.contactnumberTxtfield.text = dict.contact_number
           self.gstnoTxtfield.text = dict.gst_no
           self.useremailTxtfield.text = dict.user_email
           self.druglicenseTxtfield.text = dict.drug_license
           self.userTypeTextField.text = dict.user_type
    }
    
    @IBAction func back(_ sender: Any) {
             self.navigationController?.popViewController(animated: true)
      }
    
   func updateProfileAPI() {
   //            DispatchQueue.main.async {
   //                MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
   //                self.view.isUserInteractionEnabled = false
   //            }
               var params:Dictionary<String, Any> = [:]
       
               params[key_fld_user_id] = "12"
               params[key_fld_company_name] = self.companyNameTxtfield.text
               params[key_fld_drug_license] = self.druglicenseTxtfield.text
               params[key_fld_contact_number] = self.contactnumberTxtfield.text
               params[key_fld_gst_no] = self.gstnoTxtfield.text
               params[key_fld_user_email] = self.useremailTxtfield.text
               params[key_fld_user_type] = self.userTypeTextField.text
               
       
               APICallExecutor.postRequestForURLString(true, profileUpdateUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
                   
       //            DispatchQueue.main.async {
       //
       //                if self.navigationController != nil {
       //                    MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
       //                    self.view.isUserInteractionEnabled = true
       //                }
       //            }
                   
                   if done {
                       
                       if result  != nil {
                         let Dictionary = (result as! [String:Any])
                         let statuscode = Dictionary["statusCode"] as? Int
                        // let status = Dictionary["status"] as? Int
                        
                         if statuscode == 201 {
                              let dict = result as? [String:Any]
                             let message = dict!["message"] as? String
                             DispatchQueue.main.async {
                               //  TaskExecutor.showTost(withMessage: message!, onViewController: self)
                             //  Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
//                                 let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "ProfileTableViewController") as? ProfileTableViewController
//                             self.navigationController?.pushViewController(dashboardVC!, animated: true)
                                 self.navigationController?.popViewController(animated: true)
                           
                              //   }
                                 
                             }
                            
                        }
                           else {
                                let error = (result as? [String:Any])!
                              
                               print(error)
                               let msg = error["message"] as? String
                               DispatchQueue.main.async {
                                   TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                               }
                           }
                       }
                   }
                   else {
                       DispatchQueue.main.async {
                           TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                       }
                   }
               }
           }
    @IBAction func submitBtn(_ sender : UIButton){
            self.view.endEditing(true)
            updateProfileAPI()
    }
    @IBAction func wholesaleBtn(_ sender : UIButton){
            WholesaleBtn.setImage(UIImage(named: "radio_check"), for: UIControl.State.normal)
         AgentBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
         RetailerBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
           WholesaleBtn.isSelected = true
           RetailerBtn.isSelected = false
           AgentBtn.isSelected = false
    }
    @IBAction func agentBtn(_ sender : UIButton){
            WholesaleBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
            AgentBtn.setImage(UIImage(named: "radio_check"), for: UIControl.State.normal)
            RetailerBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
                     WholesaleBtn.isSelected = false
                    RetailerBtn.isSelected = false
                    AgentBtn.isSelected = true
    }
    @IBAction func retailBtn(_ sender : UIButton){
       WholesaleBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
        AgentBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
        RetailerBtn.setImage(UIImage(named: "radio_check"), for: UIControl.State.normal)
        WholesaleBtn.isSelected = false
        RetailerBtn.isSelected = true
        AgentBtn.isSelected = false
    }
}

extension CreateProfileTVC {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == companyNameTxtfield {
            contactnumberTxtfield.becomeFirstResponder()
            return false
        }
        
        if textField == contactnumberTxtfield {
            gstnoTxtfield.becomeFirstResponder()
            return false
        }
        
        if textField == gstnoTxtfield {
            useremailTxtfield.becomeFirstResponder()
            return false
        }
        
         if textField == useremailTxtfield {
            druglicenseTxtfield.becomeFirstResponder()
            return false
        }
        
        if textField == druglicenseTxtfield {
            druglicenseTxtfield.resignFirstResponder()
            return true
        }
        return true
    }
}
