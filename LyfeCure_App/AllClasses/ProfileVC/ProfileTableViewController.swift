//
//  ProfileTableViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController,UITextFieldDelegate {
    
    
    @IBOutlet var txtfield1 : UITextField!
    @IBOutlet var txtfield2 : UITextField!
    @IBOutlet var txtfield3 : UITextField!
    @IBOutlet var txtfield4 : UITextField!
    @IBOutlet var txtfield5 : UITextField!
    @IBOutlet var txtfield6 : UITextField!
    @IBOutlet var btnEdit : UIButton!
    @IBOutlet var lblHeading : UILabel!
    
    
    var profileInfoArr = [AnyObject]()
    var profileDict = UpdateProfoleM()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        btnEdit.layer.cornerRadius = 25
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ProfileAPI()
    }

     func ProfileAPI() {
       //            DispatchQueue.main.async {
       //                MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
       //                self.view.isUserInteractionEnabled = false
       //            }
                   var params:Dictionary<String, Any> = [:]
           
                   params[key_fld_user_id] = "12"
                   
           
                   APICallExecutor.postRequestForURLString(true, profileInfoUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
                       
           //            DispatchQueue.main.async {
           //
           //                if self.navigationController != nil {
           //                    MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
           //                    self.view.isUserInteractionEnabled = true
           //                }
           //            }
                       
                       if done {
                           
                           if result  != nil {
                               let dict = result as? [String:Any]
                               self.profileInfoArr = []
                               if let tempArr = dict?["data"] as? Array<AnyObject>
                                   {
                                       if tempArr.count > 0 {
                                           self.profileInfoArr.append(contentsOf: tempArr)

                                           DispatchQueue.main.async  {
                                               if self.profileInfoArr.count > 0{
                                                let dict = self.profileInfoArr[0]
                                                print(dict)
                                                let profileDict:UpdateProfoleM = UpdateProfoleM()
                                                profileDict.setValue(fromProfileInfo: dict as! Dictionary<String, Any>)
                                                self.profileDict = profileDict
                                                self.setProfiledata(dict: profileDict)
                                                print(profileDict)
                                               }
                                               else{
                                                 

                                               }
                                           }
                                
                                   }
                                   
                               }
                                   
                               else {
                                    let error = (result as? [String:Any])!
                                  
                                   print(error)
                                   let msg = error["message"] as? String
                                   DispatchQueue.main.async {
                                       TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                                   }
                               }
                           }
                       }
                       else {
                           DispatchQueue.main.async {
                               TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                           }
                       }
                   }
               }
    
    func setProfiledata(dict : UpdateProfoleM){
        self.txtfield1.text = dict.company_name
        self.txtfield2.text = dict.contact_number
        self.txtfield3.text = dict.gst_no
        self.txtfield4.text = dict.user_email
        self.txtfield5.text = dict.drug_license
        self.txtfield6.text = dict.user_type
    }
    
    @IBAction func back(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEdit(_ sender : Any){
        
            let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "CreateProfileTVC") as? CreateProfileTVC
            dashboardVC?.dict = self.profileDict
            self.navigationController?.pushViewController(dashboardVC!, animated: true)
    }
    
    }

