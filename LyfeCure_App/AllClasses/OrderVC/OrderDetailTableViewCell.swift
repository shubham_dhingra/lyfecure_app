//
//  OrderDetailTableViewCell.swift
//  LyfeCure_App
//
//  Created by SHUBHAM DHINGRA on 2/22/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
import Foundation

class OrderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMedicineName : UILabel!
    @IBOutlet weak var lblMedicineType : UILabel!
    @IBOutlet weak var lblQuantity : UILabel!
    @IBOutlet weak var lblMarketPrice : UILabel!
    @IBOutlet weak var imgMedicine : UIImageView!
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var imgBorderView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
