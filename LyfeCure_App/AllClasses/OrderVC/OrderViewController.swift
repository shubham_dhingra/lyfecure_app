//
//  OrderViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
import EZSwiftExtensions


class OrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var OrderTableView : UITableView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var searchBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var sideMenuBtn : UIButton!
    @IBOutlet var btnOngoing : UIButton!
    @IBOutlet var btnPast : UIButton!
    @IBOutlet var btnCompleted : UIButton!
    
    @IBOutlet var view1 : UIView!
    @IBOutlet var view2 : UIView!
    @IBOutlet var view3 : UIView!
    
    
    var OrderArr  = [OrderListingM]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        OrderAPI()
        
    }
    /* MARK : TableView Delegate */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrderArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath as IndexPath) as! OrderTableViewCell
        cell.mainView!.layer.cornerRadius = 8
        cell.mainView!.addBorder(width: 1.0, color: UIColor.init(netHex: 0xE7E2E1))
        let order = self.OrderArr[indexPath.row]
            cell.lblOrderID?.text = "Order Id : \(order.order_id ?? "")"
            cell.lblOrderDate?.text = "Order Date : \(order.order_date ?? "")"
            cell.lblOrderValue?.text = "₹\(order.order_amt ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let orderDetailVc = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
         orderDetailVc.model = self.OrderArr[indexPath.row]
         ez.topMostVC?.pushVC(orderDetailVc)
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    
    
    @IBAction func cart(_ sender: Any) {
        
    }
    
    @IBAction func CartAction(_ sender: Any) {
        
    }
    
    @IBAction func SearchAction(_ sender: Any) {
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
    }
    
    @IBAction func OngoingOrderAction(_ sender: Any) {
        
        btnOngoing.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        btnOngoing.titleLabel?.font =  UIFont(name: "OpenSans-Bold", size: 17)
        view1.backgroundColor = AppColors.appGradientGreen
        btnPast.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        btnPast.titleLabel?.font =  UIFont(name: "OpenSans-Regular", size: 17)
        view2.backgroundColor = UIColor.clear
        btnCompleted.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        btnCompleted.titleLabel?.font =  UIFont(name: "OpenSans-Regular", size: 17)
        view3.backgroundColor = UIColor.clear
        
    }
    
    @IBAction func PastOrderAction(_ sender: Any) {
        
        btnPast.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        btnPast.titleLabel?.font =  UIFont(name: "OpenSans-Bold", size: 17)
        view2.backgroundColor = AppColors.appGradientGreen
        
        btnCompleted.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        btnCompleted.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 17)
        view3.backgroundColor = UIColor.clear
        
        btnOngoing.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        btnOngoing.titleLabel?.font =  UIFont(name: "OpenSans-Regular", size: 17)
        view1.backgroundColor = UIColor.clear
    }
    
    @IBAction func CompletedOrderAction(_ sender : Any){
       
        btnCompleted.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        btnCompleted.titleLabel?.font =  UIFont(name: "OpenSans-Bold", size: 17)
        view3.backgroundColor = AppColors.appGradientGreen
        
        btnPast.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        btnPast.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 17)
        view2.backgroundColor = UIColor.clear
        
        btnOngoing.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        btnOngoing.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 17)
        view1.backgroundColor = UIColor.clear
    }
    
    @IBAction func cartAction(_ sender : UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    @IBAction func searchAction(_ sender : UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func OrderAPI() {
        //            DispatchQueue.main.async {
        //                MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
        //                self.view.isUserInteractionEnabled = false
        //            }
        var params:Dictionary<String, Any> = [:]
        
        params[key_fld_user_id] = "11"
//        params[key_fld_group_id] = "1"
        
        APICallExecutor.postRequestForURLString(true, orderListingUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
//                        DispatchQueue.main.async {
//
//                            if self.navigationController != nil {
//                                MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
//                                self.view.isUserInteractionEnabled = true
//                            }
//                        }
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    // if let dataDict =  dict?["data"] as? Dictionary<String, Any> {
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            let list : OrderListingM = OrderListingM()
                             self.OrderArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                            
                            DispatchQueue.main.async  {
                                if self.OrderArr.count > 0{
                                    self.OrderTableView.reloadData()
                                    
                                }
                                else{
                                    
                                    
                                }
                            }
                            //     }
                        }
                    }
                        
                    else {
                        let errors = ((result as! Dictionary<String, Any>)["errors"] as! Array<Any>)
                        print(errors)
                        
                        let msg: String = (errors[0] as! Dictionary<String, Any>)["message"] as! String
                        
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg, onViewController: self)
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
}
