//
//  OrderDetailViewController.swift
//  LyfeCure_App
//
//  Created by SHUBHAM DHINGRA on 2/22/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {

    @IBOutlet weak var lblOrderId : UILabel!
    @IBOutlet weak var lblOrderDate : UILabel!
    @IBOutlet weak var lblOrderTotal : UILabel!
    @IBOutlet weak var lblShippingFees : UILabel!
    @IBOutlet weak var lblTotal : UILabel!
    @IBOutlet weak var lblShippingAddress : UILabel!
    @IBOutlet weak var lblFirstDate : UILabel!
    @IBOutlet weak var lblSecondDate : UILabel!
    @IBOutlet weak var OrderTableView : UITableView!

    
    var model : OrderListingM?
    var OrdersDetailArr  = [OrderDetailM]()

    override func viewDidLoad() {
        super.viewDidLoad()
        orderDetailAPI()
    }

}

extension OrderDetailViewController {
    
    
    func orderDetailAPI() {
            var params:Dictionary<String, Any> = [:]
            guard let model = self.model , let orderId = model.order_id else {
                return
            }
            params[key_fld_user_id] = "11"
            params[key_fld_order_id] = orderId
            
            APICallExecutor.postRequestForURLString(true, orderDetailUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
                if done {
                    
                    if result  != nil {
                        let dict = result as? [String:Any]
                    if let dataDict =  dict?["data"] as? Dictionary<String, Any> {
                        
                        if let orderDetailsDict = dataDict["order_data"] as? Dictionary<String , Any>? {
                            let shippingDetail : OrderDetailM = OrderDetailM()
                            shippingDetail.setValue(fromListInfo: orderDetailsDict!)
                             DispatchQueue.main.async  {
                                self.setValueOnUI(dict : shippingDetail)
                            }
                        }
                        if let tempArr = dataDict["order_detail"] as? Array<AnyObject>
                            {
                            if tempArr.count > 0 {
                                let list : OrderDetailM = OrderDetailM()
                                 self.OrdersDetailArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                                
                                DispatchQueue.main.async  {
                                    if self.OrdersDetailArr.count > 0{
                                        self.OrderTableView.reloadData()
                                        
                                    }
                                    else{
                                        
                                        
                                    }
                                }
                                //     }
                            }
                        }
                    }
                            
                        else {
                            let errors = ((result as! Dictionary<String, Any>)["errors"] as! Array<Any>)
                            print(errors)
                            
                            let msg: String = (errors[0] as! Dictionary<String, Any>)["message"] as! String
                            
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg, onViewController: self)
                            }
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                    }
                }
            }
        }
    
    func setValueOnUI(dict : OrderDetailM?){
        
        if let dict = dict {
        lblOrderId?.text = "Order Id : \(/dict.order_id)"
        lblOrderDate?.text = "Order Date : \(/dict.order_date)"
        let shippingAddress = "\(/dict.shipping_name) \n \(/dict.shipping_address), \(/dict.shipping_landmark) \n \(/dict.shipping_city), \(/dict.shipping_state)\n \(/dict.shipping_country) \(/dict.shipping_pinCode)"
        lblShippingAddress?.text = shippingAddress
        lblOrderTotal?.text = "₹\(/dict.order_total?.toString)"
        lblShippingFees?.text = "₹\(/dict.order_shipping_total)"
        let totalAmt = (/dict.order_total) + (/dict.order_shipping_total?.toInt())
        lblTotal?.text =  "₹\(totalAmt.toString)"
        }
    }
    
    @IBAction func back(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
}

extension OrderDetailViewController : UITableViewDelegate , UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrdersDetailArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTableViewCell", for: indexPath as IndexPath) as! OrderDetailTableViewCell
        cell.mainView!.layer.cornerRadius = 8
        cell.mainView!.addBorder(width: 1.0, color: UIColor.init(netHex: 0xE7E2E1))
        cell.imgBorderView.layer.cornerRadius = 2
        cell.imgBorderView!.addBorder(width: 1.0, color: UIColor.init(netHex: 0xE7E2E1))
        
        let order = self.OrdersDetailArr[indexPath.row]
            cell.lblMedicineName?.text = "\(order.product_name ?? "")"
            cell.lblMedicineType?.text = "\(order.product_type ?? "")"
            cell.lblQuantity?.text = "Qty : \(order.order_quantity ?? "")"
            cell.lblMarketPrice?.text = "M.R.P \(order.order_price ?? "")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}
