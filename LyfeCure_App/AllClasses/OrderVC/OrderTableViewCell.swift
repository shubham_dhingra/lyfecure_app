//
//  OrderTableViewCell.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
     
    @IBOutlet weak var lblOrderDate : UILabel!
    @IBOutlet weak var lblOrderID : UILabel!
    @IBOutlet weak var lblOrderValue : UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var btnTrackOrder : UIButton!
    @IBOutlet weak var mainView : UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func btnCancelAction(_ sender : UIButton){
        
    }
    
    @IBAction func btnTrackOrderAction(_ sender : UIButton){
        
    }
    
    

}
