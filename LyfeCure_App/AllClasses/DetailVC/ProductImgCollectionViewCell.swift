//
//  ProductImgCollectionViewCell.swift
//  LyfeCure_App
//
//  Created by EL Group on 13/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ProductImgCollectionViewCell: UICollectionViewCell {
    @IBOutlet var productImgView : UIImageView!
}
