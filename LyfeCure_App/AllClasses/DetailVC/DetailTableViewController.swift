//
//  DetailTableViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet var productNameLbl:UILabel!
    @IBOutlet var productTypeLbl:UILabel!
    @IBOutlet var headerView : UIView!
    @IBOutlet var mainView : UIView!
    @IBOutlet var newPriceLbl:UILabel!
    @IBOutlet var oldPriceLbl:UILabel!
    @IBOutlet var offLbl:UILabel!
    @IBOutlet var cartCountLbl:UILabel!
    @IBOutlet var editCartTextField:UITextField!
    @IBOutlet var AddCartBtn:UIButton!
    @IBOutlet var cartBtn:UIButton!
    @IBOutlet var packingType:UILabel!
    @IBOutlet var productDescriptionLbl:UILabel!
    @IBOutlet var productImgCollectionView:UICollectionView!
    @IBOutlet var pageControl:UIPageControl!
    
    
    var productArr = [DetailM]()
    var detailsDict = DashboardM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // customNavigationBar()
         detailAPI()
       
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 0, left: productImgCollectionView.frame.size.width - 130, bottom: 0, right: 0)
//        layout.itemSize = CGSize(width: productImgCollectionView.bounds.size.width - 130 , height:130)
//        layout.scrollDirection = .horizontal
//        productImgCollectionView.setCollectionViewLayout(layout, animated: false)
       
        
        self.mainView!.layer.borderColor = UIColor.clear.cgColor
        self.mainView!.layer.cornerRadius = 8
        self.mainView!.layer.shadowColor = UIColor.lightGray.cgColor
        self.mainView!.layer.shadowOpacity = 0.3
        self.mainView!.layer.shadowOffset = CGSize(width: 2.0, height: 10)
        self.mainView!.layer.shadowRadius = 10
        
        self.AddCartBtn.layer.cornerRadius = 17
        cartCountLbl.layer.cornerRadius = 10
        cartCountLbl.layer.borderColor = UIColor.white.cgColor
        cartCountLbl.layer.borderWidth = 1
        cartCountLbl.text = "0"
        productImgCollectionView.reloadData()
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_ :)))
            tap.delegate = self
            view.addGestureRecognizer(tap)
            view.isUserInteractionEnabled = true
                  
        }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
//            customNavigationBar()
    }
    @objc func handleTap (_ sender:UITapGestureRecognizer){
        editCartTextField.resignFirstResponder()
    }
    @IBAction func addCartBtnAction (_ sender : UIButton){
        if editCartTextField.text == "" || editCartTextField.text == "Enter qty"{
            TaskExecutor.showTost(withMessage: "Please enter quantity", onViewController: self)
        }
        else {
            
            cartCountLbl.text = editCartTextField.text
            CartUpdateAPI(productQty: editCartTextField.text!, userid: "11", productId: detailsDict.product_id! , productPrice :  detailsDict.product_price!)
            TaskExecutor.showTost(withMessage: "Item add to cart", onViewController: self)
        }
    }
     override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
               return self.headerView.bounds.size.height

        }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       headerView.frame  = CGRect(x: 0, y: -30, width: self.tableView.frame.size.width, height: 70)
        cartBtn.addTarget(self, action: #selector(DetailTableViewController.cartBtnAction(_:)), for: .touchUpInside)

        return headerView

          }
        override func numberOfSections(in tableView: UITableView) -> Int {
            return  1
        }
        @objc func cartBtnAction (_ sender:UIButton){
            let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
            self.navigationController?.pushViewController(cartVC!, animated: true)
        }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier:"ProductImgCollectionViewCell", for: indexPath) as! ProductImgCollectionViewCell
       // var productImg: DashboardM = DashboardM()
           // productImg = productArr[indexPath.row]
        //  cell1.bannerImgView.image = category.image
        
        return cell1
    }
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
       let numberOfItems = CGFloat(productImgCollectionView.numberOfItems(inSection: section))
       let combinedItemWidth = (numberOfItems * flowLayout.itemSize.width) + ((numberOfItems - 1)  * flowLayout.minimumInteritemSpacing)
       let padding = (productImgCollectionView.frame.width - combinedItemWidth) / 2
       return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
   }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      productImgCollectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }
    func detailAPI() {
        var params:Dictionary<String, Any> = [:]
        
        params[key_fld_product_id] = detailsDict.product_id
        params[key_fld_group_id] = "1"
        
        APICallExecutor.postRequestForURLString(true, getdetailsURL, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            if done {
                
                if result  != nil {
                    if  let dict = result as? [String:Any]{
                        //let msg =  dict["message"] as? String
                        let statuscode =  dict["statusCode"] as? Int
                        if statuscode  == 201 {
                        if let dataDict =  dict["data"] as? Dictionary<String, Any> {
                        print(dataDict)

                                DispatchQueue.main.async  {
                                        let Dict:DetailM = DetailM()
                                     Dict.setValue(fromCartDetailInfo: dataDict )
                                     self.setdata(dict: Dict)
                                     print(Dict)
                                    }
        
                                }

                            }
                        
                    else {
                         let error = (result as? [String:Any])!
                       
                        print(error)
                        let msg = error["message"] as? String
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                        }
                    }
                }
            }
    }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    func setdata(dict : DetailM){
        self.productNameLbl.text = dict.product_name
        self.productTypeLbl.text = dict.product_type
        self.productDescriptionLbl.text = dict.product_descrip
        self.newPriceLbl.text  = String("M.R.P ₹ ") + dict.product_mrpprice!
        self.oldPriceLbl.text =  String("M.R.P ₹ ") + dict.product_price!
        let htmlText = dict.product_descrip
        self.productDescriptionLbl.attributedText = htmlText?.htmlToAttributedString
    }
    
    func customNavigationBar(){
                self.navigationController?.navigationBar.isHidden = false
                let fixedSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
                fixedSpace.width = 20.0
                
                let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                menuButton.setImage(UIImage.init(named: "back"), for: .normal)
                menuButton.addTarget(self, action: #selector(DetailTableViewController.backBtnAction(_ :)), for: .touchUpInside)
                let leftItem1 = UIBarButtonItem()
                leftItem1.customView = menuButton
                
                let labelTitle = UILabel()
                labelTitle.text = ""
                labelTitle.font = UIFont.init(name: "Poppins-Regular", size: 21.0)
                let leftItem2 = UIBarButtonItem()
                leftItem2.customView = labelTitle
                self.navigationItem.leftBarButtonItems = [leftItem1, fixedSpace,leftItem2]
                
                let buttonAdd = UIButton()
                buttonAdd.setImage(UIImage.init(named: "search"), for: .normal)
                buttonAdd.addTarget(self, action: #selector(DetailTableViewController.addButtonClicked(_:)), for: .touchUpInside)
                let rightItem1 = UIBarButtonItem()
                rightItem1.customView = buttonAdd
                
                let buttonSearch = UIButton()
                buttonSearch.setImage(UIImage.init(named: "cart"), for: .normal)
                buttonSearch.addTarget(self, action: #selector(DetailTableViewController.cartBtnAction(_:)), for: .touchUpInside)
                let rightItem2 = UIBarButtonItem()
                rightItem2.customView = buttonSearch
                
                self.navigationItem.rightBarButtonItems = [rightItem1,fixedSpace, rightItem2]
                
                self.navigationController?.navigationBar.shouldRemoveShadow(true)
                self.navigationController?.navigationBar.barTintColor = .white//UIColor.init(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1.0)
                
     }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func backBtnAction(_ sender:AnyObject){
            self.navigationController?.popViewController(animated: true)
    }
    @objc func addButtonClicked(_ sender:AnyObject){
           self.navigationController?.popViewController(animated: true)
    }
    func CartUpdateAPI(productQty : String, userid : String, productId : String , productPrice : String)  -> Void {
        
            var params:Dictionary<String, Any> = [:]
            let data = UserDefaults.standard.object(forKey: "logindict") as! NSData
            let object = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! [String: String]
            print(object)
            let userID = object["user_id"]
            params[key_fld_user_id] = userid
            params[key_fld_product_id] = productId
            params[key_fld_product_qty] = productQty
            params[key_fld_product_price] = productPrice
            params[key_fld_group_id] = "1"
        
            
            APICallExecutor.postRequestForURLString(true, addToCart, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
                
                if done {
                    
                    if result  != nil {
                       if let dict = result as? [String:Any]{
                        let msg =  dict["message"] as? String
                        let statuscode =  dict["statusCode"] as? Int
                        if statuscode  == 201 {
                            
                            DispatchQueue.main.async  {
                               // TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                        else {
                             let error = (result as? [String:Any])!
                           
                            print(error)
                            let msg = error["message"] as? String
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                    }
                }
            }
        }
        
    }
 }
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
 
}
extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}


