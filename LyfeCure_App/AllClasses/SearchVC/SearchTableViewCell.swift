//
//  SearchTableViewCell.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 09/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var lblTitle : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
