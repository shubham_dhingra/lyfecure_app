//
//  SearchViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var searchTableView : UITableView!
    @IBOutlet weak var txtfield : UITextField!
    @IBOutlet weak var backBtn : UIButton!
    @IBOutlet weak var closeBtn : UIButton!
    
    var searchListArr  = [SearchM]()
    var imagePath = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /* TABLEVIEW DELEGATE */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath as IndexPath) as! SearchTableViewCell
        let item = self.searchListArr[indexPath.row]
        
        cell.lblTitle?.text = item.product_name
        let imgstr = item.product_image
        let str = self.imagePath + imgstr!
                   if str != ""{
                       
                   
                   let url = URL(string: str)
                   if url != nil{
                       cell.imgView?.kf.setImage(with: url, placeholder: UIImage(named:"flower.jpeg"), options:  [.scaleFactor(UIScreen.main.scale)],
                                                          progressBlock: { receivedSize, totalSize in
                                                           
                       },completionHandler: { image, error, cacheType, imageURL in
                           
                       })
                    }
            }
        return cell
    }
    
    
    @IBAction func backBtnAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func closeBtnAction(_ sender : UIButton){
        
    }
    
}

extension SearchViewController {
     
    
    
    func searchAPI(searchText : String?) {
            var params:Dictionary<String, Any> = [:]
            
            params[key_fld_group_id] = "1"
            params[key_fld_search_text] = searchText
    //        params[key_fld_group_id] = "1"
            
            APICallExecutor.postRequestForURLString(true, searchUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
                
    //                        DispatchQueue.main.async {
    //
    //                            if self.navigationController != nil {
    //                                MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
    //                                self.view.isUserInteractionEnabled = true
    //                            }
    //                        }
                
                if done {
                    
                    if result  != nil {
                        let dict = result as? [String:Any]
                        // if let dataDict =  dict?["data"] as? Dictionary<String, Any> {
                        if let tempArr = dict?["data"] as? Array<AnyObject>
                        {
                             self.imagePath =  dict?["img_path"] as? String ?? ""
                            if tempArr.count > 0 {
                                let list : SearchM = SearchM()
                                 self.searchListArr  = list.makeList((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                                
                                
                                DispatchQueue.main.async  {
                                    self.searchTableView.reloadData()
                                }
                                //     }
                            }
                        }
                            
                        else {
                            let errors = ((result as! Dictionary<String, Any>)["errors"] as! Array<Any>)
                            print(errors)
                            
                            let msg: String = (errors[0] as! Dictionary<String, Any>)["message"] as! String
                            
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg, onViewController: self)
                            }
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                    }
                }
            }
        }
}


extension SearchViewController
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchStr  = ((/txtfield.text) as NSString).replacingCharacters(in: range, with: string.trimmingCharacters(in: .whitespaces))
         self.searchAPI(searchText: searchStr.isEmpty ? "\n" : searchStr)
       
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtfield {
            view.endEditing(true)
            return true
        }
        return false
    }
}
