//
//  ListingTableViewCell.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 02/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ListingTableViewCell: UITableViewCell {
    @IBOutlet weak var imagebackgroundView : UIView?
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblSubtitle : UILabel?
    @IBOutlet weak var lblNewPrice : UILabel?
    @IBOutlet weak var lblOldPrice : UILabel?
    @IBOutlet weak var lblOff : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
