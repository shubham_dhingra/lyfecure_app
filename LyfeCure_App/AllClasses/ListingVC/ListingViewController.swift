//
//  ListingViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 02/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var listingTableView : UITableView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var searchBtn : UIButton!
    @IBOutlet weak var cartBtn : UIButton!
    @IBOutlet weak var sideMenuBtn : UIButton!
    var ListingArr  = [ListingM]()
    var ListDict = DashboardM()
    var productArr = [DashboardM]()
    override func viewDidLoad() {
        super.viewDidLoad()
        ListingAPI()
        
    }
    
    
    /* MARK : TableView Delegate */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListingArr.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListingTableViewCell", for: indexPath as IndexPath) as! ListingTableViewCell
        cell.mainView!.layer.borderColor = UIColor.clear.cgColor
        cell.mainView!.layer.cornerRadius = 8
        cell.mainView!.layer.shadowColor = UIColor.lightGray.cgColor
        cell.mainView!.layer.shadowOpacity = 0.3
        cell.mainView!.layer.shadowOffset = CGSize(width: 2.0, height: 10)
        cell.mainView!.layer.shadowRadius = 10
        cell.imagebackgroundView!.layer.borderColor = UIColor.lightGray.cgColor
        cell.imagebackgroundView!.layer.borderWidth = 1
        var list : ListingM = ListingM()
        list = ListingArr[indexPath.row]
        cell.lblTitle?.text = list.product_name
        cell.lblSubtitle?.text = list.product_type
        cell.lblNewPrice?.text = String("M.R.P ₹ ") + list.product_mrpprice!
        cell.lblOldPrice?.text = String("M.R.P ₹ ") + list.product_price!
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < productArr.count {
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailTableViewController") as? DetailTableViewController
            var deal : DashboardM = DashboardM()
            deal = productArr[indexPath.row]
            detailVC?.detailsDict = deal
            self.navigationController?.pushViewController(detailVC!, animated: true)
        }
    }
    
    @IBAction func cartAction(_ sender : UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    @IBAction func searchAction(_ sender : UIButton){
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func ListingAPI() {
        //            DispatchQueue.main.async {
        //                MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
        //                self.view.isUserInteractionEnabled = false
        //            }
        var params:Dictionary<String, Any> = [:]
        
        params[key_fld_cat_id] = "1"
        params[key_fld_group_id] = "1"
        
        APICallExecutor.postRequestForURLString(true, categoryProductListUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            //            DispatchQueue.main.async {
            //
            //                if self.navigationController != nil {
            //                    MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
            //                    self.view.isUserInteractionEnabled = true
            //                }
            //            }
            
            if done {
                
                if result  != nil {
                    let dict = result as? [String:Any]
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            let list : ListingM = ListingM()
                            self.ListingArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                            
                            let dashDict : DashboardM = DashboardM()
                            self.productArr = dashDict.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                            DispatchQueue.main.async  {
                                if self.ListingArr.count > 0{
                                    self.listingTableView .reloadData()
                                    
                                    
                                }
                                else{
                                    
                                    
                                }
                            }
                        }
                        
                    }
                        
                    else {
                        let error = (result as? [String:Any])!
                        
                        print(error)
                        let msg = error["message"] as? String
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
}
