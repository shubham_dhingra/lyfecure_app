//
//  ShippingTableViewCell.swift
//  LyfeCure_App
//
//  Created by EL Group on 17/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ShippingTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLbl : UILabel!
     @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var firstlineAddLbl : UILabel!
    @IBOutlet weak var seclineAddLbl : UILabel!
    @IBOutlet weak var stateLbl : UILabel!
    @IBOutlet weak var pincodeLbl : UILabel!
    @IBOutlet weak var radioBtn : UIButton!
    @IBOutlet weak var editBtn : UIButton!
    @IBOutlet weak var deleteBtn : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
