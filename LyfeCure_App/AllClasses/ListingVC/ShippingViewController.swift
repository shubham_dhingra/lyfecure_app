//
//  ShippingViewController.swift
//  LyfeCure_App
//
//  Created by EL Group on 17/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
import EZSwiftExtensions


protocol ShippingDelegate {
    func passAddress (dict : ShippingM)
}
class ShippingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var shippingTableView : UITableView!
    @IBOutlet weak var headerView : UIView!
    @IBOutlet weak var searchBtn : UIButton!
    @IBOutlet weak var deliveryBtn : UIButton!
    @IBOutlet weak var sideMenuBtn : UIButton!
    
    let appdelegate :AppDelegate! = UIApplication.shared.delegate as? AppDelegate
    var delegate : ShippingDelegate?
    var ShippingArr  = [ShippingM]()
    var shippingDictionary = ShippingM()
    var isSelected : Bool = false
    var SelectedIdx :Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.deliveryBtn.isHidden = true
        deliveryBtn.layer.cornerRadius = 25
        ShippingListAPI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ShippingListAPI()
    }
    
    /* MARK : TableView Delegate */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ShippingArr.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShippingTableViewCell", for: indexPath as IndexPath) as! ShippingTableViewCell
        cell.mainView!.layer.borderColor = UIColor.clear.cgColor
        cell.mainView!.layer.cornerRadius = 8
        cell.mainView!.layer.shadowColor = UIColor.lightGray.cgColor
        cell.mainView!.layer.shadowOpacity = 0.3
        cell.mainView!.layer.shadowOffset = CGSize(width: 2.0, height: 10)
        cell.mainView!.layer.shadowRadius = 10
        SelectedIdx = indexPath.row
        cell.radioBtn.tag = indexPath.row
        cell.editBtn.tag = indexPath.row
        var list : ShippingM = ShippingM()
        list = ShippingArr[indexPath.row]
        cell.nameLbl?.text = list.shipping_name
        cell.firstlineAddLbl?.text = list.shipping_address
        cell.seclineAddLbl?.text = list.shipping_landmark
        cell.stateLbl?.text = list.shipping_state
        cell.pincodeLbl?.text = list.shipping_pincode
        cell.deleteBtn.tag = indexPath.row
        cell.editBtn.isHidden = true
        cell.editBtn.layer.cornerRadius = 17
        cell.radioBtn.addTarget(self, action: #selector(radioAction(_:)), for: .touchUpInside)
         cell.editBtn.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: ShippingTableViewCell = self.shippingTableView.cellForRow(at: indexPath) as! ShippingTableViewCell
        
       }

     @objc func editAction(_ sender: UIButton){
        appdelegate.fromvc = ""
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: ShippingTableViewCell = self.shippingTableView.cellForRow(at: index) as! ShippingTableViewCell
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "AddShippingAddressTableViewController") as? AddShippingAddressTableViewController
        var list : ShippingM = ShippingM()
        list = ShippingArr[SelectedIdx]
        cartVC!.nameStr = list.shipping_name
        cartVC!.emailStr = list.shipping_email
        cartVC!.mobileStr = list.shipping_mobile
         cartVC!.addressStr = list.shipping_address
        cartVC!.pincodeStr = list.shipping_pincode
        cartVC!.stateStr = list.shipping_state
        cartVC!.countryStr = list.shipping_country
        cartVC!.cityStr = list.shipping_city
        cartVC!.shippingId = list.shipping_id
        
        self.navigationController?.pushViewController(cartVC!, animated: true)
    }
    
    @IBAction func deleteAction(_ sender : UIButton){
           var list : ShippingM = ShippingM()
            list = ShippingArr[sender.tag]
            deleteShippingAddress(shippingId: /list.shipping_id)
    }
    
    @objc func radioAction(_ sender: UIButton){
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: ShippingTableViewCell = self.shippingTableView.cellForRow(at: index) as! ShippingTableViewCell
        SelectedIdx =  sender.tag
        let indexP = NSIndexPath(item: SelectedIdx, section: 0)
        print("indexP",indexP as Any)
        SelectedIdx = sender.tag
        shippingDictionary = ShippingArr[SelectedIdx]
        
        if SelectedIdx == sender.tag{
            cell.radioBtn.setImage(UIImage(named: "radio_check"), for: UIControl.State.normal)
            cell.editBtn.isHidden = false
            deliveryBtn.isHidden = false
            isSelected = false
        }
        else{
            cell.radioBtn.setImage(UIImage(named: "radio_uncheck"), for: UIControl.State.normal)
            cell.editBtn.isHidden = true
            deliveryBtn.isHidden = true
            isSelected = true
        }
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func deliveryBtnAction(_ sender : UIButton){
        
        self.delegate?.passAddress(dict: shippingDictionary )
        self.navigationController?.popViewController(animated: true)
    }
    
    
func ShippingListAPI() {

            var params:Dictionary<String, Any> = [:]
    
            params[key_fld_user_id] = "11"
            
            APICallExecutor.postRequestForURLString(true, shippinglistingrequestUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
                
                if done {
                    
                    if result  != nil {
                    let dict = result as? [String:Any]
                    if let tempArr = dict?["data"] as? Array<AnyObject>
                    {
                        if tempArr.count > 0 {
                            let list : ShippingM = ShippingM()
                            self.ShippingArr  = list.makelist((tempArr as! Array<Dictionary<String, Any>>), imgpath: "")
                            
                            DispatchQueue.main.async  {
                                if self.ShippingArr.count > 0{
                                    self.shippingTableView .reloadData()
                                }
                                else{
                                    
                                    
                                }
                            }
                        }
                        
                    }
                            
                        else {
                             let error = (result as? [String:Any])!
                           
                            print(error)
                            let msg = error["message"] as? String
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                    }
                }
            }
        }
}

extension ShippingViewController {
    
    
    func deleteShippingAddress(shippingId : String?) {

            
            var params:Dictionary<String, Any> = [:]
    
            params[key_fld_user_id] = "11"
            params[key_shipping_id] = /shippingId
            
            APICallExecutor.postRequestForURLString(true, removeShippingAddress, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
                
                if done {
                    
                    if result  != nil {
                        let Dictionary = (result as! [String:Any])
                        let statuscode = Dictionary["statusCode"] as? Int
                        
                        if statuscode == 201 {
                            let dict = result as? [String:Any]
                            let message = dict!["message"] as? String
                         ez.runThisInMainThread {
                             TaskExecutor.showTost(withMessage: /message, onViewController: self)
                             self.ShippingListAPI()
                        }
                        }
                        else {
                            let error = (result as? [String:Any])!
                            
                            print(error)
                            let msg = error["message"] as? String
                            DispatchQueue.main.async {
                                TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                            }
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                    }
                }
        }
    }
}
