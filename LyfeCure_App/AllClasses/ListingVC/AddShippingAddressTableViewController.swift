//
//  AddShippingAddressTableViewController.swift
//  LyfeCure_App
//
//  Created by EL Group on 17/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class AddShippingAddressTableViewController: UITableViewController {
    
    @IBOutlet var headerView : UIView!
    @IBOutlet var footerView : UIView!
    @IBOutlet var backBtn : UIButton!
    @IBOutlet var nameTxtField : UITextField!
    @IBOutlet var emailTxtField : UITextField!
    @IBOutlet var mobilenoTxtField : UITextField!
    @IBOutlet var pincodeTxtField : UITextField!
    @IBOutlet var stateTxtField : UITextField!
    @IBOutlet var addressTxtField : UITextField!
    @IBOutlet var landmarkTxtField : UITextField!
    @IBOutlet var townTxtField : UITextField!
    @IBOutlet var countryTxtField : UITextField!
    @IBOutlet var cancelBtn : UIButton!
    @IBOutlet var updateBtn : UIButton!
    
    var nameStr : String?
    var emailStr : String?
    var mobileStr : String?
    var pincodeStr : String?
    var addressStr : String?
    var stateStr : String?
    var countryStr : String?
    var landmarkStr : String?
    var  cityStr : String?
    var shippingId : String?
    
    let appdelegate :AppDelegate! = UIApplication.shared.delegate as? AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.nameTxtField.text = nameStr
        self.emailTxtField.text = emailStr
        self.mobilenoTxtField.text = mobileStr
        self.pincodeTxtField.text = pincodeStr
        self.addressTxtField.text = addressStr
        self.landmarkTxtField.text = landmarkStr
        self.stateTxtField.text = stateStr
        self.countryTxtField.text = countryStr
        self.townTxtField.text = cityStr
        tableView.tableFooterView = footerView
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        if appdelegate.fromvc == "addaddress" {
            self.updateBtn.setTitle("SAVE", for: .normal)
            //        self.updateBtn.titleLabel?.text = "SAVE"
        }
        else{
            self.updateBtn.setTitle("UPDATE", for: .normal)
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return self.headerView.bounds.size.height
        
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView.frame  = CGRect(x: 0, y: -30, width: self.tableView.frame.size.width, height: 70)
        backBtn.addTarget(self, action: #selector(AddShippingAddressTableViewController.backBtnAction(_:)), for: .touchUpInside)
        
        return headerView
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 55))
        cancelBtn.addTarget(self, action: #selector(AddShippingAddressTableViewController.cancelBtnAction(_:)), for: .touchUpInside)
        updateBtn.addTarget(self, action: #selector(AddShippingAddressTableViewController.updateBtnAction(_:)), for: .touchUpInside)
        return footerView
        
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    @objc func cancelBtnAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func updateBtnAction(_ sender : UIButton){
        self.view.endEditing(true)
        if appdelegate.fromvc == "addaddress"{
            addShippingAddressAPI()
            
        }
        else{
            updateShippingAddressAPI()
//            addShippingAddressAPI()
        }
        
    }
    @objc func backBtnAction(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func updateShippingAddressAPI() {
        
        var params:Dictionary<String, Any> = [:]
        params[key_fld_user_id] = "11"
        params[key_fld_group_id] = "1"
        params[key_shipping_name] = nameTxtField?.text
        params[key_shipping_mobile] = mobilenoTxtField?.text
        params[key_shipping_email] = emailTxtField?.text
        params[key_shipping_pincode] = pincodeTxtField?.text
        params[key_shipping_state] = stateTxtField?.text
        params[key_shipping_landmark] = landmarkTxtField?.text
        params[key_shipping_address] = addressTxtField?.text
        params[key_shipping_city] = townTxtField?.text
        params[key_shipping_id] = shippingId
        
        APICallExecutor.postRequestForURLString(true, shippingupdaterequestUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            
            if done {
                
                if result  != nil {
                    let Dictionary = (result as! [String:Any])
                    let statuscode = Dictionary["statusCode"] as? Int
                    
                    if statuscode == 201 {
                        let dict = result as? [String:Any]
                        let message = dict!["message"] as? String
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: message!, onViewController: self)
                            Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                                self.popVC()
                                //let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "ProfileTableViewController") as? ProfileTableViewController
                                // self.navigationController?.pushViewController(dashboardVC!, animated: true)
                                
                            }
                        }
                    }
                    else {
                        let error = (result as? [String:Any])!
                        
                        print(error)
                        let msg = error["message"] as? String
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
    func addShippingAddressAPI() {
        
        var params:Dictionary<String, Any> = [:]
        params[key_fld_user_id] = "11"
        params[key_fld_group_id] = "1"
        params[key_shipping_name] = nameTxtField?.text
        params[key_shipping_mobile] = mobilenoTxtField?.text
        params[key_shipping_email] = emailTxtField?.text
        params[key_shipping_pincode] = pincodeTxtField?.text
        params[key_shipping_state] = stateTxtField?.text
        params[key_shipping_landmark] = landmarkTxtField?.text
        params[key_shipping_address] = addressTxtField?.text
        params[key_shipping_city] = townTxtField?.text
        
        APICallExecutor.postRequestForURLString(true, shippingaddrequestUrl, [:], params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            
            if done {
                
                if result  != nil {
                    let Dictionary = (result as! [String:Any])
                    let statuscode = Dictionary["statusCode"] as? Int
                    
                    if statuscode == 201 {
                        let dict = result as? [String:Any]
                        let message = dict!["message"] as? String
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: message!, onViewController: self)
                            self.popVC()
                            Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                                //let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "ProfileTableViewController") as? ProfileTableViewController
                                // self.navigationController?.pushViewController(dashboardVC!, animated: true)
                                
                            }
                        }
                    }
                    else {
                        let errors = ((result as! Dictionary<String, Any>)["errors"] as! Array<Any>)
                        print(errors)
                        
                        let msg: String = (errors[0] as! Dictionary<String, Any>)["message"] as! String
                        
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg, onViewController: self)
                        }
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    TaskExecutor.showTost(withMessage: "Connection Error", onViewController: self)
                }
            }
        }
    }
}
