//
//  LoginViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 02/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet var NameTxtField : UITextField!
    @IBOutlet var PwdTxtField : UITextField!
    @IBOutlet var WholesaleBtn : UIButton!
    @IBOutlet var RetailerBtn : UIButton!
    @IBOutlet var AgentBtn : UIButton!
    @IBOutlet var HideBtn : UIButton!
    @IBOutlet var LoginBtn : UIButton!
    @IBOutlet var view1 : UIView!
    @IBOutlet var view2 : UIView!
    @IBOutlet var view3 : UIView!
    var locallanguage = LocalLanguage()
    var isSelected : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        WholesaleBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
       //  WholesaleBtn.titleLabel?.font =  AppFont.OpenSansBold(withSize: 17)
            view1.backgroundColor = AppColors.appGradientGreen
            AgentBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
       //  AgentBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
            view2.backgroundColor = UIColor.clear
            RetailerBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        // RetailerBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
           view3.backgroundColor = UIColor.clear
        // WholesaleBtn.isSelected = true
        LoginBtn.layer.cornerRadius = 25
        
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_ :)))
        tap.delegate = self
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
    }
    @objc func handleTap (_ sender:UITapGestureRecognizer){
             NameTxtField.resignFirstResponder()
             PwdTxtField.resignFirstResponder()
    }

    @IBAction func signUpAction(_ sender: Any) {
        
    }
    @IBAction func loginAction(_ sender: Any) {
//        let nametext:String = (NameTxtField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
//
//                   if nametext.count == 0 {
//
//                      TaskExecutor.showTost(withMessage: self.locallanguage.strMsgNameEmpty, onViewController: self, forTime: tost_time)
//                   }
//
//                  if PwdTxtField?.text == "" {
//                    TaskExecutor.showTost(withMessage: self.locallanguage.strMsgPasswordEmpty, onViewController: self, forTime: tost_time)
//                     }
        requestForLogin()
//   let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
//    self.navigationController?.pushViewController(dashboardVC!, animated: true)
        
    }
    @IBAction func forgotPwdAction(_ sender: Any) {
        let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
        self.navigationController?.pushViewController( listingVC!, animated: true)
    }
    @IBAction func hideEyeAction(_ sender: Any) {
        if isSelected{
            PwdTxtField.isSecureTextEntry = true
            HideBtn.setImage(UIImage(named: "hide"), for: UIControl.State.normal)
            isSelected = false
        }
        else{
            PwdTxtField.isSecureTextEntry = false
            HideBtn.setImage(UIImage(named: "show"), for: UIControl.State.normal)
             isSelected = true
        }
    }
    @IBAction func retailerAction(_ sender: Any) {
          RetailerBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
       RetailerBtn.titleLabel?.font =  UIFont(name: "OpenSans-Bold", size: 17)
            view3.backgroundColor = AppColors.appGradientGreen
       AgentBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
         AgentBtn.titleLabel?.font =  UIFont(name: "OpenSans-Regular", size: 17)
            view2.backgroundColor = UIColor.clear
            WholesaleBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
         WholesaleBtn.titleLabel?.font =  UIFont(name: "OpenSans-Regular", size: 17)
            view1.backgroundColor = UIColor.clear
//        WholesaleBtn.isSelected = false
//         RetailerBtn.isSelected = true
//         AgentBtn.isSelected = false
     
    }
    @IBAction func agentAction(_ sender: Any) {
       
            AgentBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
            AgentBtn.titleLabel?.font =  UIFont(name: "OpenSans-Bold", size: 17)
            view2.backgroundColor = AppColors.appGradientGreen
        
            RetailerBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
            RetailerBtn.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 17)
            view3.backgroundColor = UIColor.clear
        
             WholesaleBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
            WholesaleBtn.titleLabel?.font =  UIFont(name: "OpenSans-Regular", size: 17)
          view1.backgroundColor = UIColor.clear
//            WholesaleBtn.isSelected = false
//            RetailerBtn.isSelected = false
//            AgentBtn.isSelected = true

    }
    @IBAction func wholesaleAction(_ sender: Any) {
    
            WholesaleBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
           WholesaleBtn.titleLabel?.font =  UIFont(name: "OpenSans-Bold", size: 17)
            view1.backgroundColor = AppColors.appGradientGreen
        
            AgentBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
           AgentBtn.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 17)
        
            view2.backgroundColor = UIColor.clear
            RetailerBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
           RetailerBtn.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 17)
           view3.backgroundColor = UIColor.clear
//        WholesaleBtn.isSelected = true
//        RetailerBtn.isSelected = false
//        AgentBtn.isSelected = false
        
    }
    func requestForLogin()
      {
                
//        let nametext:String = (NameTxtField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
//
//            if nametext.count == 0 {
//
//        TaskExecutor.showTost(withMessage: self.locallanguage.strMsgNameEmpty, onViewController: self, forTime: tost_time)
//                                  }
//
//     if PwdTxtField?.text == "" {
//    TaskExecutor.showTost(withMessage: self.locallanguage.strMsgPasswordEmpty, onViewController: self, forTime: tost_time)
//                                    }
                       

                 
//          DispatchQueue.main.async {
//              MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
//          }
                  
                  var signInInfo:Dictionary<String, Any> = [:]
                  signInInfo[key_user_id]    = "LYF6811"// (nametext.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
                  signInInfo[key_user_password] = "123456" //PwdTxtField.text
                 signInInfo[key_user_type] = "1"
        /// Save
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: signInInfo), forKey: "logindict")

        /// Read
        let data = UserDefaults.standard.object(forKey: "logindict") as! NSData
        let object = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! [String: String]
        print(object)
          //      if WholesaleBtn.isSelected{
         //         signInInfo[key_user_type] = "1"
         //       }
        //         else if AgentBtn.isSelected{
        //          signInInfo[key_user_type] = "2"
        //        }
        //       else if RetailerBtn.isSelected{
        //           signInInfo[key_user_type] = "3"
        //        }
       // else{
       //             TaskExecutor.showTost(withMessage: self.locallanguage.strMsgSelectGender, onViewController: self, forTime: tost_time)
      //  }
        
                let userOperator: UserOperation = UserOperation()
                  userOperator.signIn(withSignInInfo: signInInfo) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
                  
//                          DispatchQueue.main.async {
//                              if self.navigationController != nil {
//                              MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
//                          }
  //                    }
                      
                      if done {
                          
                          if result  != nil  {
                            let Dictionary = (result as! [String:Any])
                            let statuscode = Dictionary["statusCode"] as? Int
                           // let status = Dictionary["status"] as? Int
                           
                            if statuscode == 201 {
                                 let dict = result as? [String:Any]
                                if let user_id = dict?["user_id"] as? String {
                                    UDKeys.UserID.save(user_id)
                                }
                                DispatchQueue.main.async {
                                  //  TaskExecutor.showTost(withMessage: message!, onViewController: self)
                                //  Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (timer) in
                                   // AppDelegate.getAppDelegate().SuccessfulLogin()
                                    let dashboardVC =  self.storyboard?.instantiateViewController(withIdentifier: "DashboardTableViewController") as? DashboardTableViewController
                                self.navigationController?.pushViewController(dashboardVC!, animated: true)
                              
                                 //   }
                                    
                                }
                               
                           }
                              else {
                                   let error = (result as? [String:Any])!
                                  print(error)
                                  let msg = error["message"] as? String
                                  DispatchQueue.main.async {
                                      TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                                  }
                              }
                          }
                        }
                     
                      else {
                          
                          DispatchQueue.main.async {
                              
                              TaskExecutor.showTost(withMessage: self.locallanguage.strMsgServerRequestError, onViewController: self)
                          }
                      }
                  }
                  
                 
              
          }
}
