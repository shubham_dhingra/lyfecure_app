//
//  LoginTableViewController.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 06/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class LoginTableViewController: UITableViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet var NameTxtField : UITextField!
    @IBOutlet var loginTableView : UITableView!
    @IBOutlet var PwdTxtField : UITextField!
    @IBOutlet var WholesaleBtn : UIButton!
    @IBOutlet var RetailerBtn : UIButton!
    @IBOutlet var AgentBtn : UIButton!
    @IBOutlet var HideBtn : UIButton!
    @IBOutlet var view1 : UIView!
    @IBOutlet var view2 : UIView!
    @IBOutlet var view3 : UIView!
    var locallanguage = LocalLanguage()
    var isSelected : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        WholesaleBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        WholesaleBtn.titleLabel?.font =  AppFont.OpenSansBold(withSize: 17)
        view1.backgroundColor = AppColors.appGradientGreen
        AgentBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        AgentBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view2.backgroundColor = UIColor.clear
        RetailerBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        RetailerBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view3.backgroundColor = UIColor.clear
        
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_ :)))
        tap.delegate = self
        loginTableView.addGestureRecognizer(tap)
        loginTableView.isUserInteractionEnabled = true
        
    }
    @objc func handleTap (_ sender:UITapGestureRecognizer){
        NameTxtField.resignFirstResponder()
        PwdTxtField.resignFirstResponder()
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        
    }
    @IBAction func loginAction(_ sender: Any) {
        let nametext:String = (NameTxtField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        if nametext.count == 0 {
            TaskExecutor.showTost(withMessage: self.locallanguage.strMsgNameEmpty, onViewController: self, forTime: tost_time)
        }
        
        if PwdTxtField?.text == "" {
            TaskExecutor.showTost(withMessage: self.locallanguage.strMsgPasswordEmpty, onViewController: self, forTime: tost_time)
        }
        
        let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.navigationController?.pushViewController(cartVC!, animated: true)
        
    }
    @IBAction func forgotPwdAction(_ sender: Any) {
        let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "ListingViewController") as? ListingViewController
        self.navigationController?.pushViewController( listingVC!, animated: true)
    }
    @IBAction func hideEyeAction(_ sender: Any) {
        if isSelected{
            PwdTxtField.isSecureTextEntry = true
            HideBtn.setImage(UIImage(named: "hide"), for: UIControl.State.normal)
            isSelected = false
        }
        else{
            PwdTxtField.isSecureTextEntry = false
            HideBtn.setImage(UIImage(named: "show"), for: UIControl.State.normal)
            isSelected = true
        }
    }
    @IBAction func retailerAction(_ sender: Any) {
        RetailerBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        RetailerBtn.titleLabel?.font =  AppFont.OpenSansBold(withSize: 17)
        view3.backgroundColor = AppColors.appGradientGreen
        isSelected = false
        AgentBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        AgentBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view2.backgroundColor = UIColor.clear
        WholesaleBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        WholesaleBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view1.backgroundColor = UIColor.clear
        
        
    }
    @IBAction func agentAction(_ sender: Any) {
        
        AgentBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        AgentBtn.titleLabel?.font =  AppFont.OpenSansBold(withSize: 17)
        view2.backgroundColor = AppColors.appGradientGreen
        
        RetailerBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        RetailerBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view3.backgroundColor = UIColor.clear
        
        WholesaleBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        WholesaleBtn.titleLabel?.font =  AppFont.OpenSansRegular(withSize: 17)
        view1.backgroundColor = UIColor.clear
        
        
    }
    @IBAction func wholesaleAction(_ sender: Any) {
        
        WholesaleBtn.setTitleColor(AppColors.appGradientGreen, for: UIControl.State.normal)
        WholesaleBtn.titleLabel?.font =  AppFont.OpenSansBold(withSize: 17)
        view1.backgroundColor = AppColors.appGradientGreen
        
        AgentBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        AgentBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view2.backgroundColor = UIColor.clear
        RetailerBtn.setTitleColor(AppColors.appGradientDarkColor, for: UIControl.State.normal)
        RetailerBtn.titleLabel?.font = AppFont.OpenSansRegular(withSize: 17)
        view3.backgroundColor = UIColor.clear
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 145
        }
        else  if indexPath.row == 1 {
                 return 145
               }
        else  if indexPath.row == 2 {
                   return 100
               }
        else  if indexPath.row == 3 {
            return 100
        }
        else  if indexPath.row == 4 {
            return 165
        }
        else  if indexPath.row == 5 {
            return 165
        }
        return 165
    }
    func requestForLogin()
    {
        
        let nametext:String = (NameTxtField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        
        if nametext.count == 0 {
            
            TaskExecutor.showTost(withMessage: self.locallanguage.strMsgNameEmpty, onViewController: self, forTime: tost_time)
        }
        
        if PwdTxtField?.text == "" {
            TaskExecutor.showTost(withMessage: self.locallanguage.strMsgPasswordEmpty, onViewController: self, forTime: tost_time)
        }
        
        //  return
        
        //          DispatchQueue.main.async {
        //              MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: false)
        //          }
        
        var signInInfo:Dictionary<String, Any> = [:]
        signInInfo[key_user_id]    = (nametext.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        signInInfo[key_user_password] = PwdTxtField.text
        if WholesaleBtn.isSelected{
            signInInfo[key_user_type] = "1"
        }
        else if AgentBtn.isSelected{
            signInInfo[key_user_type] = "2"
        }
        else if RetailerBtn.isSelected{
            signInInfo[key_user_type] = "3"
        }
        let userOperator: UserOperation = UserOperation()
        userOperator.signIn(withSignInInfo: signInInfo) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            //                          DispatchQueue.main.async {
            //                              if self.navigationController != nil {
            //                              MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
            //                          }
            //                    }
            
            if done {
                
                if result  != nil {
                    
                    let dict = result as? [String:Any]
                    if let dataDict =  dict?["data"] as? Dictionary<String, Any> {
                        
                        //                          let token = dataDict[key_jwt_token] as? String
                        //
                        //                          let defaults = UserDefaults.standard
                        //                          defaults.set(token, forKey: key_jwt_token)
                        //
                        //                          let msg = dataDict[key_response_msg] as? String
                        //                                  defaults.set(self.mobileNoTexfield.text, forKey: key_user_mobileno )
                        //
                        //                              DispatchQueue.main.async {
                        //                              TaskExecutor.showTost(withMessage: msg!, onViewController: self)
                        //                              Timer.scheduledTimer(withTimeInterval: 2.1, repeats: false, block: { (timer) in
                        //                                    let VerifyVC = self.storyboard!.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
                        //                                        //VerifyVC.mobileStr = self.mobileNoTexfield.text! as NSString
                        //                                        self.navigationController!.pushViewController(VerifyVC, animated: true)
                        //
                        //                                   })
                        //
                        //                                  }
                    }
                    else {
                        let errors = ((result as! Dictionary<String, Any>)["errors"] as! Array<Any>)
                        print(errors)
                        
                        let msg: String = (errors[0] as! Dictionary<String, Any>)["message"] as! String
                        
                        DispatchQueue.main.async {
                            TaskExecutor.showTost(withMessage: msg, onViewController: self)
                        }
                    }
                }
                
            }
            else {
                
                DispatchQueue.main.async {
                    
                    TaskExecutor.showTost(withMessage: self.locallanguage.strMsgServerRequestError, onViewController: self)
                }
            }
        }
        
        
        
    }
}
