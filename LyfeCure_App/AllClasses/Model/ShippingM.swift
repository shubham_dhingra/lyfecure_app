//
//  ShippingM.swift
//  LyfeCure_App
//
//  Created by EL Group on 17/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
let key_shipping_country  = "fld_shipping_country"
let key_shipping_name = "fld_shipping_name"
let key_shipping_mobile  = "fld_shipping_mobile"
let key_shipping_email = "fld_shipping_email"
let key_shipping_pincode = "fld_shipping_pincode"
let key_shipping_state = "fld_shipping_state"
let key_shipping_landmark = "fld_shipping_landmark"
let key_shipping_address = "fld_shipping_address"
let key_shipping_city = "fld_shipping_city"
let key_shipping_id = "fld_shipping_id"

class ShippingM: NSObject {
    
    var user_id : String?
    var group_id : String?
    var shipping_country : String?
    var shipping_name = ""
    var shipping_mobile : String? = ""
    var shipping_email : String? = ""
    var shipping_pincode = ""
    var shipping_state  = ""
    var shipping_landmark : String? = ""
    var shipping_address  = ""
    var shipping_city = ""
    var shipping_id : String?
    
    
    func setValue(fromShippingInfo shippingInfo: Dictionary<String,Any>) -> Void {
        if shippingInfo["fld_shipping_country"] != nil{
            self.shipping_country = shippingInfo["fld_shipping_country"] as? String
        }
        if shippingInfo["fld_user_id"] != nil{
            self.user_id = shippingInfo["fld_user_id"] as? String
        }
        if shippingInfo["fld_group_id"] != nil{
            self.group_id = shippingInfo["fld_group_id"] as? String
        }
        
        if shippingInfo["fld_shipping_id"] != nil{
            self.shipping_id = shippingInfo["fld_shipping_id"] as? String
        }
        
        if shippingInfo["fld_shipping_country"] != nil{
            self.shipping_country = shippingInfo["fld_shipping_country"] as? String
        }
        
        if let shipping_name = shippingInfo["fld_shipping_name"] as? String {
            self.shipping_name = shipping_name
        }
    
        if let shipping_pincode = shippingInfo["fld_shipping_pincode"] as? String{
            self.shipping_pincode = shipping_pincode
        }
        if let shipping_address = shippingInfo["fld_shipping_address"] as? String {
                   self.shipping_address = shipping_address
               }
        if let shipping_state = shippingInfo["fld_shipping_state"] as? String {
            self.shipping_state = shipping_state
        }
        if let shipping_city = shippingInfo["fld_shipping_city"] as? String {
            self.shipping_city = shipping_city
        }
        if shippingInfo["fld_shipping_mobile"] != nil{
            self.shipping_mobile = shippingInfo["fld_shipping_mobile"] as? String
        }
        if shippingInfo["fld_shipping_email"] != nil{
            self.shipping_email = shippingInfo["fld_shipping_email"] as? String
        }
//        if shippingInfo["fld_shipping_pincode"] != nil{
//            self.shipping_pincode = shippingInfo["fld_shipping_pincode"] as? String
//        }
//        if shippingInfo["fld_shipping_state"] != nil{
//            self.shipping_state = shippingInfo["fld_shipping_state"] as? String
//        }
        if shippingInfo["fld_shipping_landmark"] != nil{
            self.shipping_landmark = shippingInfo["fld_shipping_landmark"] as? String
        }
//        if shippingInfo["fld_shipping_city"] != nil{
//            self.shipping_city = shippingInfo["fld_shipping_city"] as? String
//        }
//        if shippingInfo["fld_shipping_address"] != nil{
//            self.shipping_address = shippingInfo["fld_shipping_address"] as? String
//        }
    }
    
    func makelist(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<ShippingM> {
        
        var conversations: Array<ShippingM> = Array<ShippingM>()
        if listArray != nil {
            
            for info in listArray! {
                
                let conversation: ShippingM = ShippingM()
                conversation.setValue(fromShippingInfo: info)
                conversations.append(conversation)
            }
        }
        
        return conversations
    }
    
}
/*
 12.shipping_add
 request =
 
 {
 "fld_user_id": "11",
 "fld_group_id": "1",
 "fld_shipping_name": "Randhir",
 "fld_shipping_mobile": "9971576373",
 "fld_shipping_email": "randhir286@gmail.com",
 "fld_shipping_pincode": "110096",
 "fld_shipping_state": "up",
 "fld_shipping_landmark": "sec 16",
 "fld_shipping_address": "sblock b",
 "fld_shipping_city": "noida",
 "fld_shipping_country": "India"
 }
 response =
 {
 "status": true,
 "statusCode": 201,
 "message": "Shipping Address added successfully"
 }
 13.shipping_listing
 request =
 {
 "fld_user_id": "11"
 }
 
 response
 {
 "status": true,
 "statusCode": 201,
 "data": [
 {
 "fld_user_id": "11",
 "fld_shipping_id": "7",
 "fld_shipping_name": "sunny Rocks",
 "fld_shipping_address": "a- 7 ,noid",
 "fld_shipping_landmark": "compulsory",
 "fld_shipping_city": "india",
 "fld_shipping_state": "up",
 "fld_shipping_country": "India",
 "fld_shipping_pincode": "110046",
 "fld_shipping_email": "randhir286@gmail.com",
 "fld_shipping_mobile": "9971576373"
 },
 {
 "fld_user_id": "11",
 "fld_shipping_id": "8",
 "fld_shipping_name": "Randhir Kumar",
 "fld_shipping_address": "a- 7 ,noid",
 "fld_shipping_landmark": "optional",
 "fld_shipping_city": "india",
 "fld_shipping_state": "up",
 "fld_shipping_country": "India",
 "fld_shipping_pincode": "110046",
 "fld_shipping_email": "randhir286@gmail.com",
 "fld_shipping_mobile": "9971576373"
 },
 {
 "fld_user_id": "11",
 "fld_shipping_id": "9",
 "fld_shipping_name": "Randhir Kumar",
 "fld_shipping_address": "A-7 ,noida",
 "fld_shipping_landmark": "Post Office",
 "fld_shipping_city": "noida",
 "fld_shipping_state": "uttar prad",
 "fld_shipping_country": "India",
 "fld_shipping_pincode": "123456",
 "fld_shipping_email": "randhir286@gmail.com",
 "fld_shipping_mobile": "1234567890"
 },
 {
 "fld_user_id": "11",
 "fld_shipping_id": "25",
 "fld_shipping_name": "Randhir",
 "fld_shipping_address": "sblock b",
 "fld_shipping_landmark": "sec 16",
 "fld_shipping_city": "noida",
 "fld_shipping_state": "up",
 "fld_shipping_country": "India",
 "fld_shipping_pincode": "110096",
 "fld_shipping_email": "randhir286@gmail.com",
 "fld_shipping_mobile": "9971576373"
 }
 ],
 "message": "Shipping Address Listing"
 }
 14.shipping_update
 request =
 {
 "fld_user_id": "11",
 "fld_group_id": "1",
 "fld_shipping_name": "Randhir",
 "fld_shipping_mobile": "9971576373",
 "fld_shipping_email": "randhir286@gmail.com",
 "fld_shipping_pincode": "110096",
 "fld_shipping_state": "up",
 "fld_shipping_landmark": "sec 16",
 "fld_shipping_address": "sblock b",
 "fld_shipping_city": "noida",
 "fld_shipping_country": "India",
 "fld_shipping_id": "7"
 
 }
 
 response=
 {
 "status": true,
 "statusCode": 201,
 "message": "Shipping Address updated"
 }
 
 15.http://crmb2c.org/anessb2b/apis/save_order
 
 Request
 
 {
 "fld_customer_id":"12",
 "fld_shipping_id":9,
 "fld_product_id":114,
 "fld_order_quantity":2,
 "fld_order_price":10
 }
 
 Response
 {
 "status": true,
 "statusCode": 201,
 "OrderNo": 1,
 "message": "Order Placed Successfuly"
 }
 */

