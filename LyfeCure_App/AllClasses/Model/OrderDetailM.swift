//
//  OrderDetailM.swift
//  LyfeCure_App
//
//  Created by SHUBHAM DHINGRA on 2/22/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit
          
//"fld_order_id": "40",
//          "fld_order_date": "2020-02-11",
//          "fld_order_total": 866,
//          "fld_order_shipping_total": "30",
//          "fld_shipping_id": "25",
//          "fld_shipping_name": "Randhir",
//          "fld_shipping_address": "sblock b",
//          "fld_shipping_landmark": "sec 16",
//          "fld_shipping_city": "noida",
//          "fld_shipping_state": "up",
//          "fld_shipping_country": "India",
//          "fld_shipping_pincode": "110096",
//          "fld_shipping_email": "randhir286@gmail.com",
//          "fld_shipping_mobile": "9971576373"
class OrderDetailM : NSObject {
    
    var product_id : String? = ""
    var product_name : String? = ""
    var product_image : String? = ""
    var product_type : String? = ""
    var order_quantity : String? = ""
    var order_price : String? = ""
    
    var order_date : String? = ""
    var order_id : String? = ""
    var order_first_date : String? = ""
    var order_second_date : String? = ""
    var order_total : Int?
    var order_shipping_total : String? = ""
    var shipping_id : String? = ""
    var shipping_name : String? = ""
    var shipping_address : String? = ""
    var shipping_landmark : String? = ""
    var shipping_city : String? = ""
    var shipping_state : String? = ""
    var shipping_country : String? = ""
    var shipping_pinCode : String? = ""
    var shipping_email : String? = ""
    var shipping_mobile : String? = ""
    


func setValue(fromListInfo listInfo: Dictionary<String,Any>) -> Void {
   
    if listInfo["fld_product_id"] != nil{
        self.product_id = listInfo["fld_product_id"] as? String
    }
    if listInfo["fld_product_name"] != nil{
        self.product_name = listInfo["fld_product_name"] as? String
    }
    if listInfo["fld_product_image"] != nil{
        self.product_image = listInfo["fld_product_image"] as? String
    }
    if listInfo["fld_product_type"] != nil{
        self.product_type = listInfo["fld_product_type"] as? String
    }
    if listInfo["fld_order_quantity"] != nil{
        self.order_quantity = listInfo["fld_order_quantity"] as? String
    }
    if listInfo["fld_order_price"] != nil{
        self.order_price = listInfo["fld_order_price"] as? String
    }
    
    if listInfo["fld_order_id"] != nil{
        self.order_id = listInfo["fld_order_id"] as? String
    }
    
    if listInfo["fld_order_date"] != nil{
        self.order_date = listInfo["fld_order_date"] as? String
    }
    
    if listInfo["fld_order_total"] != nil{
        self.order_total = listInfo["fld_order_total"] as? Int
    }
    if listInfo["fld_order_shipping_total"] != nil{
        self.order_shipping_total = listInfo["fld_order_shipping_total"] as? String
    }
    
    if listInfo["fld_shipping_id"] != nil{
        self.shipping_id = listInfo["fld_shipping_id"] as? String
    }
    
    if listInfo["fld_shipping_name"] != nil{
        self.shipping_name = listInfo["fld_shipping_name"] as? String
    }

    if listInfo["fld_shipping_address"] != nil{
        self.shipping_address = listInfo["fld_shipping_address"] as? String
    }

    if listInfo["fld_shipping_landmark"] != nil{
        self.shipping_landmark = listInfo["fld_shipping_landmark"] as? String
    }

    if listInfo["fld_shipping_city"] != nil{
        self.shipping_city = listInfo["fld_shipping_city"] as? String
    }

    if listInfo["fld_shipping_state"] != nil{
        self.shipping_state = listInfo["fld_shipping_state"] as? String
    }

    if listInfo["fld_shipping_country"] != nil{
        self.shipping_country = listInfo["fld_shipping_country"] as? String
    }

    if listInfo["fld_shipping_pincode"] != nil{
           self.shipping_pinCode = listInfo["fld_shipping_pincode"] as? String
    }
    
    if listInfo["fld_shipping_email"] != nil{
           self.shipping_email = listInfo["fld_shipping_email"] as? String
    }
    
    if listInfo["fld_shipping_mobile"] != nil{
           self.shipping_mobile = listInfo["fld_shipping_mobile"] as? String
    }
}
    
    func makelist(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<OrderDetailM> {
                 
                 var conversations: Array<OrderDetailM> = Array<OrderDetailM>()
                 if listArray != nil {
                     
                     for info in listArray! {
                         
                      let conversation: OrderDetailM = OrderDetailM()
                       conversation.setValue(fromListInfo: info)
                       conversations.append(conversation)
                     }
                 }
                 return conversations
       }
    
    
}
