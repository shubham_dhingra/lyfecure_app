//
//  UpdateProfoleM.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit


let key_fld_company_name = "fld_company_name"
let key_fld_drug_license = "fld_drug_license"
let key_fld_contact_number = "fld_contact_number"
let key_fld_gst_no = "fld_gst_no"
let key_fld_user_email = "fld_user_email"
let key_fld_user_type = "fld_user_type"
let key_fld_message = "message"

class UpdateProfoleM: NSObject {
          var user_id : Int?
          var company_name : String? = ""
          var drug_license : String? = ""
          var contact_number : String? = ""
          var gst_no : String? = ""
          var user_email : String? = ""
          var user_type : String? = ""
          var message: String? = ""


func setValue(fromProfileInfo profileInfo: Dictionary<String,Any>) -> Void {
    if profileInfo[key_fld_message] != nil{
    self.message = profileInfo[key_fld_message] as? String
 }
    if profileInfo[key_fld_user_id] != nil{
       self.user_id = profileInfo[key_fld_user_id] as? Int
    }
    if profileInfo[key_fld_company_name] != nil{
       self.company_name = profileInfo[key_fld_company_name] as? String
    }
    if profileInfo[key_fld_drug_license] != nil{
       self.drug_license = profileInfo[key_fld_drug_license] as? String
    }
    if profileInfo[key_fld_contact_number] != nil{
       self.contact_number = profileInfo[key_fld_contact_number] as? String
    }
    if profileInfo[key_fld_gst_no] != nil{
       self.gst_no = profileInfo[key_fld_gst_no] as? String
    }
    if profileInfo[key_fld_user_email] != nil{
       self.user_email = profileInfo[key_fld_user_email] as? String
    }
    if profileInfo[key_fld_user_type] != nil{
       self.user_type = profileInfo[key_fld_user_type] as? String
    }
}

func getProfileInfo() -> Dictionary<String, Any> {
      var profileInfo: Dictionary<String, Any> = Dictionary<String, Any>()
   if self.message != nil{
       profileInfo[key_fld_message] = self.message
   }
    if self.user_id != nil{
        profileInfo[key_fld_user_id] = self.user_id
    }
    if self.company_name != nil{
        profileInfo[key_fld_company_name] = self.company_name
    }
    if self.drug_license != nil{
        profileInfo[key_fld_drug_license] = self.drug_license
    }
    if self.contact_number != nil{
        profileInfo[key_fld_contact_number] = self.contact_number
    }
    if self.gst_no != nil{
        profileInfo[key_fld_gst_no] = self.gst_no
    }
    if self.user_email != nil{
        profileInfo[key_fld_user_email] = self.user_email
    }
    if self.user_type != nil{
        profileInfo[key_fld_user_type] = self.user_type
    }
    
return profileInfo
  }
}
/*
======
17.http://crmb2c.org/anessb2b/apis/profile_info

Request

{
    "fld_user_id":"12"
}

Response

{
    "status": true,
    "statusCode": 201,
    "data": [
        {
            "fld_user_id": "12",
            "fld_company_name": null,
            "fld_drug_license": "10345400",
            "fld_contact_number": "9098909890",
            "fld_gst_no": "asdf323423432",
            "fld_user_email": "dest3@gmail.com",
            "fld_user_type": "Distributor"
        }
    ],
    "message": "Profile Detail"
}
*/


