//
//  DashboardM.swift
//  LyfeCure_App
//
//  Created by EL Group on 12/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class DashboardM: NSObject {
    var id : String? = ""
    var name :String? = ""
    var image: String? = ""
    var message: String? = ""
    var img_path : String? = ""
    var content : String? = ""
    var link : String? = ""
    var status : String? = ""
    var created_at : String? = ""
    var product_id : String? = ""
    var type : String? = ""
    var cat_id : Int?
    var scat_id: Int?
    var brand_id: Int?
    var product_url: String? = ""
    var product_sku: String? = ""
    var product_qty: Int?
    var salt: Int?
    var manufacturer: String? = ""
    var product_short_descrip: String? = ""
    var product_descrip: String? = ""
    var product_size: Int?
    var product_date: String? = ""
    var tax: String? = ""
    var formula: String? = ""
    var expiry_date:String? = ""
    var product_type: String? = ""
    var product_name: String? = ""
    var product_image: String? = ""
    var product_mrpprice: String? = ""
    var product_price: String? = ""
    var product_status: String? = ""
    
    
//     required init( id: String, name: String, image: String, img_path: String,message: String) {
//            self.id = id
//            self.name = name
//            self.image = image
//            self.message = message
//            self.img_path = img_path
//        }
    
    func setValue(fromInfo dashboardInfo: Dictionary<String,Any>) -> Void {
           if dashboardInfo["id"] != nil{
               self.id = dashboardInfo["id"] as? String
           }
           if dashboardInfo["name"] != nil{
               self.name = dashboardInfo["name"] as? String
           }
           if dashboardInfo["image"] != nil{
               self.image = dashboardInfo["image"] as? String
           }
          if dashboardInfo["fld_product_image"] != nil{
            self.product_image = dashboardInfo["fld_product_image"] as? String
            }
           if dashboardInfo["img_path"] != nil{
               self.img_path = dashboardInfo["img_path"] as? String
           }
          if dashboardInfo["message"] != nil{
               self.message = dashboardInfo["message"] as? String
          }
          if dashboardInfo["fld_product_name"] != nil{
         self.product_name = dashboardInfo["fld_product_name"] as? String
          }
          if dashboardInfo["fld_product_mrpprice"] != nil{
        self.product_mrpprice = dashboardInfo["fld_product_mrpprice"] as? String
          }
        if dashboardInfo["fld_product_price"] != nil{
            self.product_price = dashboardInfo["fld_product_price"] as? String
        }
        if dashboardInfo["fld_product_id"] != nil{
            self.product_id = dashboardInfo["fld_product_id"] as? String
        }
        if dashboardInfo["fld_product_url"] != nil{
            self.product_url = dashboardInfo["fld_product_url"] as? String
        }
}
    func makelist(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<DashboardM> {
           
           var conversations: Array<DashboardM> = Array<DashboardM>()
           if listArray != nil {
               
               for info in listArray! {
                
                let conversation: DashboardM = DashboardM()
                   conversation.setValue(fromInfo: info)
                   conversations.append(conversation)
               }
           }
           return conversations
       }
}

//2.category
//
//response==
//
//{
//    "status": true,
//    "statusCode": 201,
//    "message": "Home Category",
//    "img_path": "http://crmb2c.org/anessb2b/",
//    "data": [
//        {
//            "id": "1",
//            "name": "tablet",
//            "image": "uploads/category/1571486198.jpg",
//            "status": "1"
//        },
//        {
//            "id": "2",
//            "name": "capsule",
//            "image": "uploads/category/1571486185.jpg",
//            "status": "1"
//        },
//        {
//            "id": "3",
//            "name": "syrup",
//            "image": "uploads/category/1571486173.jpg",
//            "status": "1"
//        }
//    ]
//}
//
//3.banner
//
//response==
//{
//    "status": true,
//    "statusCode": 201,
//    "message": "Home Banner",
//    "img_path": "http://crmb2c.org/anessb2b/uploads/",
//    "data": [
//        {
//            "id": "10",
//            "name": "Best Discount",
//            "content": "<h3>Best Discount</h3>",
//            "image": "538e58fb60528ce2a7a2e4dd2dcc5c97.jpg",
//            "link": "#",
//            "status": "1",
//            "created_at": "2019-10-19 10:57:26"
//        },
//        {
//            "id": "11",
//            "name": "Best Discount",
//            "content": "<h3>Best Discount</h3>",
//            "image": "538e58fb60528ce2a7a2e4dd2dcc5c97.jpg",
//            "link": "#",
//            "status": "1",
//            "created_at": "2019-10-19 10:57:10"
//        },
//        {
//            "id": "12",
//            "name": "Best Discount",
//            "content": "<p>tst</p>",
//            "image": "538e58fb60528ce2a7a2e4dd2dcc5c97.jpg",
//            "link": "#",
//            "status": "1",
//            "created_at": "2019-10-19 11:10:37"
//        }
//    ]
//}
//
//4.dealproduct
//{
//    "status": true,
//    "statusCode": 201,
//    "message": "Home Deal Product",
//    "img_path": "http://crmb2c.org/anessb2b/",
//    "data": [
//        {
//            "fld_product_id": "114",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_name": "Met-Neurobion OD Capsule",
//            "fld_product_url": "met-neurobion-od-capsule",
//            "fld_product_sku": "test123",
//            "fld_product_price": "100",
//            "fld_product_mrpprice": "200",
//            "fld_product_qty": "0",
//            "salt": "1",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p>Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_descrip": "<p>Whether omeprazole has a salt or not really results in little difference is how well it works, although, according the FDA, they are not bioequivalent or interchangeable. In fact, they have different uses based on the clinical studies completed. Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_size": "3",
//            "fld_product_image": "Chrysanthemum5.jpg",
//            "fld_product_date": "2019-10-17 11:38:42",
//            "fld_product_status": "1",
//            "tax": "5",
//            "formula": "",
//            "expiry_date": "",
//            "product_type": "Salted"
//        },
//        {
//            "fld_product_id": "115",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_name": "Met-Neurobion OD Capsule1",
//            "fld_product_url": "met-neurobion-od-capsule1",
//            "fld_product_sku": "1234",
//            "fld_product_price": "100",
//            "fld_product_mrpprice": "250",
//            "fld_product_qty": "0",
//            "salt": "0",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_size": "3",
//            "fld_product_image": "Chrysanthemum6.jpg",
//            "fld_product_date": "2019-10-17 17:55:33",
//            "fld_product_status": "1",
//            "tax": "5",
//            "formula": "yeryty",
//            "expiry_date": "",
//            "product_type": "Paracetamol"
//        }
//    ]
//}
//5.exclusive
//
//{
//    "status": true,
//    "statusCode": 201,
//    "message": "Home Deal Product",
//    "img_path": "http://crmb2c.org/anessb2b/",
//    "data": [
//        {
//            "fld_product_id": "114",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_name": "Met-Neurobion OD Capsule",
//            "fld_product_url": "met-neurobion-od-capsule",
//            "fld_product_sku": "test123",
//            "fld_product_price": "100",
//            "fld_product_mrpprice": "200",
//            "fld_product_qty": "0",
//            "salt": "1",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p>Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_descrip": "<p>Whether omeprazole has a salt or not really results in little difference is how well it works, although, according the FDA, they are not bioequivalent or interchangeable. In fact, they have different uses based on the clinical studies completed. Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_size": "3",
//            "fld_product_image": "Chrysanthemum5.jpg",
//            "fld_product_date": "2019-10-17 11:38:42",
//            "fld_product_status": "1",
//            "tax": "5",
//            "formula": "",
//            "expiry_date": "",
//            "product_type": "Salted"
//        },
//        {
//            "fld_product_id": "115",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_name": "Met-Neurobion OD Capsule1",
//            "fld_product_url": "met-neurobion-od-capsule1",
//            "fld_product_sku": "1234",
//            "fld_product_price": "100",
//            "fld_product_mrpprice": "250",
//            "fld_product_qty": "0",
//            "salt": "0",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_size": "3",
//            "fld_product_image": "Chrysanthemum6.jpg",
//            "fld_product_date": "2019-10-17 17:55:33",
//            "fld_product_status": "1",
//            "tax": "5",
//            "formula": "yeryty",
//            "expiry_date": "",
//            "product_type": "Paracetamol"
//        }
//    ]
//}
//
//6.best
//{
//    "status": true,
//    "statusCode": 201,
//    "message": "Home Deal Product",
//    "img_path": "http://crmb2c.org/anessb2b/",
//    "data": [
//        {
//            "fld_product_id": "114",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_name": "Met-Neurobion OD Capsule",
//            "fld_product_url": "met-neurobion-od-capsule",
//            "fld_product_sku": "test123",
//            "fld_product_price": "100",
//            "fld_product_mrpprice": "200",
//            "fld_product_qty": "0",
//            "salt": "1",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p>Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_descrip": "<p>Whether omeprazole has a salt or not really results in little difference is how well it works, although, according the FDA, they are not bioequivalent or interchangeable. In fact, they have different uses based on the clinical studies completed. Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_size": "3",
//            "fld_product_image": "Chrysanthemum5.jpg",
//            "fld_product_date": "2019-10-17 11:38:42",
//            "fld_product_status": "1",
//            "tax": "5",
//            "formula": "",
//            "expiry_date": "",
//            "product_type": "Salted"
//        },
//        {
//            "fld_product_id": "115",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_name": "Met-Neurobion OD Capsule1",
//            "fld_product_url": "met-neurobion-od-capsule1",
//            "fld_product_sku": "1234",
//            "fld_product_price": "100",
//            "fld_product_mrpprice": "250",
//            "fld_product_qty": "0",
//            "salt": "0",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_size": "3",
//            "fld_product_image": "Chrysanthemum6.jpg",
//            "fld_product_date": "2019-10-17 17:55:33",
//            "fld_product_status": "1",
//            "tax": "5",
//            "formula": "yeryty",
//            "expiry_date": "",
//            "product_type": "Paracetamol"
//        }
//    ]
//}
//
