//
//  OrderM.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class OrderM: NSObject {

     var order_id: Int?
     var order_date: String = ""
     var product_id: Int?
     var product_name: String = ""
     var product_image: String = ""
     var product_type: String = ""
     var order_quantity: Int?
     var order_price: String = ""
}



/*
15.  http://crmb2c.org/anessb2b/apis/order_listing

Request

{
    "fld_user_id":"12"
}

Response

{
    "status": true,
    "statusCode": 201,
    "data": [
        {
            "fld_order_id": "1",
            "fld_order_date": "2019-12-26",
            "fld_product_id": "114",
            "fld_product_name": "Met-Neurobion OD Capsule",
            "fld_product_image": "Chrysanthemum5.jpg",
            "fld_product_type": "Salted",
            "fld_order_quantity": "2",
            "fld_order_price": "10"
        }
    ],
    "message": "Order Listing"
}
 */
