//
//  ListingM.swift
//  LyfeCure_App
//
//  Created by Ruchi EL on 06/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
let key_fld_user_id = "fld_user_id"
let key_fld_group_id = "fld_group_id"
let key_fld_order_id = "fld_order_id"
let key_fld_order_quantity = "fld_order_quantity"
let key_fld_order_price = "fld_order_price"
let key_fld_cat_id = "fld_cat_id"
let key_fld_customer_id = "fld_customer_id"


class ListingM : NSObject {
    
    var user_id : Int?
    var product_id : Int?
    var product_quantity : Int?
    var product_name : String? = ""
    var product_image : String? = ""
    var product_type : String? = ""
    var product_mrpprice : String? = ""
    var product_price : String? = ""
     var product_url : String? = ""
    var shipping_charges : String? = ""
    var grand_total : String? = ""
    var message: String? = ""
    


func setValue(fromListInfo listInfo: Dictionary<String,Any>) -> Void {
    if listInfo["message"] != nil{
        self.message = listInfo["message"] as? String
    }
    if listInfo["fld_user_id"] != nil{
        self.user_id = listInfo["fld_user_id"] as? Int
    }
    if listInfo["fld_product_id"] != nil{
        self.product_id = listInfo["fld_product_id"] as? Int
    }
    if listInfo["fld_product_qty"] != nil{
        self.product_quantity = listInfo["fld_product_qty"] as? Int
    }
    if listInfo["fld_product_name"] != nil{
           self.product_name = listInfo["fld_product_name"] as? String
       }
    if listInfo["fld_product_image"] != nil{
           self.product_image = listInfo["fld_product_image"] as? String
       }
    if listInfo["product_type"] != nil{
           self.product_type = listInfo["product_type"] as? String
       }
    if listInfo["fld_product_url"] != nil{
              self.product_url = listInfo["fld_product_url"] as? String
          }
    if listInfo["fld_product_mrpprice"] != nil{
        self.product_mrpprice = listInfo["fld_product_mrpprice"] as? String
       }
    if listInfo["fld_product_price"] != nil{
           self.product_price = listInfo["fld_product_price"] as? String
       }
    if listInfo["shipping_charges"] != nil{
           self.shipping_charges = listInfo["shipping_charges"] as? String
       }
    if listInfo["grand_total"] != nil{
           self.grand_total = listInfo["grand_total"] as? String
       }
}
    func makelist(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<ListingM> {
                 
                 var conversations: Array<ListingM> = Array<ListingM>()
                 if listArray != nil {
                     
                     for info in listArray! {
                         
                      let conversation: ListingM = ListingM()
                       conversation.setValue(fromListInfo: info)
                       conversations.append(conversation)
                     }
                 }
                 return conversations
       }
    
    
 func getListingInfo() -> Dictionary<String, Any> {
       var listInfo: Dictionary<String, Any> = Dictionary<String, Any>()
    if self.message != nil{
        listInfo["message"] = self.message
    }
    if self.user_id != nil{
        listInfo["fld_user_id"] = self.user_id
    }
    if self.product_id != nil{
        listInfo["fld_product_id"] = self.product_id
    }
    if self.product_quantity != nil{
        listInfo["fld_product_qty"] = self.product_quantity
    }
    if self.product_name != nil{
           listInfo["fld_product_name"] = self.product_name
       }
    if self.product_image != nil{
           listInfo["fld_product_image"] = self.product_image
       }
    if self.product_type != nil{
           listInfo["product_type"] = self.product_type
       }
    if self.product_mrpprice != nil{
        listInfo["fld_product_mrpprice"] = product_mrpprice
    }
    if self.product_price != nil{
        listInfo["fld_product_price"] = product_price
    }
    if self.shipping_charges != nil{
        listInfo["shipping_charges"] = shipping_charges
    }
    if self.grand_total != nil{
        listInfo["grand_total"] = grand_total
    }
    
    return listInfo
}
}
//request =
//{
//    "fld_cat_id":"1",
//    "fld_group_id":"1"
//    
//}
//response =
//{
//    "status": true,
//    "statusCode": 201,
//    "message": "Category Wise Product",
//    "img_path": "http://crmb2c.org/anessb2b/images/product_images/",
//    "data": [
//        {
//            "fld_product_id": "116",
//            "type": "0",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_url": "zolmitriptan-zomig-",
//            "fld_product_sku": "325325",
//            "fld_product_qty": "0",
//            "salt": "0",
//            "manufacturer": "SunPharma",
//            "fld_product_short_descrip": "<p>sdfdsfds</p>",
//            "fld_product_descrip": "<p>dsfsdfsd</p>",
//            "fld_product_size": "2",
//            "fld_product_date": "2019-11-28 16:38:25",
//            "tax": "34",
//            "formula": "",
//            "expiry_date": "",
//            "product_type": "Triptans",
//            "fld_product_name": "Zolmitriptan (Zomig)",
//            "fld_product_image": "Test1.png",
//            "fld_product_mrpprice": "4000",
//            "fld_product_price": "3000"
//        },
//        {
//            "fld_product_id": "115",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_url": "met-neurobion-od-capsule1",
//            "fld_product_sku": "1234",
//            "fld_product_qty": "0",
//            "salt": "0",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
//            "fld_product_size": "3",
//            "fld_product_date": "2019-10-17 17:55:33",
//            "tax": "5",
//            "formula": "yeryty",
//            "expiry_date": "",
//            "product_type": "Paracetamol",
//            "fld_product_name": "Met-Neurobion OD Capsule1",
//            "fld_product_image": "Chrysanthemum6.jpg",
//            "fld_product_mrpprice": "126",
//            "fld_product_price": "22"
//        },
//        {
//            "fld_product_id": "114",
//            "type": "1",
//            "fld_cat_id": "1",
//            "fld_scat_id": "2",
//            "fld_brand_id": "1",
//            "fld_product_url": "met-neurobion-od-capsule",
//            "fld_product_sku": "test123",
//            "fld_product_qty": "0",
//            "salt": "1",
//            "manufacturer": "India",
//            "fld_product_short_descrip": "<p>Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_descrip": "<p>Whether omeprazole has a salt or not really results in little difference is how well it works, although, according the FDA, they are not bioequivalent or interchangeable. In fact, they have different uses based on the clinical studies completed. Prescription Prilosec is indicated for multiple acid-related conditions, like ulcer treatment or erosive esophagitis, while Prilosec OTC is only indicated for &ldquo;heartburn&rdquo;. However, your doctor may suggest you use Prilosec OTC for other &ldquo;off-label&rdquo; conditions, especially if your insurance does not cover the prescription drug due to the availability of the OTC product. Another reason: erosive esophagitis is a condition that should not be treated without first consulting a doctor. Heartburn can be treated short-term with OTC medications</p>",
//            "fld_product_size": "3",
//            "fld_product_date": "2019-10-17 11:38:42",
//            "tax": "5",
//            "formula": "",
//            "expiry_date": "",
//            "product_type": "Salted",
//            "fld_product_name": "Met-Neurobion OD Capsule",
//            "fld_product_image": "Chrysanthemum5.jpg",
//            "fld_product_mrpprice": "4500",
//            "fld_product_price": "400"
//        }
//    ]
//}
