//
//  SearchM.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
let key_fld_search_text = "fld_search_text"



class SearchM: NSObject {
    
    var user_id : Int?
    var type : String?
    var product_id : String?
    var cat_id : String?
    var scat_id : String? = ""
    var brand_id : String? = ""
    var product_url : String? = ""
    var product_sku : String? = ""
    var product_qty : String? = ""
    var product_name : String? = ""
    var product_image : String? = ""
    var message: String? = ""
    
    
    func setValue(fromListInfo listInfo: Dictionary<String,Any>) -> Void {
        if listInfo["fld_product_id"] != nil{
            self.product_id = listInfo["fld_product_id"] as? String
        }
        if listInfo["type"] != nil{
            self.type = listInfo["type"] as? String
        }
        if listInfo["fld_cat_id"] != nil{
            self.cat_id = listInfo["fld_cat_id"] as? String
        }
        if listInfo["fld_scat_id"] != nil{
            self.scat_id = listInfo["fld_scat_id"] as? String
        }
        if listInfo["fld_brand_id"] != nil{
            self.brand_id = listInfo["fld_brand_id"] as? String
        }
        
        if listInfo["fld_product_url"] != nil{
            self.product_url = listInfo["fld_product_url"] as? String
        }
        if listInfo["fld_product_image"] != nil{
            self.product_image = listInfo["fld_product_image"] as? String
        }
        if listInfo["fld_product_sku"] != nil{
            self.product_sku = listInfo["fld_product_sku"] as? String
        }
        if listInfo["fld_product_qty"] != nil{
            self.product_qty = listInfo["fld_product_qty"] as? String
        }
        
        if listInfo["fld_product_name"] != nil{
            self.product_name = listInfo["fld_product_name"] as? String
        }
        if listInfo["fld_product_image"] != nil{
            self.product_image = listInfo["fld_product_image"] as? String
        }
        
        
        
    }
    
    func makeList(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<SearchM> {
                    
                    var conversations: Array<SearchM> = Array<SearchM>()
                    if listArray != nil {
                        
                        for info in listArray! {
                            
                         let conversation: SearchM = SearchM()
                          conversation.setValue(fromListInfo: info)
                          conversations.append(conversation)
                        }
                    }
                    return conversations
          }
}


/*
 =====
 
 http://crmb2c.org/anessb2b/apis/search_productlist
 
 Request
 
 {
 "fld_group_id":"1",
 "fld_search_text":"Met-Neurobion OD Capsule1"
 }
 
 Response
 
 {
 "status": true,
 "statusCode": 201,
 "message": "Search Product Listing",
 "img_path": "http://crmb2c.org/anessb2b/images/product_images/",
 "data": [
 {
 "fld_product_id": "115",
 "type": "1",
 "fld_cat_id": "1",
 "fld_scat_id": "2",
 "fld_brand_id": "1",
 "fld_product_url": "met-neurobion-od-capsule1",
 "fld_product_sku": "1234",
 "fld_product_qty": "0",
 "salt": "0",
 "manufacturer": "India",
 "fld_product_short_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
 "fld_product_descrip": "<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>",
 "fld_product_size": "3",
 "fld_product_date": "2019-10-17 17:55:33",
 "tax": "5",
 "formula": "yeryty",
 "expiry_date": "",
 "product_type": "Paracetamol",
 "fld_product_name": "Met-Neurobion OD Capsule1",
 "fld_product_image": "Chrysanthemum6.jpg",
 "fld_product_mrpprice": "126",
 "fld_product_price": "22"
 }
 ]
 }
 */
