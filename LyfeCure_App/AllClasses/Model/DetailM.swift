//
//  DetailM.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit
let key_fld_product_id = "fld_product_id"
let key_fld_product_price = "fld_product_price"
//let key_fld_group_id = "fld_group_id"

class DetailM: NSObject {

    var product_id: String?
    var user_id : String?
    var type: Int?
    var cat_id : Int?
    var scat_id: Int?
    var brand_id: Int?
    var product_url: String? = ""
    var product_sku: String? = ""
    var product_qty: String? = ""
    var salt: Int?
    var manufacturer: String? = ""
    var product_short_descrip: String? = ""
    var product_descrip: String? = ""
    var product_size: Int?
    var product_date: String? = ""
    var tax: String? = ""
    var formula: String? = ""
    var expiry_date : String? = ""
    var product_type: String? = ""
    var product_name: String? = ""
    var product_image: String? = ""
    var product_mrpprice: String? = ""
    var product_price: String? = ""
    var message: String? = ""
   
    func setValue(fromCartDetailInfo cartDetailInfo: Dictionary<String,Any>) -> Void {
          if cartDetailInfo["message"] != nil{
              self.message = cartDetailInfo["message"] as? String
          }
          if cartDetailInfo["fld_user_id"] != nil{
              self.user_id = cartDetailInfo["fld_user_id"] as? String
          }
          if cartDetailInfo["fld_product_id"] != nil{
              self.product_id = cartDetailInfo["fld_product_id"] as? String
          }
//          if cartDetailInfo["fld_product_qty"] != nil{
//              self.product_quantity = cartDetailInfo["fld_product_qty"] as? String
//          }
          if cartDetailInfo["fld_product_name"] != nil{
                 self.product_name = cartDetailInfo["fld_product_name"] as? String
             }
          if cartDetailInfo["fld_product_image"] != nil{
                 self.product_image = cartDetailInfo["fld_product_image"] as? String
             }
          if cartDetailInfo["product_type"] != nil{
                 self.product_type = cartDetailInfo["product_type"] as? String
             }
          if cartDetailInfo["fld_product_mrpprice"] != nil{
              self.product_mrpprice = cartDetailInfo["fld_product_mrpprice"] as? String
             }
          if cartDetailInfo["fld_product_price"] != nil{
                 self.product_price = cartDetailInfo["fld_product_price"] as? String
             }
          if cartDetailInfo["fld_product_short_descrip"] != nil{
                 self.product_short_descrip = cartDetailInfo["fld_product_short_descrip"] as? String
             }
          if cartDetailInfo["fld_product_descrip"] != nil{
                 self.product_descrip = cartDetailInfo["fld_product_descrip"] as? String
             }
      }
}


/*
7.getdetails
request =

{
    "fld_product_id":"116",
    "fld_group_id":"1"
}
response=
{
    "status": true,
    "statusCode": 201,
    "message": "Home Deal Product",
    "img_path": "http://crmb2c.org/anessb2b/",
    "more_image": [],
    "data": {
        "fld_product_id": "116",
        "type": "0",
        "fld_cat_id": "1",
        "fld_scat_id": "2",
        "fld_brand_id": "1",
        "fld_product_url": "zolmitriptan-zomig-",
        "fld_product_sku": "325325",
        "fld_product_qty": "0",
        "salt": "0",
        "manufacturer": "SunPharma",
        "fld_product_short_descrip": "<p>sdfdsfds</p>",
        "fld_product_descrip": "<p>dsfsdfsd</p>",
        "fld_product_size": "2",
        "fld_product_date": "2019-11-28 16:38:25",
        "tax": "34",
        "formula": "",
        "expiry_date": "",
        "product_type": "Triptans",
        "fld_product_name": "Zolmitriptan (Zomig)",
        "fld_product_image": "Test1.png",
        "fld_product_mrpprice": "4000",
        "fld_product_price": "3000"
 }*/
