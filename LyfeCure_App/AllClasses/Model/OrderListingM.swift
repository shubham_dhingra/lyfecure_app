//
//  OrderListingM.swift
//  LyfeCure_App
//
//  Created by SHUBHAM DHINGRA on 2/21/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit

class OrderListingM : NSObject {
    
    var order_id : String? = ""
    var order_date : String? = ""
    var order_amt : String? = ""
    var order_shipping_total : String? = ""
   
    


func setValue(fromListInfo listInfo: Dictionary<String,Any>) -> Void {
    if listInfo["fld_order_id"] != nil{
        self.order_id = listInfo["fld_order_id"] as? String
    }
    if listInfo["fld_order_amt"] != nil{
        self.order_amt = listInfo["fld_order_amt"] as? String
    }
    if listInfo["fld_order_date"] != nil{
        self.order_date = listInfo["fld_order_date"] as? String
    }
    if listInfo["fld_order_shipping_total"] != nil{
        self.order_shipping_total = listInfo["fld_order_shipping_total"] as? String
    }
}
    
    func makelist(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<OrderListingM> {
                 
                 var conversations: Array<OrderListingM> = Array<OrderListingM>()
                 if listArray != nil {
                     
                     for info in listArray! {
                         
                      let conversation: OrderListingM = OrderListingM()
                       conversation.setValue(fromListInfo: info)
                       conversations.append(conversation)
                     }
                 }
                 return conversations
       }
    
    
// func getListingInfo() -> Dictionary<String, Any> {
//       var listInfo: Dictionary<String, Any> = Dictionary<String, Any>()
//    if self.message != nil{
//        listInfo["message"] = self.message
//    }
//    if self.user_id != nil{
//        listInfo["fld_user_id"] = self.user_id
//    }
//    if self.product_id != nil{
//        listInfo["fld_product_id"] = self.product_id
//    }
//    if self.product_quantity != nil{
//        listInfo["fld_product_qty"] = self.product_quantity
//    }
//    if self.product_name != nil{
//           listInfo["fld_product_name"] = self.product_name
//       }
//    if self.product_image != nil{
//           listInfo["fld_product_image"] = self.product_image
//       }
//    if self.product_type != nil{
//           listInfo["product_type"] = self.product_type
//       }
//    if self.product_mrpprice != nil{
//        listInfo["fld_product_mrpprice"] = product_mrpprice
//    }
//    if self.product_price != nil{
//        listInfo["fld_product_price"] = product_price
//    }
//    if self.shipping_charges != nil{
//        listInfo["shipping_charges"] = shipping_charges
//    }
//    if self.grand_total != nil{
//        listInfo["grand_total"] = grand_total
//    }
//    
//    return listInfo
//}
}
