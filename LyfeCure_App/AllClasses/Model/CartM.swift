//
//  CartM.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 08/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

let key_fld_product_qty = "fld_product_qty"

class CartM: NSObject {

             var user_id : String?
             var product_id : String?
             var product_quantity : String?
             var product_name : String? = ""
             var product_image : String? = ""
             var product_type : String? = ""
             var product_mrpprice : String? = ""
             var product_price : String? = ""
             var shipping_charges  :Int?
             var grand_total :Int?
             var message: String? = ""
   
    
    func setValue(fromCartInfo cartInfo: Dictionary<String,Any>) -> Void {
        if cartInfo["message"] != nil{
            self.message = cartInfo["message"] as? String
        }
        if cartInfo["fld_user_id"] != nil{
            self.user_id = cartInfo["fld_user_id"] as? String
        }
        if cartInfo["fld_product_id"] != nil{
            self.product_id = cartInfo["fld_product_id"] as? String
        }
        if cartInfo["fld_product_qty"] != nil{
            self.product_quantity = cartInfo["fld_product_qty"] as? String
        }
        if cartInfo["fld_product_name"] != nil{
               self.product_name = cartInfo["fld_product_name"] as? String
           }
        if cartInfo["fld_product_image"] != nil{
               self.product_image = cartInfo["fld_product_image"] as? String
           }
        if cartInfo["product_type"] != nil{
               self.product_type = cartInfo["product_type"] as? String
           }
        if cartInfo["fld_product_mrpprice"] != nil{
            self.product_mrpprice = cartInfo["fld_product_mrpprice"] as? String
           }
         if cartInfo["fld_product_price"] != nil{
               self.product_price = cartInfo["fld_product_price"] as? String
           }
        if let shipping_charges = cartInfo["shipping_charges"] as? Int{
      //  if cartInfo["shipping_charges"] != nil{
               self.shipping_charges = shipping_charges
           }
        if let grandtotal = cartInfo["grand_total"] as? Int{
             //  if cartInfo["shipping_charges"] != nil{
                      self.grand_total = grandtotal
           }
//        if cartInfo["grand_total"] != nil{
//               self.grand_total = cartInfo["grand_total"] as? String
//           }
    }
    
    func makelist(_ listArray: Array<Dictionary<String, Any>>? , imgpath : String) -> Array<CartM> {
              
              var conversations: Array<CartM> = Array<CartM>()
              if listArray != nil {
                  
                  for info in listArray! {
                      
                   let conversation: CartM = CartM()
                    conversation.setValue(fromCartInfo: info)
                    conversations.append(conversation)
                  }
              }
              
             return conversations
    }
     func getListingInfo() -> Dictionary<String, Any> {
           var cartInfo: Dictionary<String, Any> = Dictionary<String, Any>()
        if self.message != nil{
            cartInfo["message"] = self.message
        }
        if self.user_id != nil{
            cartInfo["fld_user_id"] = self.user_id
        }
        if self.product_id != nil{
            cartInfo["fld_product_id"] = self.product_id
        }
        if self.product_quantity != nil{
            cartInfo["fld_product_qty"] = self.product_quantity
        }
        if self.product_name != nil{
               cartInfo["fld_product_name"] = self.product_name
           }
        if self.product_image != nil{
               cartInfo["fld_product_image"] = self.product_image
           }
        if self.product_type != nil{
               cartInfo["product_type"] = self.product_type
           }
        if self.product_mrpprice != nil{
            cartInfo["fld_product_mrpprice"] = product_mrpprice
        }
        if self.product_price != nil{
            cartInfo["fld_product_price"] = product_price
        }
        if self.shipping_charges != nil{
            cartInfo["shipping_charges"] = shipping_charges
        }
        if self.grand_total != nil{
            cartInfo["grand_total"] = grand_total
        }
        
        return cartInfo
    }
    }
    //request=


/*

9.cart_listing=
request=

{
    "fld_user_id":"11",
    "fld_group_id":"1"
}
response=

{
    "status": true,
    "statusCode": 201,
    "data": [
        {
            "fld_user_id": "11",
            "fld_product_id": "116",
            "fld_product_qty": "1",
            "fld_product_name": "Zolmitriptan (Zomig)",
            "fld_product_image": "Test1.png",
            "product_type": "Triptans",
            "fld_product_mrpprice": "4000",
            "fld_product_price": "3000"
        }
    ],
    "shipping_charges": "0",
    "grand_total": "3000",
    "message": "Cart Listing"
}
10.cart_remove
request =

{
    "fld_product_id":"116",
    "fld_user_id":"11"

}

response =
{
    "status": true,
    "statusCode": 201,
    "message": "Product deleted from cart"
}
11.cart_update
request=

{
    "fld_product_id":"116",
    "fld_product_qty":"2",
    "fld_user_id":"11"
    
}
response =
{
    "status": true,
    "statusCode": 201,
    "message": "Product updated from cart"
 }
 */
