//
//  AppDelegate.swift
//  LyfeCure_App
//
//  Created by MacBook Pro on 02/02/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

extension UIViewController {
func topMostViewController() -> UIViewController {
    
    if let presented = self.presentedViewController {
        return presented.topMostViewController()
    }
    
    if let navigation = self as? UINavigationController {
        return navigation.visibleViewController?.topMostViewController() ?? navigation
    }
    
//    if let tab = self as? UITabBarController {
//        return tab.selectedViewController?.topMostViewController() ?? tab
//    }
    
    return self
  }
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    //var mainVC:SJSwiftSideMenuController!
    var localLanguage : LocalLanguage = LocalLanguage()
    let rechability = Reachability()
    var fromvc : String?
     var window: UIWindow?
//    var containerObj : DashboardTableViewController?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
         UINavigationBar.appearance().barStyle = .blackOpaque
        
        if #available(iOS 13.0, *) {
                      // Always adopt a light interface style.
          self.window?.overrideUserInterfaceStyle = .light
        }
        
        SuccessfulLogin()
        return true
    }

    func SuccessfulLogin()  {
           
//        let mainVc = SJSwiftSideMenuController()
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let rootVc = (storyBoard.instantiateViewController(withIdentifier: "DashboardTableViewController") as! DashboardTableViewController)
//          // self.containerObj!.selectedIndex = 0
//       //    let mainVC = SJSwiftSideMenuController()
//           let slideLeftVC = storyBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuViewController
////           let navObj = UINavigationController.init(rootViewController: mainDashboardVc)
//         
//        SJSwiftSideMenuController.setUpNavigation(rootController: rootVc, leftMenuController: slideLeftVC, rightMenuController: slideLeftVC, leftMenuType: .SlideOver, rightMenuType: .SlideView)
////        SJSwiftSideMenuController.setUpNavigation(rootController: containerObj!, leftMenuController: menuVC, rightMenuController: nil, leftMenuType: .SlideView, rightMenuType: .SlideView)
////
//           SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .LEFT)
//           SJSwiftSideMenuController.enableDimbackground = true
//           SJSwiftSideMenuController.leftMenuWidth = 300
//           
//        self.window?.rootViewController = mainVc
//        self.window?.makeKeyAndVisible()
           
       }
    class func getAppDelegate() -> AppDelegate
       {
           return UIApplication.shared.delegate as! AppDelegate
       }
   
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

