//
//  UDKeys.swift
//  LyfeCure_App
//
//  Created by SHUBHAM DHINGRA on 2/21/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import Foundation
import UIKit

enum UDKeys: String{
    
    case UserID   = "UserID"

    func save(_ value: Any) {
        
        switch self{
            
        default:
            UserDefaults.standard.set(value, forKey: self.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    func fetch() -> Any? {
        
        switch self{
        default:
            guard let value = UserDefaults.standard.value(forKey: self.rawValue) else { return nil}
            return value
        }
    }
    
    func remove() {
        UserDefaults.standard.removeObject(forKey: self.rawValue)
    }
    
}
