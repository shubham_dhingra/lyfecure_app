//
//  Keys.swift
//  WalletMaxico
//
//  Created by Ruchi EL on 30/09/19.
//  Copyright © 2019 Ruchi EL. All rights reserved.
//

import Foundation

//***** For User
let key_userdefaults_user = "user_info"
let key_user = "user"

let key_user_id: String           = "user_id"
let key_user_type: String         = "user_type"
let key_user_first_name: String   = "first_name"
let key_user_last_name: String    = "last_name"
let key_user_email: String        = "email"
let key_user_countrycode: String  = "country_code"
let key_user_mobileno   : String  = "mobile_no"
let key_image_info: String        = "photo"
let key_user_image_thumb: String  = "thumb"
let key_user_data: String         = "user"
let key_user_name: String         = "name"
let key_user_gender: String      = "gender"
let key_user_otp: String          = "otp_verify"
let key_user_password: String     = "password"
let key_user_phone: String        = "contact_no"
let key_user_identity: String     = "identity"
let key_user_language: String     = "locale"
let key_auth_token_header: String = "api_auth_token"
let key_device_token: String      = "Device-Token"
let key_response_msg: String      = "message"
let key_auth_token: String        = "jwt_token"
let key_user_flag: String         = "otp_flag"
let key_food_trend_id: String     = "food_trend_id"
let key_page_no: String           = "page_number"
let key_next_page: String         = "next_page"
let key_lat: String               = "lat"
let key_lng: String               = "lan"
let key_banner_no: String         = "banner_no"
