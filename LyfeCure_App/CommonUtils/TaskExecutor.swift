//
//  TaskExecutor.swift
//  Glam
//
//  Created by ShailshNailwal on 12/1/17.
//  Copyright © 2017 Shailsh. All rights reserved.
//

import UIKit

extension String {
    func containsEmoji() -> Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x3030, 0x00AE, 0x00A9,// Special Characters
            0x1D000...0x1F77F,          // Emoticons
            0x2100...0x27BF,            // Misc symbols and Dingbats
            0xFE00...0xFE0F,            // Variation Selectors
            0x1F900...0x1F9FF:          // Supplemental Symbols and Pictographs
                return true
            default:
                continue
            }
        }
        return false
    }
}

class TaskExecutor: NSObject {
    
    //---------- instantiate view controller ----------//
    static func instantiateViewControllerFrom(storyBoardName storyboardName:String, withIdentifier identifier:String) -> UIViewController {
        
        let storyboard:UIStoryboard = UIStoryboard.init(name: storyboardName, bundle: nil)
        let viewController:UIViewController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        return viewController
    }
    
    static func instantiateViewControllerFromMainStoryBoard(withIdentifier identifier:String) -> UIViewController {
        
        let viewController:UIViewController = TaskExecutor.instantiateViewControllerFrom(storyBoardName: "Main", withIdentifier: identifier)
        
        return viewController
    }
    
    static func instantiateViewControllerFromOtherStoryBoard(withIdentifier identifier:String) -> UIViewController {
        
        let viewController:UIViewController = TaskExecutor.instantiateViewControllerFrom(storyBoardName: "Other", withIdentifier: identifier)
        
        return viewController
    }
    
    static func instantiateViewControllerFromGourmetStoryboard(withIdentifier identifier:String) -> UIViewController {
        
        let viewController:UIViewController = TaskExecutor.instantiateViewControllerFrom(storyBoardName: "GourmetStoryboard", withIdentifier: identifier)
        
        return viewController
    }
    
    static func instantiateViewControllerFromChatStoryBoard(withIdentifier identifier:String) -> UIViewController {
        
        let viewController:UIViewController = TaskExecutor.instantiateViewControllerFrom(storyBoardName: "Chat", withIdentifier: identifier)
        
        return viewController
    }
    
    //---------- validation ----------//
    static func isValidEmail(_ email:String) -> Bool {
        
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: email)
    }
    
    static func isValidName(_ name:String) -> Bool {
        
        let finalString = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let regEx = "[A-Za-z][A-Z0-9a-z._ ]{2,64}"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: finalString)
    }
    
    static func isValidPassword(_ password:String) -> Bool {
        
        let regEx = "[A-Z0-9a-z._ ]{6,64}"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: password)
    }
    
    static func isEmojiFound(aString: String) -> Bool {
        
        for scalar in aString.unicodeScalars {
            
            switch scalar.value {
                
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F//,   // Variation Selectors
                //            0x0030...0x0039,
                //            0x00A9...0x00AE,
                //            0x203C...0x2049,
                //            0x2122...0x3299,
                //            0x1F004...0x1F251,
                //            0x1F910...0x1F990
                :
                return true
            default:
                break
            }
        }
        return false
    }
    
    //---------- Alert and Tost ----------//
    static func showAlert(withMessage message:String, onViewController viewController:UIViewController) {
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.default) { (action) in
        }
        alert.addAction(okAction)
        viewController.present(alert, animated: false) {
        }
    }
    
    static func showTost(withMessage message:String, onViewController viewController:UIViewController) {
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        viewController.present(alert, animated: false) {
            
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (timer) in
                
                TaskExecutor.dismissTost(alert)
            })
        }
    }
    
    static func showTost(withMessage message:String, onViewController viewController:UIViewController, forTime time: TimeInterval) {
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        viewController.present(alert, animated: false) {
            
            Timer.scheduledTimer(withTimeInterval: time, repeats: false, block: { (timer) in
                
                TaskExecutor.dismissTost(alert)
            })
        }
    }
    
    static func dismissTost(_ alert:UIAlertController) {
        
        alert.dismiss(animated: false) {}
    }
    
    //MARK: - Image Base64
    static func base64String(fromImage image: UIImage) -> String {
        
        var base64String: String = ""
        
        let imageData: Data = image.jpegData(compressionQuality: 0.90)!// UIImagePNGRepresentation(image)!
        base64String = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        
        return String.init(format: "data:image/jpeg;base64,%@", base64String)
    }
    
    static func base64String(fromImageData imageData: Data) -> String {
        
        var base64String: String = ""
        
//        let imageData: Data = UIImageJPEGRepresentation(image, 0.90)!// UIImagePNGRepresentation(image)!
        base64String = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        
        return String.init(format: "data:image/jpeg;base64,%@", base64String)
    }
    
    static func image(fromBase64String string: String) -> UIImage {
        
        let dataDecoded : Data = Data(base64Encoded: string, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage != nil ? decodedimage! : UIImage()
    }
    
//    static func image(fromURLString urlStr:String) -> UIImage? {
//        
//        var useURLString = urlStr
//        
//        if !(useURLString.starts(with: APICallExecutor.baseURL)) {
//            
//            useURLString = APICallExecutor.baseURL + urlStr
//        }
//        
//        var img: UIImage?
//        do
//        {
//            
//            let imageData: Data? = try Data.init(contentsOf: URL.init(string: useURLString)!)
//            
//            if imageData != nil {
//                
//                img = UIImage.init(data: imageData!)
//            }
//        }
//        catch {
//            
//            //print("Image found in URL is nil")
//        }
//        
//        return img
//    }
    
//    static func setUserImage(inImageView imageView: UIImageView) {
//
//        let user: User = User.getUserDataFromUserDefaults()
//        if (UIApplication.shared.delegate as! AppDelegate).userImage != nil {
//
//            imageView.image = (UIApplication.shared.delegate as! AppDelegate).userImage
//        }
//        else {
//
//            DispatchQueue.global(qos: .background).async {
//
//                let img = TaskExecutor.image(fromURLString: user.imageURLString!)
//                if img != nil {
//
//                    DispatchQueue.main.async {
//
//                        (UIApplication.shared.delegate as! AppDelegate).userImage = img
//                        imageView.image = img
//                    }
//                }
//            }
//        }
//    }
    
    //MARK: - Date Methods
    private func toGlobalTime(_ date: Date) -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: date))
        return Date(timeInterval: seconds, since: date)
    }
    
    // Convert UTC (or GMT) to local time
    private func toLocalTime(_ date: Date) -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: date))
        return Date(timeInterval: seconds, since: date)
    }
    
    static func dayName(fromDateString dateString: String, withFormate formateString: String) -> String {
        
        let dateStr: String = dateString// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = TaskExecutor().toLocalTime(newDate!)
        formate.dateFormat = "EEEE"
        let dayInWeek = formate.string(from: localDate)
        
        return dayInWeek
    }
    
    static func monthName(fromDateString dateString: String, withFormate formateString: String) -> String {
        
        let dateStr: String = dateString// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = TaskExecutor().toLocalTime(newDate!)
        formate.dateFormat = "LLLL"
        let dayInWeek = formate.string(from: localDate)
        
        return dayInWeek
    }
    
    static func dayNumber(fromDateString dateString: String?, withFormate formateString: String) -> Int {
        
        if dateString == nil {
            
            return 0
        }
        
        let dateStr: String = dateString!// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = TaskExecutor().toLocalTime(newDate!)
        formate.dateFormat = "dd"
        let dayNumberStr = formate.string(from: localDate)
        
        return Int(dayNumberStr)!
    }
    
    static func hour(fromDateString dateString: String?, withFormate formateString: String) -> Int {
        
        if dateString == nil {
            
            return 0
        }
        
        let dateStr: String = dateString!// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = TaskExecutor().toLocalTime(newDate!)
        formate.dateFormat = "HH"
        let hourStr = formate.string(from: localDate)
        
        return Int(hourStr)!
    }
    
    static func minute(fromDateString dateString: String?, withFormate formateString: String) -> Int {
        
        if dateString == nil {
            
            return 0
        }
        
        let dateStr: String = dateString!// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = TaskExecutor().toLocalTime(newDate!)
        formate.dateFormat = "mm"
        let hourStr = formate.string(from: localDate)
        
        return Int(hourStr)!
    }
    
    //MARK:- Other
    static func numberWithPrefixString(forNumber number: Int) -> String {
        
        var prefixString: String? = ""
        
        switch number {
        case 1:
            prefixString = "\(number)st"
            break
        case 2:
            prefixString = "\(number)nd"
            break
        case 3:
            prefixString = "\(number)rd"
            break
        default:
            prefixString = "\(number)th"
        }
        
        return prefixString != nil ? prefixString! : "\(number)"
    }
    
    //MARK: - Gradient add
    static func addAppGradientInBackground(onView view: UIView, withFrame frame: CGRect?, shouldHorizontal isHorizontal: Bool) -> Void {
        
        var rect: CGRect = view.bounds
        
        if frame != nil {
            
            rect = frame!
        }
        
        view.backgroundColor = UIColor.clear
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = rect
        
        if !isHorizontal {
            
            gradient.startPoint = CGPoint.init(x: 0.5, y: 0.0)
            gradient.endPoint   = CGPoint.init(x: 0.5, y: 1)
        }
        else {
            
            gradient.startPoint = CGPoint.init(x: 0.0, y: 0.5)
            gradient.endPoint   = CGPoint.init(x: 1.0, y: 0.5)
        }
        
       // gradient.colors     = [AppColors.appGradientLight.cgColor,
                          //     AppColors.appGradientMedium.cgColor,
                          //     AppColors.appGradientDark.cgColor]
      //  gradient.opacity    = 1
      //  gradient.isHidden   = false
      //  view.layer.insertSublayer(gradient, at: 0)
    }
    
    static func addAppGradientInBackground(withColors colors:Array<CGColor>, onView view: UIView, withFrame frame: CGRect?, shouldHorizontal isHorizontal: Bool) -> Void {
        
        var rect: CGRect = view.bounds
        
        if frame != nil {
            
            rect = frame!
        }
        
        view.backgroundColor = UIColor.clear
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = rect
        
        if !isHorizontal {
            
            gradient.startPoint = CGPoint.init(x: 0.5, y: 0.0)
            gradient.endPoint   = CGPoint.init(x: 0.5, y: 1)
        }
        else {
            
            gradient.startPoint = CGPoint.init(x: 0.0, y: 0.5)
            gradient.endPoint   = CGPoint.init(x: 1.0, y: 0.5)
        }
        
        gradient.colors     = colors
        gradient.opacity    = 1
        gradient.isHidden   = false
        view.layer.insertSublayer(gradient, at: 0)
    }
}
