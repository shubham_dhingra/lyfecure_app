//
//  PlacePickerVC.swift
//  eFavour
//
//  Created by EL Group on 12/06/19.
//  Copyright © 2019 Meena. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

//MARK: Protocol
protocol PlacePickerDelegate: class {
    func updateLocationData(address: String?, lat: Double?, long: Double?)
}

class PlacePickerVC: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, GMSAutocompleteResultsViewControllerDelegate, UISearchBarDelegate {
    
    let locationManager = CLLocationManager()
    var location : CLLocation?
    var currentPlace : GMSPlace!
    var locationSet : Bool = true
    var placesClient: GMSPlacesClient!
    
    var updatedPlace: GMSAddress!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var fromHomeVC: Bool!
    var isMarkerHidden: Bool! = false
    var isFrist: Bool! = true
    
    var delegate: PlacePickerDelegate?
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var centerImgView: UIImageView!
    @IBOutlet var lblMsgView: UIView!
    @IBOutlet var btnBack: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpMapView()
        
        // Get location
        self.setUpLocation()
        
        if fromHomeVC {
            self.view.isHidden = true
        }
        else {
            self.view.isHidden = false
            self.actInd.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0);
            self.actInd.center = self.mapView.center
            self.actInd.hidesWhenStopped = true
            self.actInd.style = UIActivityIndicatorView.Style.gray
            self.mapView.addSubview(self.actInd)
        }
        self.mapView.delegate = self
        placesClient = GMSPlacesClient.shared()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        navigationController?.setNavigationBarHidden(true, animated: true)
        if fromHomeVC {
            self.centerImgView.isHidden = true
            self.lblMsgView.isHidden = true
        }
        else {
            self.lblMsgView.isHidden = false
            self.mapView.bringSubviewToFront(lblMsgView)
        }
        
        self.btnBack.isHidden = true
        self.mapView.bringSubviewToFront(btnBack)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            if self.fromHomeVC {
                self.searchController?.searchBar.becomeFirstResponder()
            }
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if searchController?.searchBar.text != "" {
            delegate?.updateLocationData(address: self.updatedPlace.lines![0], lat: self.updatedPlace.coordinate.latitude, long: self.updatedPlace.coordinate.longitude)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setUpMapView() -> Void {
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        searchController?.searchBar.tintColor = .darkGray
        searchController?.searchBar.delegate = self
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        self.currentPlace = place
        searchController?.isActive = false
        
        if fromHomeVC {
            delegate?.updateLocationData(address: place.formattedAddress, lat: place.coordinate.latitude, long: place.coordinate.longitude)
            self.dismiss(animated: true, completion: nil)
        }
        else {
            let camera = GMSCameraPosition.camera(withLatitude: self.currentPlace.coordinate.latitude, longitude: self.currentPlace.coordinate.longitude, zoom: 14.0)
            self.mapView!.animate(to: camera)
            searchController?.searchBar.text = place.formattedAddress
        }
        
        print("Place name: \(String(describing: place.name))")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if fromHomeVC {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // Program mark: Setup current location
    func setUpLocation() -> Void {
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = (locations.last)!
        if location != nil && self.locationSet {
            // Do any additional setup after loading the view.
          //  self.loadMap()
            self.locationSet = false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .restricted:
            print("Location Ristricted")
            break
        case .denied:
            print("Location Denied")
            break
        }
    }
    
    // MARK : Custom function
    
    func loadMap() {
        let position = CLLocationCoordinate2DMake((self.location?.coordinate.latitude)!, (self.location?.coordinate.longitude)!)
        _ = GMSMarker(position: position)
        
        // Set camera position and map zoom level
        
        let camera = GMSCameraPosition.camera(withLatitude: (self.location?.coordinate.latitude)!, longitude: (self.location?.coordinate.longitude)!, zoom: 14.0)
        self.mapView!.animate(to: camera)
        
        self.mapView.settings.compassButton = true
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.btnBack.isEnabled = false
        self.searchController?.searchBar.text = ""
        print(mapView.camera.target.latitude)
        print(mapView.camera.target.longitude)
        
        let position = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(position) { response, error in
            //
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            } else {
                if let places = response?.results() {
                    if let place = places.first {
                        
                        self.updatedPlace = places.first
                        
                        if let lines = place.lines {
                            print("GEOCODE: Formatted Address: \(lines[0])")
//                            self.searchController?.searchBar.text = lines[0]
                        }
                        
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                } else {
                    print("GEOCODE: nil in places")
                }
            }
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if isMarkerHidden {
            self.lblMsgView.isHidden = true
            self.centerImgView.isHidden = false
            self.mapView.bringSubviewToFront(self.centerImgView)
            self.isMarkerHidden = false
        }
    }
    
    func mapViewSnapshotReady(_ mapView: GMSMapView) {
        self.isMarkerHidden = true
        self.isFrist = false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !isFrist {
            self.actInd.startAnimating()
            
            _ = Timer.scheduledTimer(timeInterval: 5.0,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: false)
        }
    }
    
    @objc func eventWith(timer: Timer!) {
        let address = updatedPlace.lines
        self.searchController?.searchBar.text = address![0]
        self.actInd.stopAnimating()
        self.btnBack.isEnabled = true
        self.btnBack.isHidden = false
    }
}
