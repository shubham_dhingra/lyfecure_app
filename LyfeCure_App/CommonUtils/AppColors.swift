//
//  AppColors.swift
//  Glam
//
//  Created by ShailshNailwal on 1/25/18.
//  Copyright © 2018 Shailsh. All rights reserved.
//

import UIKit



public class AppColors: NSObject {
    
    public static let appGradientGreen:  UIColor = UIColor.init(red: 35/255.0, green: 141/255.0, blue: 141/255.0, alpha: 1)  //rgba(254,153,73,1)
    public static let appGradientButtonColor: UIColor = UIColor.init(red: 137/255.0, green: 192/255.0, blue: 89/255.0, alpha: 1) //rgba(255,118,105,1)
    public static let appGradientDarkColor:   UIColor = UIColor.init(red: 26/255.0, green: 26/255.0, blue: 26/255.0, alpha: 1)  //rgba(255,85,135,1)
    public static let appGradientlightColor:   UIColor = UIColor.init(red: 241/255.0, green: 239/255.0, blue: 239/255.0, alpha: 1)  //rgba(255,85,135,1)
    public static let appGreen:   UIColor = UIColor.init(red: 61/255.0, green: 171/255.0, blue: 84/255.0, alpha: 1)  //rgba(255,85,135,1)
    public static let textFieldBorder:  UIColor = UIColor.init(red: 116/255.0, green: 116/255.0, blue: 116/255.0, alpha: 1)
    
    public static let golden: UIColor = UIColor.init(red: 255/255.0, green: 201/255.0, blue: 0/255.0, alpha: 1)
     public static let greyBorder: UIColor = UIColor.init(red: 112/255.0, green: 112/255.0, blue: 112/255.0, alpha: 1)
    public static let silver: UIColor = UIColor.init(red: 202/255.0, green: 202/255.0, blue: 202/255.0, alpha: 1)
    public static let greyViewBackground: UIColor = UIColor.init(red: 226/255.0, green: 226/255.0, blue: 226/255.0, alpha: 1)
      public static let upcomingBackground: UIColor = UIColor.init(red: 232/255.0, green: 255/255.0, blue: 244/255.0, alpha: 1)
      public static let pendingBackground: UIColor = UIColor.init(red: 255/255.0, green: 247/255.0, blue: 235/255.0, alpha: 1)
      public static let cancelBackground: UIColor = UIColor.init(red: 255/255.0, green: 239/255.0, blue: 239/255.0, alpha: 1)
      public static let completedBackground: UIColor = UIColor.init(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1)
}

