//
//  GoogleMapVC.swift
//  Glam
//
//  Created by Macbook Pro on 30/12/17.
//  Copyright © 2017 Shailsh. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class AddressCell: UITableViewCell {
    
    @IBOutlet var lblAddress: UILabel!
}

protocol MapControllerDelegate: NSObjectProtocol {

   func mapController(_ mapController: GoogleMapVC, didFinishPickingAddress address:String, addLocation location:CLLocation) -> Void;
}

class GoogleMapVC: UITextFieldDelegate,CLLocationManagerDelegate,   UITableViewDelegate, UITableViewDataSource, GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
    }
    
    @IBOutlet var tblAddress: UITableView!
    @IBOutlet var viewAddressList: UIView!
    @IBOutlet var lcAddressListViewBottom: NSLayoutConstraint!
//  @IBOutlet var lcAddressBottom: NSLayoutConstraint!
    var arrAdrress : Array<GMSAutocompletePrediction> = Array<GMSAutocompletePrediction>()
   
//    let cookerDetails: CookerReviewDetailVC = TaskExecutor.instantiateViewControllerFrom(storyBoardName: "GourmetStoryboard", withIdentifier: "CookerReviewDetailVC") as! CookerReviewDetailVC
    
    var placesClient: GMSPlacesClient?
    var mapView: GMSMapView = GMSMapView()
    var markerSelectLocation: GMSMarker = GMSMarker()
    var markerUserLocation: GMSMarker = GMSMarker()
    var btnDeviceLocation: UIButton = UIButton()
    var btnSelectedLocation: UIButton = UIButton()
//    var btnDone: UIButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 60, height: 60))
//    var viewHeader: UIView = UIView()
    @IBOutlet var viewMapContainer: UIView!
    @IBOutlet var viewTextBG: UIView!
    @IBOutlet var tfAddress: UITextField!
    
    @IBOutlet var viewCookerInfo: UIView!
    @IBOutlet var viewReviewBG: UIView!
    @IBOutlet var viewReviewBGTap: UIView!
    @IBOutlet var btnConfirm: UIButton!
    
//    @IBOutlet var lcInfoWindowBottom: NSLayoutConstraint!
//    @IBOutlet var lcDetailsTop: NSLayoutConstraint!
//    @IBOutlet var lcDetailsLeft: NSLayoutConstraint!
//    @IBOutlet var lcDetailsBottom: NSLayoutConstraint!
//    @IBOutlet var lcDetailsRight: NSLayoutConstraint!
//    @IBOutlet var lcDetailsMainTop: NSLayoutConstraint!
//    @IBOutlet var lcConfirmBottom: NSLayoutConstraint!
//
//    var initialDetailsTopMargin: CGFloat = 0
//    var initialDetailsLeftMargin: CGFloat = 0
//    var initialDetailsRightMargin: CGFloat = 0
    
    var locationManager: CLLocationManager? = (UIApplication.shared.delegate as! AppDelegate).locationManager
    
    var strAddress: String = ""
    var locationSelected: CLLocation? = (UIApplication.shared.delegate as! AppDelegate).locationManager.location
   // var arrCooker: Array<Cooker> = Array<Cooker>()

    weak var delegate:MapControllerDelegate? = nil
    
    var alert:UIAlertController? = nil
    
   // var cuisine: Cuisine?
  //  var order: Order?//
    var currentLatitude = Double()
    var currentLongitude = Double()

    var mapAdded: Bool = false
    var markerTapped: Bool = false
    
    var currentSelection: Int = -1
    
//    @IBOutlet weak var imageBubble: UIImageView!
    
  override func viewDidLoad() {
        
       // self.shouldAddBackButton = true
      //  self.viewTopGradient = self.view
        
        super.viewDidLoad()
        
        placesClient = GMSPlacesClient()
        
        tfAddress.delegate = self
        
//        lblTitle.frame = CGRect.init(x: 0, y: 0, width: 150, height: 30)
//        self.navigationItem.titleView = lblTitle
//        lblTitle.text = self.localLanguage.strHeaderFindCooker
        
//        initialDetailsTopMargin   = lcDetailsTop.constant
//        initialDetailsLeftMargin  = lcDetailsLeft.constant
//        initialDetailsRightMargin = lcDetailsRight.constant
//
//        lcDetailsMainTop.isActive = false
       
        
//        btnDone.backgroundColor = AppColors.appGradientMedium
//        btnDone.setImage(UIImage.init(named: "icon_selection_check"), for: UIControlState.normal)
//        btnDone.addTarget(self, action: #selector(doneAddressPicking(_:)), for: UIControlEvents.touchUpInside)
//        btnDone.clipsToBounds         = true
//        btnDone.layer.cornerRadius    = btnDone.frame.size.height/2
        
//        btnDeviceLocation.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
//        btnDeviceLocation.setImage(UIImage.init(named: "icon_gps"), for: UIControlState.normal)
//        btnDeviceLocation.layer.cornerRadius = btnDeviceLocation.bounds.size.width/2
//        btnDeviceLocation.clipsToBounds         = true
//        btnDeviceLocation.backgroundColor       = UIColor.clear
//        btnDeviceLocation.addTarget(self, action: #selector(goToDeviceLocation(_:)), for: UIControlEvents.touchUpInside)
//        self.view.addSubview(btnDeviceLocation)
//
//        btnSelectedLocation.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
//        btnSelectedLocation.setImage(UIImage.init(named: "icon_focus"), for: UIControlState.normal)
//        btnSelectedLocation.layer.cornerRadius    = btnSelectedLocation.bounds.size.width/2
//        btnSelectedLocation.clipsToBounds         = true
//        btnSelectedLocation.backgroundColor       = UIColor.white.withAlphaComponent(0.6)
//        btnSelectedLocation.addTarget(self, action: #selector(goToSelectedLocation(_:)), for: UIControlEvents.touchUpInside)
//        self.view.addSubview(btnSelectedLocation)
//
//        locationManager = (UIApplication.shared.delegate as! AppDelegate).locationManager
//
//        if locationManager?.location != nil {
//
//            markerUserLocation.position = (locationManager?.location?.coordinate)!
//        }
//
//        let backBtn:UIButton = UIButton.init()
//        backBtn.frame = CGRect.init(x: 20, y: 40, width: 40, height: 40)
//        backBtn.setTitle("←", for: UIControlState.normal)
//        backBtn.setTitleColor(.white, for: UIControlState.normal)
//        backBtn.addTarget(self, action: #selector(goBack(_:)), for: UIControlEvents.touchUpInside)
//
//        backBtn.layer.cornerRadius    = backBtn.frame.size.height/2
////        backBtn.layer.shouldRasterize = true
//        backBtn.clipsToBounds         = true
//        backBtn.backgroundColor       = UIColor.white.withAlphaComponent(0.4)
//
       // self.viewHeader.addSubview(backBtn)
        
//        let path:String = Bundle.main.path(forResource: "MapStyle", ofType: "json")!
//        var style:GMSMapStyle? = nil
//
//        do {
//
//        try style = GMSMapStyle.init(contentsOfFileURL: URL.init(fileURLWithPath: path)) //styleWithContentsOfFileURL:[NSURL fileURLWithPath:path] error:nil];
//        }
//        catch {
//
//        }
//
//        if (style == nil) {
//
//            //print("The style definition could not be loaded");
//        }
//        else {
//
//              mapView.mapStyle = style
//        }
        
//        self.view.addSubview(viewHeader)
//        self.view.addSubview(imageBubble)

        //// for address input box ////
        let addressView: UIView = UIView()
        addressView.backgroundColor = UIColor.white.withAlphaComponent(0.80)
        addressView.clipsToBounds   = true
        addressView.frame           = CGRect.init(x: 20,
                                                  y: 60,
                                                  width: UIScreen.main.bounds.size.width - 40,
                                                  height: 25)
        
//        tfAddress.attributedPlaceholder = NSAttributedString(string: self.localLanguage.strAskAddressOrZip, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
//        viewTextBG.layer.masksToBounds = false
//        viewTextBG.layer.shadowColor = UIColor.lightGray.cgColor
//        viewTextBG.layer.shadowOpacity = 0.6
//        viewTextBG.layer.shadowOffset = CGSize(width: 0, height: 5)
//        viewTextBG.layer.shadowRadius = 6
//
//        viewCookerInfo.layer.masksToBounds = false
//        viewCookerInfo.layer.shadowColor = UIColor.lightGray.cgColor
//        viewCookerInfo.layer.shadowOpacity = 0.6
//        viewCookerInfo.layer.shadowOffset = CGSize(width: 0, height: 5)
//        viewCookerInfo.layer.shadowRadius = 6
//
//        let infoTap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(cookerDetailBackgroundTapped(_:)))
//        viewReviewBGTap.addGestureRecognizer(infoTap)
//
//        self.mapView.backgroundColor = .white
//        self.mapView.delegate = self
//
//        TaskExecutor.addAppGradientInBackground(onView: btnConfirm, withFrame: nil, shouldHorizontal: true)
        
        tfAddress.addTarget(self, action: #selector(search(_:)), for: UIControl.Event.editingChanged)
    }
    
//    override open func viewWillAppear(_ animated: Bool) {
//        
//        super.viewWillAppear(animated)
//        locationManager?.startUpdatingLocation()
//        
//        //// reset cooker details view ////
//        
//        if cookerDetails != nil && cookerDetails.btnSeeMore != nil {
//            
//            cookerDetails.btnSeeMore.isSelected = true
//            cookerDetails.btnSeeMore.tintColor  = UIColor.clear
//            cookerDetails.seeMoreReview(cookerDetails.btnSeeMore)
//        }
//    }
    
    func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        tblAddress.estimatedRowHeight = 50
        tblAddress.rowHeight = UITableView.automaticDimension
        locationManager?.startUpdatingLocation()
      //  tblAddress.isHidden = true
      //  viewAddressList.isHidden = true
       // tblAddress.contentInset = UIEdgeInsetsMake(0, 0, 40, 0)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIResponder.keyboardWillHideNotification, object: nil)
        //// reset cooker details view ////
        
      //  if cookerDetails != nil && cookerDetails.btnSeeMore != nil {
            
        //    cookerDetails.btnSeeMore.isSelected = true
         //   cookerDetails.btnSeeMore.tintColor  = UIColor.clear
//            cookerDetails.seeMoreReview(UITapGestureRecognizer())
        //}
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            self.lcAddressListViewBottom.constant = keyboardHeight
            //             self.tblBottom.constant = self.viewBottom.constant
            
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.lcAddressListViewBottom.constant  = 0
            
        }
    }
    
//    override open func viewDidLayoutSubviews() {
//
//        super.viewDidLayoutSubviews()
//
//        if mapAdded {
//
//            return
//        }
//
//        var frame:CGRect  = self.view.bounds
//        frame.size.height = frame.size.height - 120
//        frame.size.width  = frame.size.width - 0
//        frame.origin.x    = 0
//        frame.origin.y    = 120
//
//        mapView.frame         = frame
//        mapView.clipsToBounds = true
//
//        frame = btnDeviceLocation.frame
//        frame.origin.x = self.view.bounds.size.width - btnDeviceLocation.frame.size.width - 30
//        frame.origin.y = self.view.bounds.size.height - btnDeviceLocation.frame.size.height - 30
//        btnDeviceLocation.frame = frame
//
//        frame = btnSelectedLocation.frame
//        frame.origin.x = 30
//        frame.origin.y = self.view.bounds.size.height - btnSelectedLocation.frame.size.height - 30
//        btnSelectedLocation.frame = frame
//
//        self.viewMapContainer.addSubview(self.mapView)
//        mapView.frame = self.viewMapContainer.bounds
//
//        cookerDetails.view.frame = self.viewReviewBG.bounds
//
//        viewReviewBG.addSubview(cookerDetails.view)
//        cookerDetails.controllerViewCycleRefresh()
//        viewReviewBG.superview?.isHidden = true
//    }
    
    override open func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if mapAdded {
            
            return
        }
        mapAdded = true
        
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
        
        if locationManager?.location != nil {
            
            markerSelectLocation.position    = (locationManager?.location?.coordinate)!
            markerSelectLocation.map         = mapView
            markerSelectLocation.isDraggable = true
            
            mapView.animate(to: GMSCameraPosition.init(target: markerSelectLocation.position, zoom: 12, bearing: 0, viewingAngle: 0))
            
            self.coordinateToAddress((locationManager?.location?.coordinate)!)
            
           / self.requestToFindCookerList(aroundLocation: locationManager?.location?.coordinate)
            
        //    lcConfirmBottom.constant = lcConfirmBottom.constant + self.view.safeAreaInsets.bottom
        }
        markerSelectLocation.map = mapView
        
//        btnDone.center = CGPoint.init(x: self.view.frame.size.width/2, y: self.view.frame.size.height-btnDone.frame.size.height - 20)
//        self.view.addSubview(btnDone)
        
        //// reseting view if previously opened ////
//        self.lcDetailsMainTop.isActive = false
//        self.lcDetailsRight.isActive   = false
//        self.lcDetailsLeft.isActive    = false
//        
//        self.cookerDetails.btnSeeMore.isSelected = false
//        
//        self.lcDetailsTop.constant   = self.initialDetailsTopMargin
//        self.lcDetailsLeft.constant  = self.initialDetailsLeftMargin
//        self.lcDetailsRight.constant = self.initialDetailsRightMargin
        ////*********************************////
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
//        locationManager?.delegate = (UIApplication.shared.delegate as! AppDelegate)
    }

    
    //MARK: - Others
    func makeRegion(withRadius radius: Double) -> Void {
        
        let geoFenceCircle1: GMSCircle = GMSCircle();
        geoFenceCircle1.radius = radius; // Meters
        geoFenceCircle1.position = (locationManager?.location?.coordinate)!; // Some CLLocationCoordinate2D position
        geoFenceCircle1.fillColor = UIColor.red.withAlphaComponent(0.1)
        geoFenceCircle1.strokeWidth = 0;
        geoFenceCircle1.strokeColor = UIColor.clear
        geoFenceCircle1.map = mapView;
    }
    
//    func placeAutocomplete(textSearch : String) -> Void {
//        let filter = GMSAutocompleteFilter()
//        filter.type = .establishment
//        placesClient?.autocompleteQuery(textSearch, bounds: nil, filter: filter, callback: {(results, error) -> Void in
//            if let error = error {
//                //print("Autocomplete error \(error)")
//                return
//            }
//            if let results = results {
//                for result in results {
//                    //print("Result \(result.attributedFullText) with placeID \(String(describing: result.placeID))")
//                }
//            }
//        })
//    }
    
    //MARK: - Responders
    @objc func goToDeviceLocation(_ sender:UIButton) -> Void {
        
        if locationManager?.location == nil {
            
            return
        }
        
        mapView.animate(to: GMSCameraPosition.init(target: (locationManager?.location?.coordinate)!, zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0))
    }
    
    @objc func goToSelectedLocation(_ sender:UIButton) -> Void {
        
        mapView.animate(to: GMSCameraPosition.init(target: markerSelectLocation.position, zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0))
    }
    
//    @objc func goBack(_ sender:UIButton) -> Void {
//
//        self.dismiss(animated: true, completion: {})
//    }
    
//    @objc func doneAddressPicking(_ sender:UIButton) -> Void {
//
//        if self.locationSelected == nil {
//
//            alert = UIAlertController.init(title: "", message: self.localLanguage.strMsgLocationCheckWait, preferredStyle: .alert)
//            self.present(alert!, animated: true, completion: {
//
//                self.locationManager?.startUpdatingLocation()
//            })
//
//            return
//        }
//
//        if self.delegate != nil && (self.delegate?.responds(to: #selector(MapControllerDelegate.mapController(_:didFinishPickingAddress:addLocation:))))! {
//
//            self.delegate?.mapController!(self, didFinishPickingAddress: self.strAddress, addLocation: self.locationSelected!)
//        }
//
//        self.dismiss(animated: true, completion: {})
//    }
    
    @objc func cookerDetailBackgroundTapped(_ sender: UIButton) {
        
        viewReviewBG.superview?.isHidden = true
    }
    
    @IBAction func changeDetailsViewSize(_ sender: UIButton) {
        
    }
    
//    @IBAction func confirmCooker(_ sender: UIButton) {
//
//        if currentSelection == 0 {
//
//            TaskExecutor.showTost(withMessage: self.localLanguage.strMsgSelectCooker, onViewController: self, forTime: 2)
//            return
//        }
//
//        self.order?.cookerID = arrCooker[self.currentSelection-1].id
//        self.order?.cooker   = arrCooker[self.currentSelection-1]
//
//        let selectDish: CookerDishListToOrderVC = TaskExecutor.instantiateViewControllerFromGourmetStoryboard(withIdentifier: "CookerDishListToOrderVC") as! CookerDishListToOrderVC
//        selectDish.order = self.order
//        self.navigationController?.pushViewController(selectDish, animated: true)
//    }
    
//    @objc func search(_ sender: Any) {
//
//        self.placeAutocomplete(textSearch: tfAddress.text!)
//    }
    
    //MARK: - Address found
    func coordinateToAddress(_ coordinate: CLLocationCoordinate2D) -> Void {
        
        locationSelected = CLLocation.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
      
        GMSGeocoder.init().reverseGeocodeCoordinate(coordinate) { (response, error) in
            
            if response != nil && ((response?.results()) != nil) {
                
                var addressStr: String = ""
                for address:GMSAddress in (response?.results())! {
                    
                    let a = Double((address.lines?.count)!)
                    let b = Double(0)
                    if a > b {
                        
                        addressStr = (address.lines?.joined(separator: ", "))!
                    }
                    break
                }
                
                DispatchQueue.main.async {
                    
                    self.strAddress = addressStr
                    self.tfAddress.text = addressStr
                }
            }
        }
    }
    
    func addressToCoordinate(_ addressStr:String) -> Void {
        
        DispatchQueue.main.async {
            
          //  MBProgressHUD.showAdded(to: self.view, animated: false)
        }
        
        strAddress = addressStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let finalAddress: String! = addressStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)!
        
        var params:Dictionary<String, Any>! = Dictionary()
        params["sensor"] = false
        params["address"] = finalAddress
        APICallExecutor.getRequestForURLString(false, "http://maps.google.com/maps/api/geocode/json", Dictionary<String,String>(), params) { (success, error, resultSuccessStatus, result, message, isActiveSession) in
            
            var results:Any?      = nil
            var singleResult:Any? = nil
            var geometry:Any?     = nil
            var loation:Any?      = nil
            var lat:Double?       = nil
            var lon:Double?       = nil
            
            if success && result != nil {
                
                if ((result as! [String: Any])["results"] != nil) {
                    
                    results = (result as! [String: Any])["results"]
                }
                
                if (results != nil && (results as! [Any]).count > 0) {
                    
                    singleResult = (results as! [Any])[0]
                }
                
                if (singleResult != nil && ((singleResult as! [String: Any])["geometry"] != nil)) {
                    
                    geometry = (singleResult as! [String: Any])["geometry"]
                }
                
                if (geometry != nil && (geometry is [String: Any])) {
                    
                    loation = (geometry as! [String: Any])["location"]
                }
                
                if (loation != nil) {
                    
                    lat = (loation as! [String: Any])["lat"] as? Double
                    lon = (loation as! [String: Any])["lng"] as? Double
                }
                
                if (lat != nil && lon != nil) {
                    
                    DispatchQueue.main.async {
                        
                        self.locationSelected = CLLocation.init(latitude: (lat)!, longitude: (lon)!)
                      //  self.moveMarker(atLocation: CLLocationCoordinate2D.init(latitude: (lat)!, longitude: (lon)!))
                        
                        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { (timer) in
                            
                       //     MBProgressHUD.hide(for: self.view, animated: false)
                        })
                    }
                }
                else {
                    
                    DispatchQueue.main.async {
                        
                        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { (timer) in
                            
                     //       MBProgressHUD.hide(for: self.view, animated: false)
                        })
                    }
                }
            }
            else {
                
                DispatchQueue.main.async {
                    
                 //   MBProgressHUD.hide(for: self.view, animated: false)
                    
                    Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { (timer) in
                        
                   //     TaskExecutor.showTost(withMessage: self.localLanguage.strMsgLocationError, onViewController: self)
                    })
                }
            }
        }
    }
    
//    func moveMarker(atLocation location: CLLocationCoordinate2D) -> Void {
//
//        markerSelectLocation.position = location
//        mapView.animate(to: GMSCameraPosition.init(target: markerSelectLocation.position, zoom: self.mapView.camera.zoom, bearing: 0, viewingAngle: 0))
//    }
    
//    MARK: - Textfield
//    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//        self.addressToCoordinate(textField.text!)
//        self.view.endEditing(true)
//        return true
//    }
    //MARK: - TextField
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//
//            let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//
//            // Specify the place data types to return.
////            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
////                UInt(GMSPlaceField.placeID.rawValue))!
////            autocompleteController.placeFields = fields
//
//            // Specify a filter.
//            let filter = GMSAutocompleteFilter()
//            filter.type = .address
//            autocompleteController.autocompleteFilter = filter
//
//            // Display the autocomplete view controller.
//            present(autocompleteController, animated: true, completion: nil)
//
//
////        let token = GMSAutocompleteSessionToken.init()
////
////        // Create a type filter.
////        let filter = GMSAutocompleteFilter()
////        filter.type = .establishment
////
////        placesClient?.findAutocompletePredictions(fromQuery: "cheesebu",
////                                                  bounds: nil,
////                                                  boundsMode: GMSAutocompleteBoundsMode.bias,
////                                                  filter: filter,
////                                                  sessionToken: token,
////                                                  callback: { (results, error) in
////
////
////                                                    if let error = error {
////                                                        print("Autocomplete error: \(error)")
////                                                        return
////                                                    }
////                                                    if let results = results {
////                                                        for result in results {
////                                                            print("Result \(result.attributedFullText) with placeID \(result.placeID)")
////                                                        }
////                                                    }
////        })
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.count == 0 {
            viewAddressList.isHidden = true
            tblAddress.isHidden = true
            lcAddressListViewBottom.constant = 0
            //            tblBottom.constant = 0
            arrAdrress.removeAll()
        }
        
        return true
    }
    
    @IBAction func search(_ sender: Any) {
        
        if tfAddress.text == "" {
            viewAddressList.isHidden = true
            tblAddress.isHidden = true
            lcAddressListViewBottom.constant = 0
            //            tblBottom.constant = 0
        }
        arrAdrress.removeAll()
        self.placeAutocomplete(textSearch: tfAddress.text!)
        
    }
    
    
    func placeAutocomplete(textSearch : String) -> Void {
        
//        placesClient?.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
//            if let error = error {
//                print("Pick Place error: \(error.localizedDescription)")
//                return
//            }
//            
//            
//            if let placeLikelihoodList = placeLikelihoodList {
//                let place = placeLikelihoodList.likelihoods.first?.place
//                
//                
//                if let place = place {
//                    
//                }
//            }
//        })
//        return;
        
//        self.placeAutocomplete(textSearch)
//        return;
        
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        
        placesClient?.autocompleteQuery(textSearch, bounds: nil, filter: filter, callback: {(results, error) -> Void in
            
            if let error = error {
                
                self.arrAdrress.removeAll()
                self.tblAddress.isHidden = true
                self.viewAddressList.isHidden = true
                print("Autocomplete error \(error)")
                return
            }
            
            if let results = results {
                
                for result in results {
                    
                    self.arrAdrress .append(result)
                    
                    //print("Result \(result.attributedFullText) with placeID \(String(describing: result.placeID))")
                }
                
                
                self.tblAddress.reloadData()
                if self.arrAdrress.count > 0 {
                    
                    self.tblAddress.isHidden = false
                    self.viewAddressList.isHidden = false
                }
                else{
                    
                    self.tblAddress.isHidden = true
                    self.viewAddressList.isHidden = true
                }
            }
        })
    }
    
    func placeAutocomplete(_ text: String) {
        let visibleRegion = mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(coordinate: visibleRegion.farLeft, coordinate: visibleRegion.nearRight)
        
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        placesClient?.autocompleteQuery(text, bounds: bounds, filter: filter, callback: {
            (results, error) -> Void in
            guard error == nil else {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
        })
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrAdrress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAddress.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
        
        if arrAdrress.count > 0 {
            
            let objAddress: GMSAutocompletePrediction = arrAdrress[indexPath.row]
            let string  = objAddress.attributedPrimaryText.string
            let string2 = objAddress.attributedSecondaryText?.string
            let address = string + string2!
            cell.lblAddress.text = address
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objAddress = arrAdrress[indexPath.row]
        let string = objAddress.attributedPrimaryText.string
        let string2 =  objAddress.attributedSecondaryText?.string
        let address = string + string2!
        tfAddress.text = address
        viewAddressList.isHidden = true
        tfAddress.resignFirstResponder()
        
        placesClient?.lookUpPlaceID(objAddress.placeID, callback: { (place, error) in
            
            DispatchQueue.main.async {
                
                if place?.coordinate != nil {
                    
                   // self.mapView.animate(toLocation: (place?.coordinate)!)
                }
            }
           // self.requestToFindCookerList(aroundLocation: place?.coordinate)
        })
        
//        return;
//        
//        let geoCoder = CLGeocoder()
//        geoCoder.geocodeAddressString(address) { (placemarks, error) in
//            guard
//                let placemarks = placemarks,
//                let location = placemarks.first?.location
//                else {
//                    // handle no location found
//                    //print("No loction found")
//                    return
//            }
//            
//            let long = location.coordinate.longitude
//            let lat  = location.coordinate.latitude
//            //print("%@ , %@",long ,lat)
//            
//            let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D.init(latitude: long, longitude: lat)
//            self.requestToFindCookerList(aroundLocation: coordinate)
//            // Use your location
//        }
    }
    
//    //MARK: - Details Delegate
//    func cookerReviewDetail(details: CookerReviewDetailVC, withShowMoreStatus status: Bool) {
//
//        UIView.animate(withDuration: 0.5) {
//
//            self.lcDetailsMainTop.isActive = status
//            self.lcDetailsRight.isActive   = status
//            self.lcDetailsLeft.isActive    = status
//
//            if status {
//
//                self.lcDetailsTop.constant   = -30
//                self.lcDetailsRight.constant = 10
//                self.lcDetailsLeft.constant  = 10
//            }
//            else {
//
//                self.lcDetailsTop.constant   = self.initialDetailsTopMargin
//                self.lcDetailsLeft.constant  = self.initialDetailsLeftMargin
//                self.lcDetailsRight.constant = self.initialDetailsRightMargin
//            }
//
//            self.view.layoutIfNeeded()
//        }
//    }

//    // MARK: - GOOGLE MAP
//    public func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
//
////        markerSelectLocation.position = position.target;
////        markerSelectLocation.appearAnimation = GMSMarkerAnimation.none;
//    }
//
//    public func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
//
//        mapView.animate(to: GMSCameraPosition.init(target: markerSelectLocation.position, zoom: self.mapView.camera.zoom, bearing: 0, viewingAngle: 0))
//
//        self.coordinateToAddress(marker.position)
//    }
//
//    public func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
//
//    }
    
//    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//
//        self.view.endEditing(true)
//    }
    
//    public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//
//        markerTapped = true
//
//        self.mapView.padding = UIEdgeInsets.init(top: mapView.bounds.size.height - 2*(64), left: 0, bottom: 0, right: 0)
//        mapView.animate(to: GMSCameraPosition.init(target: marker.position, zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0))
//
//        //// reseting view if previously opened ////
//        self.lcDetailsMainTop.isActive = false
//        self.lcDetailsRight.isActive   = false
//        self.lcDetailsLeft.isActive    = false
//
//        self.cookerDetails.btnSeeMore.isSelected = false
//
//        self.lcDetailsTop.constant   = self.initialDetailsTopMargin
//        self.lcDetailsLeft.constant  = self.initialDetailsLeftMargin
//        self.lcDetailsRight.constant = self.initialDetailsRightMargin
//        ////*********************************////
//
//        if marker.zIndex > 0 {
//
//            Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { (timer) in
//
//                self.viewReviewBG.superview?.isHidden = false
//            })
//
//            currentSelection = Int(marker.zIndex)
//           // cookerDetails.cooker = arrCooker[currentSelection-1]
//           // cookerDetails.controllerViewCycleRefresh()
//        }
//
//        return true
//    }
    
    // MARK: - Location manager
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        alert?.dismiss(animated: false, completion: { })
        currentLatitude  = (manager.location?.coordinate.latitude)!
        currentLongitude = (manager.location?.coordinate.longitude)!
        //self.requestToFindCookerList(aroundLocation: manager.location?.coordinate)
        
        manager.stopUpdatingLocation()
        
//        markerSelectLocation.position    = (locationManager?.location?.coordinate)!
//        markerSelectLocation.map         = mapView
//        markerSelectLocation.isDraggable = true
//
//        mapView.animate(to: GMSCameraPosition.init(target: markerSelectLocation.position, zoom: self.mapView.camera.zoom, bearing: 0, viewingAngle: 0))
//
//        self.coordinateToAddress((locationManager?.location?.coordinate)!)
//
//        self.makeRegion(withRadius: 20000)
////        self.makeRegion(withRadius: 3000)
//
//        if markerTapped {
//
//            self.mapView.padding = UIEdgeInsets.init(top: mapView.bounds.size.height - (64 - self.view.safeAreaInsets.bottom), left: 0, bottom: 0, right: 0)
//        }
        
        self.locationManager?.delegate = (UIApplication.shared.delegate as! AppDelegate)
        
        //        CATransition.begin()
        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            self.markerSelectLocation.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            //            self.markerSelectLocation.rotation = Double(Data["bearing"] ?? 0.0)
        })
        
        //            [CATransaction begin];
        //        [CATransaction setValue:[NSNumber numberWithFloat:2.0] forKey:kCATransactionAnimationDuration]; [CATransaction setCompletionBlock:^{ driverMarker.groundAnchor = CGPointMake(0.5, 0.5); driverMarker.rotation = [[data valueForKey:@"bearing"] doubleValue]; //New bearing value from backend after car movement is done }];
    }
    
//    //MARK: - API calls
//    func requestToFindCookerList(aroundLocation locationCoordinate: CLLocationCoordinate2D?) -> Void {
//
//        viewReviewBG.superview?.isHidden = true
//
//        if(locationCoordinate != nil) {
//
//            var withgourmentCategoryDict:Dictionary<String, Any> = [:]
//
//            order?.latitude  = locationCoordinate?.latitude
//            order?.longitude = locationCoordinate?.longitude
//
//            withgourmentCategoryDict["category_id"] = order?.cuisineID
//            withgourmentCategoryDict["is_delivery"] = order?.ishomeDeliveryAvailable
//            withgourmentCategoryDict["latitude"]    = order?.latitude
//            withgourmentCategoryDict["longitude"]   = order?.longitude
//            withgourmentCategoryDict["is_halal"]    = order?.isHalal
//            withgourmentCategoryDict["is_kosher"]   = order?.isKosher
//
////            withgourmentCategoryDict["category_id"] = cuisine?.id
////            withgourmentCategoryDict["is_delivery"] = false
////            withgourmentCategoryDict["latitude"]    = String.init(format: "%f", (locationManager?.location?.coordinate.latitude)!)
////            withgourmentCategoryDict["longitude"]   = String.init(format: "%f", (locationManager?.location?.coordinate.longitude)!)
////            withgourmentCategoryDict["is_halal"]    = true
////            withgourmentCategoryDict["is_kosher"]   = false
//
//            if self.navigationController != nil {
//
//                MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
//            }
//
//            CookerOperation().findCookerList(withgourmentCategoryDict: withgourmentCategoryDict) { (done, error, successResult, result, msg, isActiveSession) in
//
//                DispatchQueue.main.async {
//
//                    if self.navigationController != nil {
//
//                        MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: false)
//                    }
//                }
//
//                if !isActiveSession {
//
//                    self.logoutOnSessionExpired()
//                    return
//                }
//
//                if result != nil {
//
//                    if successResult {
//
//                        if (result as! Dictionary<String, Any>)[key_user] != nil {
//
//                            var cookerList: Array<Cooker> = CookerOperation().makeList(fromInfoList: (result as! Dictionary<String, Any>)[key_user] as? Array<Dictionary<String, Any>>)
//
//                            cookerList = cookerList.filter({$0.bankAccountAdded == true})
//
//                            self.arrCooker.removeAll()
//                            self.arrCooker.append(contentsOf: cookerList)
//
//                            DispatchQueue.main.async {
//
//                                self.mapView.clear()
//                                self.markerUserLocation.map = self.mapView
//                                self.markerUserLocation.zIndex = 0
//                                //print(self.arrCooker.count)
//                                for i in 0..<self.arrCooker.count {
//
//                                    let cooker: Cooker = self.arrCooker[i]
//
////                                    let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: Double(cooker.cookerLatitude)!, longitude: Double(cooker.cookerLongitude)!, zoom: self.mapView.camera.zoom)
//                                    //                                    self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
//                                    //                                    self.mapView.camera = camera
//
//                                    let marker = GMSMarker()
//                                    marker.position = CLLocationCoordinate2D(latitude:Double( cooker.cookerLatitude)!, longitude: Double(cooker.cookerLongitude)!)
//                                    marker.title  = cooker.address
//                                    marker.icon   = UIImage.init(named: "icon_location")
//                                    //  marker.snippet = "Australia"
//                                    marker.map    = self.mapView
//                                    marker.zIndex = Int32(i+1)
////                                    marker.appearAnimation = GMSMarkerAnimation.pop
//
//                                }
//
//                            }
//                        }
//                    }
//                    else {
//
//                        if (result as! Dictionary<String, Any>)[key_response_msg] != nil {
//
//                            DispatchQueue.main.async {
//
//                                TaskExecutor.showTost(withMessage: ((result as! Dictionary<String, Any>)[key_response_msg] as! String), onViewController: self, forTime: tost_time)
//                            }
//                        }
//                        else {
//
//                            DispatchQueue.main.async {
//
//                                TaskExecutor.showTost(withMessage: self.localLanguage.strMsgServerDataError, onViewController: self, forTime: tost_time)
//                            }
//                        }
//                    }
//                }
//                else {
//
//                    DispatchQueue.main.async {
//                        TaskExecutor.showTost(withMessage: self.localLanguage.strMsgNetworkError, onViewController: self, forTime: tost_time)
//                    }
//                }
//            }
//        }
//    }
//

}
