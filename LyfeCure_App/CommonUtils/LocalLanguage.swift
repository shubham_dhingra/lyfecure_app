//
//  LocalLanguage.swift
//  Cook4
//
//  Created by Shailsh on 20/09/18.
//  Copyright © 2018 Shailsh Nailwal. All rights reserved.
//

import UIKit

class LocalLanguage: NSObject {
    
    let strMsgServerRequestError = NSLocalizedString("Server Request Error!", comment: "adadfadf")
    
    let strMsgServerDataError    = NSLocalizedString("Data error", comment: "adadfadf")
    let strMsgNetworkError       = NSLocalizedString("Network connection error.", comment: "adadfadf")
    let strMsgLocationError       = NSLocalizedString("Location could not find", comment: "adadfadf")
    let strMsgError       = NSLocalizedString("An error occured", comment: "adadfadf")
    let strMsgSessionEndError       = NSLocalizedString("Session did not ended correctly", comment: "adadfadf")
    let strMsgSessionEndedError       = NSLocalizedString("Session did not end correctly", comment: "adadfadf")
    let strMsgLocationCouldNotFind       = NSLocalizedString("Location could not find", comment: "adadfadf")
    let strMsgDocumentUploadFail       = NSLocalizedString("Document could not be uploaded!", comment: "adadfadf")
    let strMsgServerRequestFail       = NSLocalizedString("Request could not be completed!", comment: "adadfadf")
    let strMsgErrorPageLoad       = NSLocalizedString("Error in loding the page!", comment: "adadfadf")
    
    let strMsgSelectImageDialogue = NSLocalizedString("Select image from", comment: "adadfadf")
    let strMsgSelectMedia = NSLocalizedString("Please select media first!", comment: "adadfadf")

    let strMsgSelectDate = NSLocalizedString("Please select date!", comment: "adadfadf")
    let strMsgSelectTime = NSLocalizedString("Please select time!", comment: "adadfadf")
    let strMsgViewLessReview = NSLocalizedString("View Less Reviews", comment: "adadfadf")
    let strMsgViewMoreReview = NSLocalizedString("View More Reviews", comment: "adadfadf")
    let strMsgStatusUpdate = NSLocalizedString("Your order status has been updated", comment: "adadfadf")
    let strMsgFirstNameEmpty = NSLocalizedString("First Name can not be empty!", comment: "adadfadf")
     let strMsgLastNameEmpty = NSLocalizedString("Last Name can not be empty!", comment: "adadfadf")
    let strMsgEmailEmpty = NSLocalizedString("Email Address can not be empty!", comment: "adadfadf")
    let strMsgEmailInvalid = NSLocalizedString("Email Address is invalid!", comment: "adadfadf")
    let strMsgSelectGender = NSLocalizedString("Select your gender!", comment: "adadfadf")
    let strMsgSelectIdentity = NSLocalizedString("Select your identity!", comment: "adadfadf")
    let strMsgAgreetoterms = NSLocalizedString("Agree to terms to move further!", comment: "adadfadf")
    let strMsgDOBEmpty = NSLocalizedString("Date Of Birth can not be empty!", comment: "adadfadf")
    let strMsgPasswordEmpty = NSLocalizedString("Password can not be empty!", comment: "adadfadf")
     let strMsgconfirmPasswordEmpty = NSLocalizedString("Confirm password can not be empty!", comment: "adadfadf")
    let strMsgPasswordLength = NSLocalizedString("Password length should be greater of equal to 6 characters!", comment: "adadfadf")
    let strMsgPasswordsMatch = NSLocalizedString("Password and Confirm Password do not match!", comment: "adadfadf")
    let strMsgAddressEmpty = NSLocalizedString("Address can not be empty!", comment: "adadfadf")
    let strMsgSelectFirstSpeciality = NSLocalizedString("Please select First Speciality!", comment: "adadfadf")
     let strMsgMobileEmpty = NSLocalizedString("Phone no. can not be empty!", comment: "adadfadf")
     let strMsgNameEmpty = NSLocalizedString("Name. can not be empty!", comment: "adadfadf")
        let strMsgOTPEmpty = NSLocalizedString("OTP can not be empty!", comment: "adadfadf")
      let strMsgMobileInvalid = NSLocalizedString("Phone no is invalid!", comment: "adadfadf")
    let strMsgNameLength = NSLocalizedString("Name must contain 2 or more characters!", comment: "adadfadf")
    let strMsgNameValidity = NSLocalizedString("Name must start with an Alphabet and can have Alphabets, Digits, Spaces, Underscores and Periods!", comment: "adadfadf")
    let strMsgLocationCheckWait = NSLocalizedString("Wait for location check!", comment: "adadfadf")
    let strMsgOrderStatusUpdated = NSLocalizedString("Order status updated!", comment: "adadfadf")
    let strMsgYouConfirmedOrder = NSLocalizedString("You have confirmed the order", comment: "adadfadf")
    let strMsgYouArePreparingOrder = NSLocalizedString("You are preparing the order", comment: "adadfadf")
    let strMsgReadyToPickUp = NSLocalizedString("Order is ready to pickup", comment: "adadfadf")
    let strMsgYouHaveDeliveredOrder = NSLocalizedString("You have delivered the order", comment: "adadfadf")
    let strMsgWeHaveReceivedYouOrder = NSLocalizedString("We have received your order.", comment: "adadfadf")
    let strMsgYourOrderHasBeenConfirmed = NSLocalizedString("Your order has been confirmed", comment: "adadfadf")
    let strMsgWeArePreparingYourOrder = NSLocalizedString("We are preparing your order", comment: "adadfadf")
    let strMsgYourOrderIsReady = NSLocalizedString("Your order is ready", comment: "adadfadf")
    let strMsgYourOrderHasBeenDelivered = NSLocalizedString("Your order has been delivered", comment: "adadfadf")
    let strMsgSelectContact = NSLocalizedString("Please select contact!", comment: "adadfadf")
    let strMsgSelectDocument = NSLocalizedString("Please select document first!", comment: "adadfadf")
    let strMsgAccountAddedSuccessfully = NSLocalizedString("Account Added Successfully!", comment: "adadfadf")
    let strMsgEnterDishName = NSLocalizedString("Please enter dish name.", comment: "adadfadf")
    let strMsgEmojisNotAllowed = NSLocalizedString("Emojis are not allowed.", comment: "adadfadf")
    let strMsgSelectCuisine = NSLocalizedString("Please select cuisine.", comment: "adadfadf")
    let strMsgEnterDishPrice = NSLocalizedString("Please enter dish price.", comment: "adadfadf")
    let strMsgSelectDishImage = NSLocalizedString("Please select dish image.", comment: "adadfadf")
    let strMsgAddYourBankAccount = NSLocalizedString("Please add your bank account with us.", comment: "adadfadf")
    
    let strMsgAddYourBankAccountToSell = NSLocalizedString("You are not yet visible to your customer. Please add your bank account to start selling your dishes.", comment: "adadfadf")
    
    let strMsgTryAgainLater = NSLocalizedString("Please try again after some time!", comment: "adadfadf")
    let strMsgNameIsInvalid = NSLocalizedString("Name is not valid!", comment: "adadfadf")
    let strMsgCardNumberIsInvalid = NSLocalizedString("Card Number is not valid!", comment: "adadfadf")
    let strMsgYearIsInvalid = NSLocalizedString("Year is not valid!", comment: "adadfadf")
    let strMsgMonthIsInvalid = NSLocalizedString("Month is not valid!", comment: "adadfadf")
    let strMsgCVVIsInvalid = NSLocalizedString("CVV number is not valid!", comment: "adadfadf")
    let strMsgSelectCard = NSLocalizedString("Please select a card!", comment: "adadfadf")
    let strMsgNoCardDataFound = NSLocalizedString("No cards data found!", comment: "adadfadf")
    let strMsgCookerHaveNoDish = NSLocalizedString("Cooker does not have any dish yet.", comment: "adadfadf")
    let strMsgSelectDish = NSLocalizedString("Please select dish first!", comment: "adadfadf")
    let strMsgSelectLanguage = NSLocalizedString("Please select language", comment: "adadfadf")
    let strMsgJoinMe = NSLocalizedString("Hi!\nPlease join me on COOK4", comment: "adadfadf")
    let strMsgSelect5Image = NSLocalizedString("Please select 5 or less images!", comment: "adadfadf")
    let strMsgPleaseWait = NSLocalizedString("Please wait...", comment: "adadfadf")
    let strMsgOrderConfirmationtSoon = NSLocalizedString("Please wait for your order confirmation.\nWe will confirm you soon.", comment: "adadfadf")
    let strMsgOrderUpdateSoon = NSLocalizedString("Please wait for your order is processing.\nWe will update you shortly.", comment: "adadfadf")
    let strMsgSessionExpired = NSLocalizedString("Session has expired.\nYou need to login again!", comment: "adadfadf")
    
    let strEditProfile     = NSLocalizedString("Edit Profile", comment: "adadfadf")
    let strSwitchToGourmet = NSLocalizedString("Switch to GOURMET", comment: "adadfadf")
    let strSwitchToCooker  = NSLocalizedString("Switch to COOKER", comment: "adadfadf")
    let strChangePassword  = NSLocalizedString("Change Password", comment: "adadfadf")
    let strChangeLanguage  = NSLocalizedString("Change Language", comment: "adadfadf")
    let strFirstSpeciality  = NSLocalizedString("First Speciality", comment: "adadfadf")
    let strSecondSpeciality  = NSLocalizedString("Second Speciality", comment: "adadfadf")
    let strSelectLanguage = NSLocalizedString("Select language", comment: "adadfadf")
    
    let strAskToSwitchAsGroumet  = NSLocalizedString("Do you want to switch as GOURMET?", comment: "adadfadf")
    let strAskToSwitchAsCooker   = NSLocalizedString("Do you want to switch as COOKER?", comment: "adadfadf")
    let strAskAddressOrZip   = NSLocalizedString("YOUR ADDRESS OR ZIP CODE ?", comment: "adadfadf")
    let strAskAddress   = NSLocalizedString("YOUR ADDRESS ?", comment: "adadfadf")
    let strAskUpdateStatus   = NSLocalizedString("Do you want to update status?", comment: "adadfadf")
    let strAskIsOrderReceived   = NSLocalizedString("Did you receive your order?", comment: "adadfadf")
    let strAskShouldDeleteDish   = NSLocalizedString("Do you want to delete the dish?", comment: "adadfadf")
    
    let strMe           = NSLocalizedString("ME", comment: "adadfadf")
    let strDeliveryCost = NSLocalizedString("Delivery Cost", comment: "adadfadf")
    
    let strYes      = NSLocalizedString("Yes", comment: "adadfadf")
    let strNo       = NSLocalizedString("No", comment: "adadfadf")
    let strView     = NSLocalizedString("View", comment: "adadfadf")
    let strIgnore   = NSLocalizedString("Ignore", comment: "adadfadf")
    let strSubmit   = NSLocalizedString("Submit", comment: "adadfadf")
    let strOK       = NSLocalizedString("OK", comment: "adadfadf")
    let strCuisine  = NSLocalizedString("Cuisine", comment: "adadfadf")
    let strSave     = NSLocalizedString("Save", comment: "adadfadf")
    let strCamera   = NSLocalizedString("Camera", comment: "adadfadf")
    let strGallery  = NSLocalizedString("Gallery", comment: "adadfadf")
    let strCancel   = NSLocalizedString("Cancel", comment: "adadfadf")
    let strOrder    = NSLocalizedString("Orders", comment: "adadfadf")
    let strRating   = NSLocalizedString("Rating", comment: "adadfadf")
    let strReview   = NSLocalizedString("Review", comment: "adadfadf")
    let strMore     = NSLocalizedString("More", comment: "adadfadf")
    let strTotal    = NSLocalizedString("Total", comment: "adadfadf")
    let strChange   = NSLocalizedString("Change", comment: "adadfadf")
    let strRefresh  = NSLocalizedString("Refresh", comment: "adadfadf")
    
    let strHeaderStory = NSLocalizedString("S T O R Y", comment: "adadfadf")
    let strHeaderChangePassword = NSLocalizedString("C H A N G E  P A S S W O R D", comment: "adadfadf")
    let strHeaderSetting = NSLocalizedString("S E T T I N G", comment: "adadfadf")
    let strHeaderNotification = NSLocalizedString("N O T I F I C A T I O N S", comment: "adadfadf")
    let strHeaderOrderHistory = NSLocalizedString("O R D E R  H I S T O R Y", comment: "adadfadf")
    let strHeaderEditProfile = NSLocalizedString("E D I T  P R O F I L E", comment: "adadfadf")
    let strHeaderSignUp = NSLocalizedString("S I G N  U P", comment: "adadfadf")
    let strHeaderInfo = NSLocalizedString("I N F O", comment: "adadfadf")
    let strHeaderVerify = NSLocalizedString("V E R I F Y", comment: "adadfadf")
    let strHeaderBankAccount = NSLocalizedString("B A N K  A C C O U N T", comment: "adadfadf")
    let strHeaderSetupPayment = NSLocalizedString("S E T  P A Y M E N T", comment: "adadfadf")
    let strHeaderReviews = NSLocalizedString("R E V I E W S", comment: "adadfadf")
    let strHeaderAddress = NSLocalizedString("A D D R E S S", comment: "adadfadf")
    let strHeaderInvite = NSLocalizedString("I N V I T E", comment: "adadfadf")
    let strHeaderThankYou = NSLocalizedString("T H A N K  Y O U", comment: "adadfadf")
    let strHeaderFindCooker = NSLocalizedString("F i n d  C o o k e r", comment: "adadfadf")
    let strHeaderChangeLanguage = NSLocalizedString("C H A N G E  L A N G U A G E", comment: "adadfadf")
    let strHeaderAddAccount = NSLocalizedString("A D D  A C C O U N T", comment: "adadfadf")
    let strHeaderAddCard = NSLocalizedString("A D D  C A R D", comment: "adadfadf")
    let strHeaderPayment = NSLocalizedString("P A Y M E N T", comment: "adadfadf")
    let strHeaderForgotPassword = NSLocalizedString("F O R G E T  P A S S W O R D", comment: "adadfadf")
    let strHeaderAbout = NSLocalizedString("A B O U T", comment: "adadfadf")
    
    let strOrderStatusDelivery = NSLocalizedString("Delivery", comment: "adadfadf")
    let strOrderStatusTakeAway = NSLocalizedString("Take away", comment: "adadfadf")
    let strOrderStatusPaymentPending = NSLocalizedString("Order Pending Payment", comment: "adadfadf")
    let strOrderStatusCompleted = NSLocalizedString("Completed", comment: "adadfadf")
    let strOrderStatusDelivered = NSLocalizedString("Delivered", comment: "adadfadf")
    let strOrderStatusReadyToPick = NSLocalizedString("Ready To Pick", comment: "adadfadf")
    let strOrderStatusInKitchen = NSLocalizedString("In Kitchen", comment: "adadfadf")
    let strOrderStatusConfirmed = NSLocalizedString("Confirmed", comment: "adadfadf")
    let strOrderStatusPaid = NSLocalizedString("Paid", comment: "adadfadf")
    let strOrderStatusWaitingForPayment = NSLocalizedString("Waiting For Payment!", comment: "adadfadf")
    let strOrderStatusRefused = NSLocalizedString("Refused", comment: "adadfadf")
    let strOrderStatusNewOrder = NSLocalizedString("New Order!", comment: "adadfadf")
    let strOrderStatusTapToPay = NSLocalizedString("Payment is pending.\nTap here to pay", comment: "adadfadf")
    let strOrderStatusPlaced = NSLocalizedString("Placed", comment: "adadfadf")
    let strOrderStatusPending = NSLocalizedString("Pending", comment: "adadfadf")
    let strOrderStatusAccepted = NSLocalizedString("Order Accepted", comment: "adadfadf")
    let strOrderStatusReadyToPickup = NSLocalizedString("Ready to Pickup", comment: "adadfadf")
    let strOrderStatusOrderDelivered = NSLocalizedString("Order Delivered", comment: "adadfadf")
    let strOrderStatusOrderPlaced = NSLocalizedString("Order Placed", comment: "adadfadf")
    let strOrderStatusOrderConfirmed = NSLocalizedString("Order Confirmed", comment: "adadfadf")
    
    
    let strTitleInviteFriends = NSLocalizedString("  Invite Friends on Cook4", comment: "adadfadf")
    let strTitleAddCard = NSLocalizedString("  Add Card", comment: "adadfadf")
    let strTitleInviteCooks = NSLocalizedString("  Invite Cooks on Cook4", comment: "adadfadf")
    let strTitleAddBankAccount = NSLocalizedString("  Add Bank Account", comment: "adadfadf")
    let strTitleRateOrder = NSLocalizedString("Rate Order", comment: "adadfadf")
    let strTitleSelectLanguage = NSLocalizedString("Select language", comment: "adadfadf")
    
    
    //"Max allowed selection (%d) reached"
    
    
}
