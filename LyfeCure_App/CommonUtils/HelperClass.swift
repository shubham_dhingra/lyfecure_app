//
//  HelperClass.swift
//  eFavour
//
//  Created by Meena on 30/08/18.
//  Copyright © 2018 Meena. All rights reserved.
//

import UIKit

class HelperClass: NSObject {
    
    //---------- instantiate view controller ----------//
    static func instantiateViewControllerFrom(storyBoardName storyboardName:String, withIdentifier identifier:String) -> UIViewController {
        
        let storyboard:UIStoryboard = UIStoryboard.init(name: storyboardName, bundle: nil)
        let viewController:UIViewController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        return viewController
    }
    
    static func instantiateViewControllerFromMainStoryBoard(withIdentifier identifier:String) -> UIViewController {
        
        let viewController:UIViewController = HelperClass.instantiateViewControllerFrom(storyBoardName: "Main", withIdentifier: identifier)
        
        return viewController
    }
    
    //---------- validation ----------//
    static func isValidEmail(_ email:String) -> Bool {
        
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: email)
    }
    
    static func isValidName(_ name:String) -> Bool {
        
        let finalString = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let regEx = "[A-Za-z][A-Z0-9a-z._ ]{2,64}"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: finalString)
    }
    
    static func isValidPassword(_ password:String) -> Bool {
        
//        let regEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{6,})"
//
//        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
//        return predicate.evaluate(with: password)

        if password.count > 5 {
            return true
        }
        
        return false
        
    }
    
    static func isValidPhone(phoneNumber:String) -> Bool {
        //"^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$"
        let phoneRegEx = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: phoneNumber)
    }
    
    static func isValidPostalCode(code:String) -> Bool {
        
        let codeRegEx = "^\\d{5}?$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", codeRegEx)
        return predicate.evaluate(with: code)
    }
    
    static func isValidOTP(code:String) -> Bool {
        
        let codeRegEx = "^\\d{4}?$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", codeRegEx)
        return predicate.evaluate(with: code)
    }
    
    static func isValidStreetName(sName:String) -> Bool {
        
        let codeRegEx = "^([\\w]{3,})([A-Za-z]\\.)?([ \\w]*\\#\\d+)?(\\r\\n| )[ \\w]{3,},\\x20[A-Za-z]{2}\\x20\\d{5}(-\\d{4})?$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", codeRegEx)
        return predicate.evaluate(with: sName)
    }
    
   static func isValidCardNumber(cardNumber: String) -> Bool {
        
        if cardNumber.count < 18 {
            
            return false
        }
        
        return true
    }
    
   static func isValidCardExpDate(expDate:String) -> Bool {
        
        let thisYear: UInt = 18
        let thisMonth: UInt = 11
        
        let arr = expDate.components(separatedBy: "/")
        let expiryMonth = UInt(arr[0]) ?? 00
        let expiryYear = UInt(arr[1]) ?? 00
        
        if (expiryYear < thisYear) {
            return false;
        }
        if (expiryYear == thisYear && expiryMonth < thisMonth) {
            return false;
        }
        if (expiryYear > thisYear + 15) {
            return false;
        }
        if(expiryYear>=thisYear+15 && expiryMonth>thisMonth){
            return false;
        }
        
        return true
    }
    
    //---------- Alert and Tost ----------//
    static func showAlert(withMessage message:String, onViewController viewController:UIViewController) {
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction.init(title: "Ok", style: UIAlertAction.Style.default) { (action) in
        }
        alert.addAction(okAction)
        viewController.present(alert, animated: false) {
        }
    }
    
    static func showTost(withMessage message:String, onViewController viewController:UIViewController) {
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        viewController.present(alert, animated: false) {
            
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (timer) in
                
                HelperClass.dismissTost(alert)
            })
        }
    }
    
    static func showTost(withMessage message:String, onViewController viewController:UIViewController, forTime time: TimeInterval) {
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        viewController.present(alert, animated: false) {
            
            Timer.scheduledTimer(withTimeInterval: time, repeats: false, block: { (timer) in
                
                HelperClass.dismissTost(alert)
            })
        }
    }
    
    static func dismissTost(_ alert:UIAlertController) {
        
        alert.dismiss(animated: false) {}
    }
    
    //MARK: - Image Base64
    static func base64String(fromImage image: UIImage) -> String {
        
        var base64String: String = ""
        
//        let imageData: Data = UIImageJPEGRepresentation(image, 0.90)!// UIImagePNGRepresentation(image)!
//        base64String = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        
        let imageData: Data? = image.jpegData(compressionQuality:0.4)
        base64String = imageData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters) ?? ""
        
        return String.init(format: "data:image/jpeg;base64,%@", base64String)
    }
    
    static func image(fromBase64String string: String) -> UIImage {
        
        let dataDecoded : Data = Data(base64Encoded: string, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        return decodedimage != nil ? decodedimage! : UIImage()
    }
    
//    static func image(fromURLString urlStr:String) -> UIImage? {
//        
//        var useURLString = urlStr
//        
//        if !(useURLString.starts(with: APIExecutor.baseURL)) {
//            
//            useURLString = APIExecutor.baseURL + urlStr
//        }
//        
//        var img: UIImage?
//        do
//        {
//            
//            let imageData: Data? = try Data.init(contentsOf: URL.init(string: useURLString)!)
//            
//            if imageData != nil {
//                
//                img = UIImage.init(data: imageData!)
//            }
//        }
//        catch {
//            
//            print("Image found in URL is nil")
//        }
//        
//        return img
//    }
        
    //MARK: - Date Methods
    private func toGlobalTime(_ date: Date) -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: date))
        return Date(timeInterval: seconds, since: date)
    }
    
    // Convert UTC (or GMT) to local time
    private func toLocalTime(_ date: Date) -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: date))
        return Date(timeInterval: seconds, since: date)
    }
    
    static func dayName(fromDateString dateString: String, withFormate formateString: String) -> String {
        
        let dateStr: String = dateString// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = HelperClass().toLocalTime(newDate!)
        formate.dateFormat = "EEEE"
        let dayInWeek = formate.string(from: localDate)
        
        return dayInWeek
    }
    
    static func monthName(fromDateString dateString: String, withFormate formateString: String) -> String {
        
        let dateStr: String = dateString// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = HelperClass().toLocalTime(newDate!)
        formate.dateFormat = "LLLL"
        let dayInWeek = formate.string(from: localDate)
        
        return dayInWeek
    }
    
    static func dayNumber(fromDateString dateString: String?, withFormate formateString: String) -> Int {
        
        if dateString == nil {
            
            return 0
        }
        
        let dateStr: String = dateString!// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = HelperClass().toLocalTime(newDate!)
        formate.dateFormat = "dd"
        let dayNumberStr = formate.string(from: localDate)
        
        return Int(dayNumberStr)!
    }
    
    static func hour(fromDateString dateString: String?, withFormate formateString: String) -> Int {
        
        if dateString == nil {
            
            return 0
        }
        
        let dateStr: String = dateString!// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = HelperClass().toLocalTime(newDate!)
        formate.dateFormat = "HH"
        let hourStr = formate.string(from: localDate)
        
        return Int(hourStr)!
    }
    
    static func minute(fromDateString dateString: String?, withFormate formateString: String) -> Int {
        
        if dateString == nil {
            
            return 0
        }
        
        let dateStr: String = dateString!// "30-03-2018 10:19"
        let formate: DateFormatter = DateFormatter()
        formate.dateFormat = formateString
        let newDate = formate.date(from: dateStr)
        let localDate = HelperClass().toLocalTime(newDate!)
        formate.dateFormat = "mm"
        let hourStr = formate.string(from: localDate)
        
        return Int(hourStr)!
    }
    
    //MARK:- Other
    static func numberWithPrefixString(forNumber number: Int) -> String {
        
        var prefixString: String? = ""
        
        switch number {
        case 1:
            prefixString = "\(number)st"
            break
        case 2:
            prefixString = "\(number)nd"
            break
        case 3:
            prefixString = "\(number)rd"
            break
        default:
            prefixString = "\(number)th"
        }
        
        return prefixString != nil ? prefixString! : "\(number)"
    }
    
    func addShaddow(inView view:UIView, withCornerRadius radius:CGFloat ) -> Void {
       
        let layer: CALayer = view.layer
        layer.cornerRadius = radius
        layer.masksToBounds = false

        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.2
        
    }
    
    
    //MARK: - Gradient add
    static func addAppGradientInBackground(onView view: UIView, withFrame frame: CGRect?, shouldHorizontal isHorizontal: Bool) -> Void {
        
        var rect: CGRect = view.bounds
        
        if frame != nil {
            
            rect = frame!
        }
        
        view.backgroundColor = UIColor.clear
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = rect
        
        if !isHorizontal {
            
            gradient.startPoint = CGPoint.init(x: 0.5, y: 0.0)
            gradient.endPoint   = CGPoint.init(x: 0.5, y: 1)
        }
        else {
            
            gradient.startPoint = CGPoint.init(x: 0.0, y: 0.5)
            gradient.endPoint   = CGPoint.init(x: 1.0, y: 0.5)
        }
        
        gradient.colors     =  [UIColor.red,
                                UIColor.yellow,
                                UIColor.white]
        gradient.opacity    = 1
        gradient.isHidden   = false
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    //MARK: ************* SHADOW VIEW ****************** //
static func applyShadowOnView(_ view: UIView) {
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 8
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 2.0, height: 10)
        view.layer.shadowRadius = 10
    }
     static func removeShadow(_ view: UIView) {
        view.layer.shadowOffset = CGSize(width: 0 , height: 0)
        view.layer.shadowColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 8
        view.layer.shadowRadius = 0.0
        view.layer.shadowOpacity = 0.0
    }
    static func addCornerView(_ view: UIView) {
    view.layer.cornerRadius = 10
    view.layer.borderWidth = 2.0
    view.layer.borderColor = UIColor.black.cgColor
    
    }
    static func addDoneBtn(_ view: UIView) {
       view.layer.cornerRadius = 10
       view.layer.borderWidth = 1.0
       view.layer.borderColor = UIColor.black.cgColor
       
    }
}
