//
//  UserOperation.swift
//  Glam
//
//  Created by ShailshNailwal on 2/5/18.
//  Copyright © 2018 Shailsh. All rights reserved.
//

import UIKit
//http://crmb2c.org/anessb2b/apis/

//let signUpURL: String          = "/users"
let sliderImgURL: String       = "/get-sliders"
let signInURL: String          = "/user/number-otp"
let verifyOtpURL: String       = "/user/verify-number-otp"
let signOutURL: String         = "/users/sign_out"
let SignUpURL: String          = "/user/congratulation-profile-update"
let trendURL: String           = "/user/get-trend-topic"
let subTrendURL: String        = "/user/get-sub-trending-topics"
let singleEateryOfferURL: String  = "/user/home-single-eatery-offer"
let nearRestaurantURL: String  = "/user/rest-near-user"
let homeDealURL: String        = "/user/home-deals"
let homeBannerURL: String      = "/user/home-banner"
let restaurantDetailsURL: String = "/user/get-restaurant-details"
let userGetBookingsURL: String    = "/user/get-user-bookings"
let userMakeBookingURL: String    = "/user/make-booking"
let homeOfferURL: String          = "/user/home-single-offer"


let loginURL: String          = "/Login"
let profileURL: String        = "/profile"
let bannerURL: String         = "/banner"
let categoryURL: String       = "/category"
let dealproductURL: String    = "/dealproduct"
let exclusiveURL: String      = "/exclusive"
let bestURL: String           = "/best"
let getdetailsURL: String     = "/getdetails"
let catlistingUrl : String    = "/cart_listing"
let orderListingUrl : String  = "/order_listing"
let orderDetailUrl : String   = "/order_details"
let cartremoveUrl : String    = "/cart_remove"
let catupdateUrl : String     = "/cart_update"
let addToCart : String        = "/cart"
let profileInfoUrl : String   =  "/profile_info"
let profileUpdateUrl : String =  "/profile_update"
let categoryProductListUrl : String = "/category_productlist"
let shippingaddrequestUrl : String =  "/shipping_add"
let shippinglistingrequestUrl : String =  "/shipping_listing"
let removeShippingAddress : String = "/shipping_remove"
let shippingupdaterequestUrl : String =  "/shipping_update"
let saveorderUrl : String =  "/save_order"
let searchUrl : String = "/search_productlist"

class UserOperation: NSObject {
    
      func signUp(withSignInInfo signinInfo:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {

            print("Request Params: ",signinInfo)
            APICallExecutor.postRequestForURLString(true, SignUpURL, [:], signinInfo) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in

                if done  {
                    let user: User = User()
                    let dict = result as? [String:Any]
                    if let dataDict =  dict?["data"] as? Dictionary<String, Any> {
                        user.setUserData((dataDict )[key_user] as? Dictionary<String, Any>)
                     user.setUserDataToUserDefaults()
                     let user: User = User.getUserDataFromUserDefaults()
                     print("Details**",user)
                    }
                }

                complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
            }
        }

    func signIn(withSignInInfo signinInfo:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {

        print("Request Params: ",signinInfo)
        APICallExecutor.postRequestForURLString(true, loginURL, [:], signinInfo) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in

            if done && resultSuccessStatus {

                let user: User = User()
                user.setUserData((result as! Dictionary<String, Any>)[key_user] as? Dictionary<String, Any>)
                user.setUserDataToUserDefaults()
            }

            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
        }
    }
      func VerifyOTP(withSignInInfo signinInfo:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {


            print("Request Params: ",signinInfo)
            APICallExecutor.postRequestForURLString(true, verifyOtpURL, [:], signinInfo) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in

                if done && resultSuccessStatus {

                    let user: User = User()
                  user.setUserData((result as! Dictionary<String, Any>)[key_user] as? Dictionary<String, Any>)
                  user.setUserDataToUserDefaults()
                }

                complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
            }
        }
    func getScrollInfo(complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
          
          var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
          let user: User = User.getUserDataFromUserDefaults()
          headerInfo[key_auth_token_header] = user.accessToken
          
          APICallExecutor.getRequestForURLString(true, sliderImgURL, headerInfo, Dictionary<String, Any>()) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
              
              complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
          }
      }
    func getProfile(complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
        
        let user: User = User.getUserDataFromUserDefaults()
        
        let finalURLStr: String = SignUpURL + "/\(user.id!)"
        
        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
        headerInfo[key_auth_token_header] = user.accessToken
        
        APICallExecutor.getRequestForURLString(true, finalURLStr, headerInfo, [:]) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            if done && resultSuccessStatus {
                
                let user: User = User()
                
                user.setUserData((result as! Dictionary<String, Any>)[key_user] as? Dictionary<String, Any>)
                user.setUserDataToUserDefaults()
            }
            
            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
        }
    }
    
    func logOut(withParams params:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
        
        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
        
        let user: User = User.getUserDataFromUserDefaults()
        
        headerInfo[key_auth_token_header] = user.accessToken// UserDefaults.standard.value(forKey: key_auth_token) as? String
        APICallExecutor.deleteRequestForURLString(true, signOutURL, headerInfo, params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
            
            UserDefaults.standard.removeObject(forKey: key_userdefaults_user)
            UserDefaults.standard.synchronize()
            
            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
        }
    }
    
//    func forgetPassword(withParams params:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//
//        APICallExecutor.postRequestForURLString(true, forgerPasswordURL, Dictionary<String, String>(), params, complitionHandler: complitionHandler)
//    }
    
//    func profile(withUserID userID:Int, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//
//        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
//
//        let user: User = User.getUserDataFromUserDefaults()
//
//        headerInfo[key_auth_token_header] = user.accessToken
//
//        //            {
//        //                "user":
//        //                {
//        //                    "id": 1
//        //                }
//        //        }
//
//        var userInfo: Dictionary<String, Int> = Dictionary<String, Int>()
//        userInfo[key_user_id] = userID
//
//        var params: Dictionary<String, Any> = Dictionary<String, Any>()
//        params["user"] = userInfo
//
//        APICallExecutor.postRequestForURLString(true, profileURL, headerInfo, params, complitionHandler: complitionHandler)
//    }
    
//    func profileUpdate(withUser user:User, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//
//        var params: Dictionary<String, Any> = Dictionary<String, Any>()
//
//
////        if user.country.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
////
////            params[key_user_country] = user.country
////        }
//
//        let currentUser: User = User.getUserDataFromUserDefaults()
//
//        if currentUser.userRole == user_role_gourmet {
//
//            params[key_user] = user.getUserDataForUpdate()
//        }
//        else {
//
//            params[key_user] = (user as! Cooker).getUserDataForUpdate()
//        }
//
//        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
//        headerInfo[key_auth_token_header] = currentUser.accessToken
//
//        let finalURLStr: String = signUpURL + "/\(currentUser.id!)"
//
//        APICallExecutor.putRequestForURLString(true, finalURLStr, headerInfo, params, complitionHandler: complitionHandler)
//    }
    
//    func profileUpdate(withUpdateInfo updateInfo:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//
//        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
//
//        let user: User = User.getUserDataFromUserDefaults()
//
//        headerInfo[key_auth_token_header] = user.accessToken
//
//        var params: Dictionary<String, Any> = Dictionary<String, Any>()
//        params[key_user] = updateInfo
//
//        APICallExecutor.postRequestForURLString(true, profileUpdateURL, headerInfo, params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
//
//            if done && resultSuccessStatus {
//
//                let userNew: User = User()
//                userNew.setUserData((result as! Dictionary<String, Any>)[key_user] as! Dictionary<String, Any>)
//
//                userNew.accessToken = user.accessToken
//
//                userNew.setUserDataToUserDefaults()
//            }
//
//            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
//        }
//    }
    
   
//    func changeUserRole(withRoleInfo roleInfo:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//        
//        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
//        let user: User = User.getUserDataFromUserDefaults()
//        headerInfo[key_auth_token_header] = user.accessToken
//        
//        var params: Dictionary<String, Any> = Dictionary<String, Any>()
//        params[key_user] = roleInfo
//        
//        APICallExecutor.postRequestForURLString(true, roleSwipeURL, headerInfo, params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
//            
//            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
//        }
//    }
    
//    func changePassword(withInfo info:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//        
//        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
//        let user: User = User.getUserDataFromUserDefaults()
//        headerInfo[key_auth_token_header] = user.accessToken
//        
//        var params: Dictionary<String, Any> = Dictionary<String, Any>()
//        params[key_user] = info
//        
//        APICallExecutor.postRequestForURLString(true, passwordUpdateURL, headerInfo, params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
//            
//            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
//        }
//    }
    
//    func resetPassword(withInfo info:Dictionary<String, Any>, complitionHandler: @escaping (_ successStatus: Bool, _ err: Error?, _ resultSuccessStatus: Bool, _ result: Any?, _ msg: String?, _ isActiveSession: Bool)-> Void) -> Void {
//
//        var headerInfo: Dictionary<String, String> = Dictionary<String, String>()
//        let user: User = User.getUserDataFromUserDefaults()
//        headerInfo[key_auth_token_header] = user.accessToken
//
//        var params: Dictionary<String, Any> = Dictionary<String, Any>()
//        params[key_user] = info
//
//        APICallExecutor.putRequestForURLString(true, passwordResetURL, headerInfo, params) { (done, error, resultSuccessStatus, result, msg, isActiveSession) in
//
//            complitionHandler(done, error, resultSuccessStatus, result, msg, isActiveSession)
//        }
//    }
}
