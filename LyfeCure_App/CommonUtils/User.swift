//
//  User.swift
//  Glam
//
//  Created by ShailshNailwal on 11/28/17.
//  Copyright © 2017 Shailsh. All rights reserved.
//

import UIKit

@objc class User: NSObject {
    
    var image: UIImage?
    var imageURLString: String?      = ""
    
  //  private var _languageCode: String = "en"
    
//    var languageCode: String {
//
//        get {
//
//            return _languageCode
//        }
//
//        set {
//
////            _languageCode = "rr"
//
//            if newValue.uppercased().elementsEqual("PT") {
//
//                _languageCode = "bp"
//            }
//            else if newValue.count > 0 {
//
//                _languageCode = newValue
//            }
//            else {
//
//                _languageCode = "en"
//            }
//        }
//    }
    
    var latitude: Double?
    var longitude: Double?
   // var postalCode: String?
  //  var city: String?
    


    var id: Int?
    
    var country: String = ""
    
    //var title: String? = ""
    
    private var _phone: String? = ""
    var phone: String?{
        
        get {
            
            return _phone
        }
        
        set {
            
            if newValue != nil {
                
                _phone = newValue?.replacingOccurrences(of: " ", with: "").trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            }
        }
    }
    
 //   private var _dob: String = ""
//    var dob: String? {
//
//        get {
//
//            return _dob
//        }
//
//        set {
//
//            if newValue != nil {
//
//                _dob = (newValue?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
//            }
//        }
//    }
    
//    private var _fName: String = ""
//    var fName: String? {
//
//        get {
//
//            return _fName
//        }
//
//        set {
//
//            if newValue != nil {
//
//                _fName = (newValue?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
//
//                _name = _fName
//                if _lName.count > 0 {
//
//                    _name = _fName + " " + _lName
//                }
//            }
//        }
//    }
//
//    private var _lName: String = ""
//    var lName: String? {
//
//        get {
//
//            return _lName
//        }
//
//        set {
//
//            if newValue != nil {
//
//                _lName = (newValue?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
//
//                _name = _lName
//                if _fName.count > 0 {
//
//                    _name = _fName + " " + _lName
//                }
//            }
//        }
//    }
    
    private var _name:String = ""
    var name:String? {
        
        get {
            
            return _name
        }
        
        set {
            
            _name = (newValue?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
        }
    }
    
    private var _email:String?
    var email:String? {
        
        get {
            
            return _email
        }
        
        set {
            
            _email = newValue?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    var password:String?
    
    private var _address: String = ""
    var address: String? {
        
        get {
            
            return _address
        }
        
        set {
            
            if newValue != nil {
                
                _address = (newValue?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
            }
        }
    }
    
    private var _accessToken:String?
    @objc var accessToken:String? {
        
        get {
            
            return _accessToken
        }
        
        set {
            
            _accessToken = newValue
        }
    }
    
    
    func getUserData() -> Dictionary<String, Any>  {

        var userInfo: Dictionary<String, Any> = Dictionary<String, Any>()

        if self.id != nil {

            userInfo[key_user_id] = self.id
        }

        userInfo[key_user_phone] = _phone

        if self.name != nil {

            userInfo[key_user_name] = self.name ?? ""
        }

        if self.email != nil {

            userInfo[key_user_email] = self.email ?? ""
        }

//        if self.city != nil {
//
//            userInfo[key_user_city] = self.city ?? ""
//        }
//
//        if self.postalCode != nil {
//
//            userInfo[key_user_postal_code] = self.postalCode ?? ""
//        }

//        if self.password != nil {
//
//            userInfo[key_user_password] = self.password ?? ""
//        }
//
//        if self.accessToken != nil {
//
//            userInfo[key_auth_token] = self.accessToken ?? ""
//        }

//        if dob != nil {
//
//            userInfo[key_user_dob] = dob
//        }
//
//        userInfo[key_user_role] = userRole
//
//        userInfo[key_user_location] = _address

        if self.imageURLString != nil {

            userInfo[key_image_info] = self.imageURLString
        }

//        if self.thumbImageURLString != nil {
//
//            userInfo[key_user_image_thumb] = self.thumbImageURLString
//        }

        if self.name != nil {

            userInfo[key_user_name] = self.name
        }

//        if self.fName != nil {
//
//            userInfo[key_user_first_name] = self.fName
//        }
//
//        if self.lName != nil {
//
//            userInfo[key_user_last_name] = self.lName
//        }
//
//        if self.postalCode != nil {
//
//            userInfo[key_user_postal_code] = self.postalCode
//        }
//
//        if self.city != nil {
//
//            userInfo[key_user_city] = self.city
//        }

//        if self.fName != nil {
//
//            self.name = self.fName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
//
//            if self.lName != nil {
//
//                self.name = self.name! + " " + (self.lName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!
//            }
//        }

        //// for cooker ////
//        userInfo[key_user_is_cooker]     = self.isCooker
//        userInfo[key_cooker_is_approved] = self.isApprovedAsCooker

//        if latitude != nil {
//
//            userInfo[key_location_lat] = latitude
//        }
//
//        if longitude != nil {
//
//            userInfo[key_location_lon] = longitude
//        }
//
//        userInfo[key_user_language] = languageCode

        return userInfo
    }
    
//    func getUserDataForUpdate() -> Dictionary<String, Any>  {
//
//        var userInfo: Dictionary<String, Any> = Dictionary<String, Any>()
//
//        if _phone != nil && !(_phone?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual(""))! {
//
//            userInfo[key_user_phone] = _phone
//        }
//
////        if self.email != nil {
////
////            userInfo[key_user_email] = self.email ?? ""
////        }
//
//        if city != nil && !(city?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual(""))!{
//
//            userInfo[key_user_city] = city ?? ""
//        }
//
//        if postalCode != nil && !(postalCode?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual(""))! {
//
//            userInfo[key_user_postal_code] = postalCode ?? ""
//        }
//
////        if dob != nil {
////
////            userInfo[key_user_dob] = dob
////        }
//
//        if !(_address.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual("")) {
//
//            userInfo[key_user_location] = _address
//        }
//
//        if name != nil && !(name?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual(""))! {
//
//            userInfo[key_user_name] = name
//        }
//
//        if fName != nil && !(fName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual(""))!{
//
//            userInfo[key_user_first_name] = fName
//        }
//
//        if lName != nil && !(lName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).elementsEqual(""))! {
//
//            userInfo[key_user_last_name] = lName
//        }
//
//        if image != nil {
//
//            userInfo["image"] = TaskExecutor.base64String(fromImage: image!)
//        }
//
//        userInfo[key_user_language] = languageCode
//
//        return userInfo
//    }
    
    func setUserData(_ userData:Dictionary<String, Any>?) -> Void {

        if userData == nil {

            return
        }

        var userInfo: Dictionary = userData!

        if (userData![key_auth_token] != nil) {

            userInfo[key_auth_token] = userData?[key_auth_token]
            accessToken = (userData![key_auth_token] as? String)!
        }

        if (userData![key_user_id] != nil) {

            userInfo[key_user_id] = userData?[key_user_id]
            id = userData![key_user_id] as? Int
        }

//        if (userData![key_user_password] != nil) {
//
//            userInfo[key_user_password] = userData?[key_user_password]
//            password = userData?[key_user_password] as? String
//        }

        if (userData![key_user_email] != nil) {

            userInfo[key_user_email] = userData?[key_user_email]
            email = userData?[key_user_email] as? String
        }

        if (userData![key_user_phone] != nil) {

            userInfo[key_user_phone] = userData?[key_user_phone]
            phone = userData?[key_user_phone] as? String
        }

        if (userData![key_user_name] != nil) {

            userInfo[key_user_name] = userData?[key_user_name]
            name = userData?[key_user_name] as? String
            
        }

//        if (userData![key_user_first_name] != nil) {
//
//            fName = userData?[key_user_first_name] as? String
//        }
//
//        if (userData![key_user_last_name] != nil) {
//
//            lName = userData?[key_user_last_name] as? String
//        }

//        if (userData![key_user_location] != nil) {
//
//            userInfo[key_user_location] = userData?[key_user_location]
//            address = userData?[key_user_location] as? String
//        }

    
        //// for image ////
        if userData![key_image_info] != nil {

            if userData![key_image_info] is String {

                self.imageURLString = userData![key_image_info] as? String
            }
//            else if (userData![key_image_info] is Dictionary<String, Any>) {
//
//                if (userData![key_image_info] as! Dictionary<String, Any>)[key_image] != nil {
//
//                    self.imageURLString = (userData![key_image_info] as! Dictionary<String, Any>)[key_image] as? String
//                }

//                if (userData![key_image_info] as! Dictionary<String, Any>)[key_image_thumb_info] != nil {
//
//                    if ((userData![key_image_info] as! Dictionary<String, Any>)[key_image_thumb_info] as! Dictionary<String, Any>)[key_image] != nil {
//
//                        self.thumbImageURLString = ((userData![key_image_info] as! Dictionary<String, Any>)[key_image_thumb_info] as! Dictionary<String, Any>)[key_image] as? String
//                    }
//                }
//            }
//        }

//        if userData![key_image_info] != nil {
//
//            let imageInfo: Dictionary<String, Any> = userData?[key_image_info] as! Dictionary<String, Any>
//
//            if imageInfo[key_image] != nil {
//
//                imageURLString = imageInfo[key_image] as? String
//                userInfo[key_user_image_main] = imageURLString
//
//                if (imageInfo[key_image_thumb_info] as! Dictionary<String, Any>)[key_image] != nil && (imageInfo[key_image_thumb_info] as! Dictionary<String, Any>)[key_image] is String{
//
//                    thumbImageURLString = (imageInfo[key_image_thumb_info] as! Dictionary<String, Dictionary<String, Any>>)[key_image] as? String
//                    userInfo[key_user_image_thumb] = thumbImageURLString
//                }
//            }
//        }

      


//        if userData![key_user_dob] != nil {
//
//            let bookings: Array<ActiveOrder> = ActiveOrderOperation().makeActiveOrderList(fromInfoList: userData?[key_active_booking])
//            arrActiveOrders.removeAll()
//            arrActiveOrders.append(contentsOf: bookings)
//        }

//        if (userData![key_location_lat] != nil) {
//
//            let obj: Any = userData![key_location_lat]!
//            self.latitude = Double("\(obj)")
//        }

//        if (userData![key_location_lon] != nil) {
//
//            let obj: Any = userData![key_location_lon]!
//            self.longitude = Double("\(obj)")
//        }

       

//        if userData![key_user_language] != nil && userData![key_user_language] is String {
//
//            self.languageCode = userData![key_user_language] as! String
        }
    }
    
  
    var thumbImageURLString: String? = ""
    
    func setUserDataToUserDefaults() -> Void {

        let userDefaults: UserDefaults = UserDefaults.standard

        let userInfo: Dictionary<String, Any> = self.getUserData()

        userDefaults.setValue(userInfo, forKey: key_userdefaults_user)
        userDefaults.synchronize()
    }
    
    @objc static func getUserDataFromUserDefaults() -> User {
        
        let userDefaults: UserDefaults = UserDefaults.standard
        
        let savedUserInfo: Dictionary<String, Any>? = userDefaults.value(forKey: key_userdefaults_user) as? Dictionary<String, Any>
        
        let user: User = User()
     //   user.setUserData(savedUserInfo)
        
        return user;
    }
}
