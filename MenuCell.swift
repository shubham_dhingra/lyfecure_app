//
//  MenuCell.swift
//  CARDXE
//
//  Created by Ruchi on 24/04/19.
//  Copyright © 2019 E L Group. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imageViewSelected:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
