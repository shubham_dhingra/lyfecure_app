//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import EZSwiftExtensions

protocol SJSWiftDelegate :class{
    func didSelectMenuIndex(_ index:Int)
}

extension SJSWiftDelegate {
    
    func didSelectMenuIndex(_ index:Int){}
}
class MenuViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    
    weak var delegate:SJSWiftDelegate?
    let localLanguage = LocalLanguage()
    var titles = [String]()
    //  let images: [String] = ["Home","create_card","scan_qrcode","card_radar","help","recognition_languages","change_password","businesscard_contact","personal_card","logout"]
    var msgCount = 0
    @IBOutlet weak var vwSidePanel: UIView!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet weak var AddressLbl: UILabel!
    
    var deviceToken:String!
    //    let linkedinHelper = LinkedinSwiftHelper()
    
    override func viewDidLoad() {
        
        titles = ["Shop By Category","Tablet","Capsule","Syrup","Search By Company","Search By Composition","Dashboard","My Profile","Orders","My Cart" , "Logout", ""]
        
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let SocialStatus = UserDefaults.standard.bool(forKey:"SocialLoginStatus")
        if SocialStatus{
            if  indexPath.row == 6{
                return 0
            }
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        
        //cell.imgIcon.image = UIImage(named: images[indexPath.row])
        cell.lblTitle.text = titles[indexPath.row]
        if titles[indexPath.row] == "Shop By Category" ||  titles[indexPath.row] == "Dashboard" || titles[indexPath.row] == "Search By Company" || titles[indexPath.row] == "Search By Composition" {
            cell.lblTitle.font = UIFont(name: "OpenSans-Bold", size: 14)
            cell.backgroundColor = UIColor(netHex: 0xEFEFEF)
            cell.selectionStyle = .none
        }
        else {
            cell.lblTitle.font = UIFont(name: "OpenSans-Regular", size: 14)
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        switch (indexPath.row)
        {
            
        //Shop By Category & Blank & Dashboard
        case 0,6:
            return
            
        //Tablet
        case 1:
            hideSideMenu()

            let ListVC = mainStoryboard.instantiateViewController(withIdentifier: "ListingViewController") as! ListingViewController
            ez.topMostVC?.pushVC(ListVC)
            
            break
            
        //Capsule
        case 2:
            hideSideMenu()
            let ListVC = mainStoryboard.instantiateViewController(withIdentifier: "ListingViewController") as! ListingViewController
            ez.topMostVC?.pushVC(ListVC)
            break
            
        //Syrup
        case 3:
            hideSideMenu()
            let ListVC = mainStoryboard.instantiateViewController(withIdentifier: "ListingViewController") as! ListingViewController
            ez.topMostVC?.pushVC(ListVC)
            break
            
        //Search By Company & Search By Composition
        case 4 , 5:
            hideSideMenu()
            let ListVC = mainStoryboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            ez.topMostVC?.pushVC(ListVC)
            break
            
        //Profile
        case 7:
            hideSideMenu()
            let profileVC = mainStoryboard.instantiateViewController(withIdentifier: "ProfileTableViewController") as! ProfileTableViewController
            ez.topMostVC?.pushVC(profileVC)
            break
            
        //Orders
        case 8:
            hideSideMenu()
            let profileVC = mainStoryboard.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
            ez.topMostVC?.pushVC(profileVC)
            break
            
        //Cart
        case 9:
            hideSideMenu()
            let cartVC =  self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
              ez.topMostVC?.pushVC(cartVC)
            break
            
        //Logout
        case 10:
            self.logout()
        default:
            break
        }
        
    }
    
    func hideSideMenu() {
        ez.topMostVC?.so_containerViewController?.isSideViewControllerPresented = false
    }
    
    
    func logout() {
           
           UDKeys.UserID.remove()
           
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initialNavVC = mainStoryboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                
        
           
            let VC = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
           initialNavVC.viewControllers = [VC]
          
           UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
               UIApplication.shared.keyWindow?.rootViewController = initialNavVC
           }) { (completed) -> Void in
           }
       }
    
    
}


